//
//  ViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 31/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import TwitterKit
import Twitter
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let logInButton = TWTRLogInButton { (session, error) in
            if let unwrappedSession = session {
                let userId = unwrappedSession.userID
                print_debug("userid: \(userId)")
                let client = TWTRAPIClient.clientWithCurrentUser()
                let request = client.URLRequestWithMethod("GET",
                    URL: "https://api.twitter.com/1.1/account/verify_credentials.json",
                    parameters: ["include_email": "true", "skip_status": "true"],
                    error: nil)
                
                client.sendTwitterRequest(request) { response, data, connectionError in
                    if connectionError != nil {
                        print("Error: \(connectionError)")
                    }
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                        print_debug("json: \(json)")
                    } catch let jsonError as NSError {
                        print_debug("json error: \(jsonError.localizedDescription)")
                    }
                }
            } else {
                print_debug("Login error: %@", error!.localizedDescription);
            }
        }

        // TODO: Change where the log in button is positioned in your view
//        if((Twitter.sharedInstance().sessionStore.session()?.userID) == nil)
//        {
            logInButton.loginMethods = [.WebBased]
            logInButton.center = self.view.center
            self.view.addSubview(logInButton)
//        }
//        else
//        {
//            //user has already logged in take appropriate action
//        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
