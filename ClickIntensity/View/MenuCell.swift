//
//  MenuCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 04/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var menuOptionLabel : UILabel!
    @IBOutlet weak var menuIndicator : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class MenuLogoutCell: UITableViewCell {
    @IBOutlet weak var menuOptionLabel : UILabel!
    @IBOutlet weak var menuIndicator : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
