//
//  LoginService.swift
//  Sinr
//
//  Created by TecOrb on 25/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import Foundation
import Alamofire
import TSMessages
import KVNProgress
import SwiftyJSON

class LoginService {
    static let sharedInstance = LoginService()
    private init(){}
     func loginWithParams(email:String,password: String,completionBlock:(AnyObject?) -> Void){
        CommonClass.showLoaderWithStatus("Signing in..")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        let param = ["email":email,"password":password]
        print_debug("Hitting url : \(LOGIN_USER) \n with headers: \(head) \n and params : \(param)")

        Alamofire.request(.POST, LOGIN_USER,parameters:param,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("userToken is \n \(json)")
                    if let token = json["token"].string as String?{
                        completionBlock(token)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }


            case .Failure(let error):
                print_debug(error)
                completionBlock(nil)
                showErrorWithMessage(error.localizedFailureReason ?? "Request Time Out" )
            }
        }
    }

//     func signupWithParams(params:[String:AnyObject],completionBlock:(AnyObject?) -> Void){
//        // print_debug("email:\(email),password:\(password) headers: \(head)")
//
//        Alamofire.request(.POST, "", parameters: params).responseJSON { response in
//            switch response.result {
//            case .Success:
//                if let value = response.result.value {
//                    completionBlock(value)
//                }
//            case .Failure(let error):
//                print(error)
//                showErrorWithMessage(error.localizedFailureReason!)
//            }
//        }
//    }

     func loginWithSocialParams(params:[String:AnyObject],loginType : SocialLoginType,completionBlock:(AnyObject?) -> Void){
        var parameters = [String:AnyObject]()
        parameters = params
        parameters.updateValue(loginType.rawValue, forKey: "provider_name")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")

        print_debug("Hitting url : \(SOCIAL_LOGIN_USER) \n with headers: \(head) \n and params : \(parameters)")
        Alamofire.request(.GET, SOCIAL_LOGIN_USER, parameters: parameters,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    completionBlock(value)
                }
            case .Failure(let error):
                print_debug(error)
                showErrorWithMessage(error.localizedFailureReason!)
            }
        }
    }

    func loginWithGoogleAuth(authCode: String,completionBlock:(AnyObject?) -> Void){
        let parameters = ["code":authCode]
        let head = ["Accept":"application/json"]//,"Content-Type":"application/json"]
        print_debug("Hitting url : \(SOCIAL_LOGIN_USER) \n with headers: \(head) \n and params : \(parameters)")
        Alamofire.request(.POST, SOCIAL_LOGIN_USER, parameters: parameters,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    completionBlock(value)
                }
            case .Failure(let error):
                print_debug(error)
                showErrorWithMessage(error.localizedFailureReason!)
            }
        }
    }


     func forgotPasswordForEmail(email: String,completionBlock:(AnyObject?)-> Void){
        let param = [kEmail:email]
        Alamofire.request(.POST, "", parameters: param).responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    completionBlock(value)
                }
            case .Failure(let error):
                print(error)
                showErrorWithMessage(error.localizedFailureReason!)
            }
        }

    }
}

