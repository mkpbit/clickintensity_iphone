//
//  DashboardNavigationView.swift
//  ClickIntensity
//
//  Created by TecOrb on 02/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class DashboardNavigationView: UIView {
    @IBOutlet weak var frontLineCountLabel : UILabel!
    @IBOutlet weak var teamCountLabel : UILabel!
    @IBOutlet weak var frontLineButton : UIButton!
    @IBOutlet weak var teamButton : UIButton!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
