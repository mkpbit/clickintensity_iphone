//
//  MenuProfileCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 04/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class MenuProfileCell: UITableViewCell {
    @IBOutlet weak var profilePic : UIImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var DOJLabel : UILabel!
    @IBOutlet weak var LDJNumberLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircular(profilePic, borderColor: UIColor.lightGrayColor(), borderWidth: 1)
    }

}
