//
//  AddProspectViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import KVNProgress
class AddProspectViewController: UIViewController,SlideNavigationControllerDelegate,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var prospectTableView : UITableView!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBar : UISearchBar!

    var prospectArr = [ProspectModel]()
    var filteredProspect = [ProspectModel]()
    var selectedSet = Set<ProspectModel>()
    var user = UserModel()
    var searchHidden = false
    //let searchController = UISearchController(searchResultsController: nil)

    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeysForStyle(.FullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactImageDataKey,
            CNContactThumbnailImageDataKey]

        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containersMatchingPredicate(nil)
        } catch {
            // print("Error fetching containers")
        }

        var results: [CNContact] = []

        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainerWithIdentifier(container.identifier)

            do {
                let containerResults = try contactStore.unifiedContactsMatchingPredicate(fetchPredicate, keysToFetch: keysToFetch)
                results.appendContentsOf(containerResults)
            } catch {
                //print("Error fetching results for container")
            }
        }
        return results
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = UserModel.loadUserInfo()
        self.user = UserModel(json: userjson)
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.view.backgroundColor = UIColor.init(patternImage: self.blur(image: UIImage(named: "background")!))
        // self.searchBar.barTintColor = UIColor.init(patternImage: self.blur(image: UIImage(named: "background")!))
        self.searchBar.tintColor = UIColor(colorLiteralRed: 0/255.0, green: 122.0/255.0, blue: 255/255, alpha: 1.0)
        searchBar.delegate = self
        dispatch_async(dispatch_get_main_queue()) { 
            self.getAllContacts()
        }
    }

    func getAllContacts() {
        for contact in contacts{
            let prospect : ProspectModel = ProspectModel()
            //let formatter = CNContactFormatter.descriptorForRequiredKeysForStyle(.FullName)
            let name = CNContactFormatter.stringFromContact(contact, style: .FullName)
            var mobile: String?
            var tempArray : NSArray = NSArray()
            if (contact.phoneNumbers).count > 0 {
                tempArray = ((contact.phoneNumbers as NSArray?)?.valueForKey("value").valueForKey("digits")) as! NSArray
                for i in 0  ..< tempArray.count
                {
                    var phoneNumber : String = (tempArray.objectAtIndex(i)) as! String
                    let cnCondeExist = phoneNumber.containsString("+91")
                    if cnCondeExist{
                        phoneNumber = phoneNumber.stringByReplacingOccurrencesOfString("+91", withString: "+91-")
                    }else{
                        phoneNumber = "+91-\(phoneNumber)"
                    }

                    if phoneNumber.characters.count >= 10 {
                        mobile = phoneNumber
                        break
                    }
                }
            }
            var email: String?
            for emailAdd in contact.emailAddresses {
                email = emailAdd.value as? String
                if email != nil{
                    break
                }
            }
            if mobile != nil && email != nil
            {
                prospect.name = name ?? ""
                prospect.mobile = mobile ?? ""
                prospect.email = email ?? ""
                prospect.status = "hot"
                if contact.imageDataAvailable{
                    prospect.profileImage = UIImage(data: contact.imageData ?? NSData()) ?? UIImage()
                }
                self.prospectArr.append(prospect)
            }
        }
        prospectArr.sortInPlace { (pr1, pr2) -> Bool in
            pr1.name < pr2.name
        }
        self.prospectTableView.reloadData()
    }
    func getStringFromContact() {

    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    func blur(image image: UIImage) -> UIImage {
        let radius: CGFloat = 2.8;
        let context = CIContext(options: nil);
        let inputImage = CIImage(CGImage: image.CGImage!);
        let filter = CIFilter(name: "CIGaussianBlur");
        filter?.setValue(inputImage, forKey: kCIInputImageKey);
        filter?.setValue("\(radius)", forKey:kCIInputRadiusKey);
        let result = filter?.valueForKey(kCIOutputImageKey) as! CIImage;
        let rect = CGRectMake(radius * 2, radius * 2, image.size.width - radius * 4, image.size.height - radius * 4)
        let cgImage = context.createCGImage(result, fromRect: rect);
        let returnImage = UIImage(CGImage: cgImage);
        return returnImage;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if self.searchBar.text != "" {
            count = filteredProspect.count
        } else {
            count = prospectArr.count
        }

        return count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let prospect : ProspectModel

        if let cell = tableView.dequeueReusableCellWithIdentifier("AddProspectCell", forIndexPath: indexPath) as? AddProspectCell{
            if self.searchBar.text != "" {
                prospect = filteredProspect[indexPath.row]
            } else {
                prospect = prospectArr[indexPath.row]
            }

            var selected : Bool = false
            for pm in selectedSet{
                if pm.name == prospect.name && pm.email == prospect.email{
                   selected = true
                    break
                }
            }
            cell.radioButton.selected = selected
            cell.userImageView.image = prospect.profileImage
            cell.userNameLabel.text = prospect.name
            cell.secondaryTextLabel.text = prospect.email
            return cell
        }
        return UITableViewCell()
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = NSBundle.mainBundle().loadNibNamed("AddNewProspectHeader", owner: self, options: nil)
        if let headerView = nib[0] as? AddNewProspectHeader{
            headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 44)
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()

                if selectedSet.count > 0{
                    headerView.addNewButton.setTitle("Add Selected", forState: UIControlState.Normal)
                    headerView.iconImageView.image = UIImage(named: "doneTick")
                    headerView.addNewButton.addTarget(self, action: #selector(onClickAddNewProspectsOnServer(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                }else{
                    headerView.addNewButton.setTitle("Add New", forState: UIControlState.Normal)
                    headerView.iconImageView.image = UIImage(named: "right_arrow")
                    headerView.addNewButton.addTarget(self, action: #selector(onClickAddNewProspect(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                }

            return headerView
        }
        return UIView()
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        // print_debug("cell selectec: \()")
        if self.searchBar.text != "" {
            let pros = filteredProspect[indexPath.row]
            var selected : Bool = false
            for pm in selectedSet{
                if pm.name == pros.name && pm.email == pros.email{
                    selected = true
                    break
                }
            }
            if selected{
                selectedSet.remove(pros)
            }else{
                selectedSet.insert(pros)
            }
        } else {
            let pros = prospectArr[indexPath.row]
            var selected : Bool = false
            for pm in selectedSet{
                if pm.name == pros.name && pm.email == pros.email{
                    selected = true
                    break
                }
            }
            if selected{
                selectedSet.remove(pros)
            }else{
                selectedSet.insert(pros)
            }
        }
        tableView.reloadData()
    }

    func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        if(scrollView.panGestureRecognizer.translationInView(scrollView.superview).y > 0 )
        {
            if searchHidden{
                searchHidden = false
                UIView.animateKeyframesWithDuration(1, delay: 0.3, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: {
                    self.searchViewHeight.constant = 10
                    UIView.animateWithDuration(2, animations: {
                        self.searchViewHeight.constant = 50
                    })
                    }, completion: nil)
            }
        }
        else
        {
            if !searchHidden{
                searchHidden = true
                UIView.animateKeyframesWithDuration(1, delay: 0.3, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: {
                    self.searchViewHeight.constant = 10
                    UIView.animateWithDuration(2, animations: {
                        self.searchViewHeight.constant = 0
                    })
                    }, completion: nil)
            }
        }
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(scrollView.panGestureRecognizer.translationInView(scrollView.superview).y > 0 )
        {
            UIView.animateKeyframesWithDuration(1, delay: 0.3, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: {
                self.searchViewHeight.constant = 10
            UIView.animateWithDuration(2, animations: {
                self.searchViewHeight.constant = 50
            })
            }, completion: nil)
        }
        else
        {
            UIView.animateKeyframesWithDuration(1, delay: 0.3, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: {
                self.searchViewHeight.constant = 20

                UIView.animateWithDuration(2, animations: {
                    self.searchViewHeight.constant = 0
                })
                }, completion: nil)

        }
    }
    @IBAction func onClickAddNewProspect(sender:UIButton){

        let addNewProspectVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddNewProspectsViewController") as! AddNewProspectsViewController
        self.navigationController?.pushViewController(addNewProspectVC, animated: true)
    }

    @IBAction func onClickAddNewProspectsOnServer(sender:UIButton){
        print_debug("to be added : \(selectedSet.count)")
        var params = [String:AnyObject]()
        var prospects = [[String:String]]()
        for pros in selectedSet{
            var prospect = [String:String]()
            prospect.updateValue(pros.name, forKey:"name")
            prospect.updateValue(pros.email, forKey: "email")
            prospect.updateValue(pros.mobile, forKey: "mobile")
            prospect.updateValue(self.user.ID, forKey: "userid")
            prospects.append(prospect)
        }
        params.updateValue(true, forKey: "multiple")
        params.updateValue(prospects, forKey: "prospects")
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            ProspectService.sharedInstance.createProspectsWith(token, params: params, completionBlock: { (response) in
                if let _ = response as? [UserModel]{
                    KVNProgress.showSuccessWithStatus("Done")
                    self.selectedSet.removeAll()
                    self.prospectTableView.reloadData()
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = mainStoryboard.instantiateViewControllerWithIdentifier("MyProspectViewController")
                    SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: true, andCompletion: nil)
                }
            })
        }

    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
//        if self.searchBar.text! != ""{
//
//        }else{
//
//        }
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        self.view.endEditing(true)
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar){
        self.view.endEditing(true)
    }
}

extension AddProspectViewController: UISearchBarDelegate {
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        filterContentForSearchText(self.searchBar.text!)

    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(self.searchBar.text!)
    }

    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredProspect = prospectArr.filter { prospect in
            return prospect.name.lowercaseString.containsString(searchText.lowercaseString)
        }
        prospectTableView.reloadData()
    }
}
