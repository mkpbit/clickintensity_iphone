//
//  CoreDataHelper.swift
//  Bloom Trade
//
//  Created by TecOrb on 29/08/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//
import UIKit
import CoreData

class CoreDataHelper: NSObject {
    let responseContainner = "FutureResponse"
    static let sharedInstance = CoreDataHelper()
   override private init(){}

    func isCategoryAlreadyInDataBase(prospect: ProspectResponse)-> Bool{
        var result = false
        let managedObjectContext = AppDelegate().managedObjectContext
        let fetchRequest = NSFetchRequest()

        let entityDescription = NSEntityDescription.entityForName(responseContainner, inManagedObjectContext: managedObjectContext)

        fetchRequest.entity = entityDescription

        do {
            let stArray = try managedObjectContext.executeFetchRequest(fetchRequest)
            if stArray.count > 0{
                for s in stArray{
                    if let str = s as? NSManagedObject{
                        if ((str.valueForKey(prospect.kProspectID) as! String) == prospect.prospectID) || ((str.valueForKey(prospect.kResponseType) as! String) == prospect.responseType) {
                            result = true
                            break;
                        }
                    }
                }

            }else{
                result = false
            }

        } catch {
            result = false
        }

        return result
    }

    func saveProspectResponse(prospect: ProspectResponse) -> Bool{
        var result = false
        let managedObjectContext = AppDelegate().managedObjectContext
        let entityDescription = NSEntityDescription.entityForName(responseContainner, inManagedObjectContext: managedObjectContext)
        let newProspectResponse = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        newProspectResponse.setValue(prospect.prospectID, forKey: prospect.kProspectID)
        newProspectResponse.setValue(prospect.name, forKey: prospect.kName)
        newProspectResponse.setValue(prospect.email, forKey: prospect.kEmail)
        newProspectResponse.setValue(prospect.mobile, forKey: prospect.kMobile)
        newProspectResponse.setValue(prospect.responseType, forKey: prospect.kResponseType)
        newProspectResponse.setValue(prospect.date, forKey: prospect.kDate)
        newProspectResponse.setValue(prospect.time, forKey: prospect.kTime)
        newProspectResponse.setValue(prospect.comment, forKey: prospect.kComment)
        newProspectResponse.setValue(prospect.createdAt, forKey: prospect.kCreatedAt)
        
        do {
            try newProspectResponse.managedObjectContext?.save()
            result = true
        } catch {
            print_debug("Error in saving the Category")
        }
        return result
    }

    func getAllResponseFromDataBaseWith(prospectID:String) -> [ProspectResponse]{
        var prospectArray = [ProspectResponse]()
        let managedObjectContext = AppDelegate().managedObjectContext
        let fetchRequest = NSFetchRequest()
        let entityDescription = NSEntityDescription.entityForName(responseContainner, inManagedObjectContext: managedObjectContext)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "prospectID = %@", prospectID)

        do {
            let stArray = try managedObjectContext.executeFetchRequest(fetchRequest)
            if stArray.count > 0{
                for s in stArray{
                    if let strn = s as? NSManagedObject{
                        let prospect = ProspectResponse()
                        prospect.prospectID = strn.valueForKey(prospect.kProspectID) as! String
                        prospect.name = strn.valueForKey(prospect.kName) as! String
                        prospect.email = strn.valueForKey(prospect.kEmail) as! String
                        prospect.mobile = strn.valueForKey(prospect.kMobile) as! String
                        prospect.responseType = strn.valueForKey(prospect.kResponseType) as! String
                        prospect.date = strn.valueForKey(prospect.kDate) as! String
                        prospect.time = strn.valueForKey(prospect.kTime) as! String
                        prospect.comment = strn.valueForKey(prospect.kComment) as! String
                        prospect.createdAt = strn.valueForKey(prospect.kCreatedAt) as! NSDate
                        prospectArray.append(prospect)
                    }
                }
            }
        } catch {
        }
        prospectArray.sortInPlace { (p1, p2) -> Bool in
            p1.createdAt.isLessThanDate(p2.createdAt)
        }
        return prospectArray
    }

}
