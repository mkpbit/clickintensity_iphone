//
//  LatestSignUpModel.swift
//  ClickIntensity
//
//  Created by TecOrb on 05/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class LatestSignUpModel: NSObject {
    let kSignUpDate = "signupDate"
    let kFlagUrl = "flag"
    let kName = "name"

    var signUpDate : String = ""
    var name : String = ""
    var flagUrl : String = ""

   override init() {
        super.init()
    }
    init(json:JSON) {
        if let _name = json[kName].string as String?{
            self.name = _name
        }
        if let _flagUrl = json[kFlagUrl].string as String?{
            self.flagUrl = _flagUrl
        }
        if let _signUpDate = json[kSignUpDate].string as String?{
            self.signUpDate = _signUpDate
        }
        super.init()
    }

    init(dictionary:[String:AnyObject]){
        if let _name = dictionary[kName] as? String{
            self.name = _name
        }
        if let _flagUrl = dictionary[kFlagUrl] as? String{
            self.flagUrl = _flagUrl
        }
        if let _signUpDate = dictionary[kSignUpDate] as? String{
            self.signUpDate = _signUpDate
        }
        super.init()
    }

}

class LatestSignUpParser: NSObject {
    let kLatestSignUps = "users"
    var latestSignUps = [LatestSignUpModel]()
    override init() {
        super.init()
    }

    init(json:JSON) {
        if let latestUsers = json[kLatestSignUps].arrayObject as? [[String:AnyObject]]{
            latestSignUps.removeAll()
            for user in latestUsers{
                let lUser = LatestSignUpModel(dictionary: user)
                self.latestSignUps.append(lUser)
            }
        }

    }
}

