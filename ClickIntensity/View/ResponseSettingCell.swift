//
//  ResponseSettingCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 10/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
protocol ResponseSelectionProtocol {
    func resposeTypeDidSelected(responseType:String)
    func resposeDateDidSelected(responseDate:String,responseTime:String)
    func resposeDateDidSelected(date:NSDate)

}

class ResponseSettingCell: UITableViewCell,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var responseTextField:UITextField!
    @IBOutlet weak var dateTimeTextField:UITextField!
    @IBOutlet weak var commentTextView:UITextView!
    var activeTextField:UITextField?
    var delegate : ResponseSelectionProtocol?
    var responsePicker : UIPickerView!
    var datePicker : UIDatePicker!
    var responseDataSource : [String]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonClass.makeViewCircularWithCornerRadius(commentTextView, borderColor:UIColor(red: 120.0/255, green: 139.0/255.0, blue: 143.0/255.0, alpha: 0.5) , borderWidth: 1, cornerRadius: 4)
        self.responseTextField.delegate = self
        self.dateTimeTextField.delegate = self
        self.setUpResponsePicker()
        setUpDatePicker()
    }
    override func prepareForReuse() {
        commentTextView.text = ""
        dateTimeTextField.text = ""
        responseTextField.text = ""
        activeTextField = nil
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextField = textField
    }

    func setUpResponsePicker(){
        responsePicker = UIPickerView()
        responsePicker.backgroundColor = UIColor.lightTextColor()

        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action:#selector(onClickSelectResponse(_:)))
        doneButton.tintColor = UIColor.whiteColor()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action:nil)
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let array = [spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.barStyle = UIBarStyle.BlackTranslucent
        toolbar.backgroundColor = UIColor.darkGrayColor()
        toolbar.tintColor = UIColor.whiteColor()
        responseTextField.inputView = responsePicker
        responseTextField.inputAccessoryView = toolbar;
        responsePicker.delegate = self
        responsePicker.dataSource = self

    }

    func setUpDatePicker() {
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .DateAndTime
        datePicker.date = NSDate()
        datePicker.minimumDate = NSDate()
        datePicker.addTarget(self, action: #selector(ResponseSettingCell.dateChanged(_:)), forControlEvents: .ValueChanged)
        self.dateTimeTextField.inputView = datePicker
        let doneBar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.contentView.frame.size.width, 44))
        doneBar.barStyle = .BlackTranslucent
        let spacer2: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        doneBar.setItems([spacer2, UIBarButtonItem(title: "Done", style: .Done, target: self, action: #selector(onClickSelectDate(_:)))], animated: true)
        doneBar.backgroundColor = UIColor.darkGrayColor()
        doneBar.tintColor = UIColor.whiteColor()
        self.dateTimeTextField.inputAccessoryView = doneBar
    }

    func dateChanged(sender: AnyObject) {
        let outputFormatter: NSDateFormatter = NSDateFormatter()
        outputFormatter.locale = NSLocale.currentLocale()
        outputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dstr: String = outputFormatter.stringFromDate(datePicker.date)
        let sourceTimeZone = NSTimeZone(abbreviation: "GMT")
        let myTimeZone = NSTimeZone.localTimeZone()
        if let mydate = outputFormatter.dateFromString(dstr){
            let sourceGMTOffset = sourceTimeZone!.secondsFromGMTForDate(mydate)
            let destinationGMTOffset = myTimeZone.secondsFromGMTForDate(mydate)
            let interval = destinationGMTOffset - sourceGMTOffset
            let destinationDate = NSDate(timeInterval: NSTimeInterval(interval), sinceDate: mydate)
            delegate?.resposeDateDidSelected(destinationDate)
        }

        //to set text from date
        outputFormatter.dateFormat = "yyyy-MM-dd"
        let dateString: String = outputFormatter.stringFromDate(datePicker.date)
        outputFormatter.dateFormat = "hh:mm a"
        let timeString: String = outputFormatter.stringFromDate(datePicker.date)
        dateTimeTextField.text = "\(dateString) . \(timeString)"

        delegate?.resposeDateDidSelected(dateString, responseTime: timeString)

    }

    func dateFromDateString(dateString: String) -> NSDate?{
        let outputFormatter: NSDateFormatter = NSDateFormatter()
        let sourceTimeZone = NSTimeZone(abbreviation: "GMT")
        let myTimeZone = NSTimeZone.localTimeZone()
        outputFormatter.dateFormat = "dd-MM-YYYY HH:mm:ss"
        var date : NSDate?
        if let ldate = outputFormatter.dateFromString(dateString) as NSDate?{
            let sourceGMTOffset = sourceTimeZone!.secondsFromGMTForDate(ldate)
            let destinationGMTOffset = myTimeZone.secondsFromGMTForDate(ldate)
            let interval = destinationGMTOffset - sourceGMTOffset
            let destinationDate = NSDate(timeInterval: NSTimeInterval(interval), sinceDate: ldate)
            date = destinationDate
        }
        return date
    }

    @IBAction func onClickSelectResponse(sender: AnyObject){
        let row = responsePicker.selectedRowInComponent(0)
        let selectedReponse = responseDataSource![row]
        responseTextField.text = responseDataSource?[row]
        delegate?.resposeTypeDidSelected(selectedReponse)
        activeTextField?.resignFirstResponder()
    }
    @IBAction func onClickSelectDate(sender: AnyObject){
        let outputFormatter: NSDateFormatter = NSDateFormatter()
        outputFormatter.locale = NSLocale.currentLocale()
        outputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dstr: String = outputFormatter.stringFromDate(datePicker.date)
        let sourceTimeZone = NSTimeZone(abbreviation: "GMT")
        let myTimeZone = NSTimeZone.localTimeZone()
        if let mydate = outputFormatter.dateFromString(dstr){
            let sourceGMTOffset = sourceTimeZone!.secondsFromGMTForDate(mydate)
            let destinationGMTOffset = myTimeZone.secondsFromGMTForDate(mydate)
            let interval = destinationGMTOffset - sourceGMTOffset
            let destinationDate = NSDate(timeInterval: NSTimeInterval(interval), sinceDate: mydate)
            delegate?.resposeDateDidSelected(destinationDate)
        }

        outputFormatter.dateFormat = "yyyy-MM-dd"
        let dateString: String = outputFormatter.stringFromDate(datePicker.date)
        outputFormatter.dateFormat = "hh:mm a"
        let timeString: String = outputFormatter.stringFromDate(datePicker.date)
        dateTimeTextField.text = "\(dateString) . \(timeString)"
        delegate?.resposeDateDidSelected(dateString, responseTime: timeString)
        delegate?.resposeDateDidSelected(datePicker.date)
        activeTextField?.resignFirstResponder()
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return (responseDataSource?.count)!
    }

    func pickerView(pickerView: UIPickerView,titleForRow row: Int,forComponent component: Int) -> String?
    {
        return responseDataSource?[row]
    }

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerView == responsePicker{
            responseTextField.text = responseDataSource?[row]
            delegate?.resposeTypeDidSelected(responseDataSource![row])

        }
    }

}
