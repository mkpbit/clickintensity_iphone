//
//  InputTextFieldCell.swift
//  Sinr
//
//  Created by TecOrb on 27/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class InputTextFieldCell: UITableViewCell {
    @IBOutlet weak var inputTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
