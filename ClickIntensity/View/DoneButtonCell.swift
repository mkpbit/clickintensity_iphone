//
//  DoneButtonCell.swift
//  Sinr
//
//  Created by TecOrb on 27/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class DoneButtonCell: UITableViewCell {
    @IBOutlet weak var doneButton : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(doneButton, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 4)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
