//
//  SwitchCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell {
    @IBOutlet weak var mySwitch :UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let offColor = UIColor(red: 255.0/255.0, green: 44.0/255.0, blue: 56.0/255.0, alpha: 1.0)
        mySwitch.tintColor = offColor
        mySwitch.layer.cornerRadius = 16
        mySwitch.backgroundColor = offColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
