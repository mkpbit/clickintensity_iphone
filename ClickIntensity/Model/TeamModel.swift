//
//  TeamModel.swift
//  ClickIntensity
//
//  Created by TecOrb on 06/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class TeamModel: NSObject {
    let kMembers = "members"
    let kRows = "rows"
    let kLimit = "limit"
    var members = [TeamMember]()
    var rowsCount : Int = 0
    var limit :Int = 0

    init(json : JSON){
        if let rC = json[kRows].int as NSInteger?{
            self.rowsCount = rC
        }

        if let rL = json[kLimit].int as NSInteger?{
            self.limit = rL
        }

        if let memberArray = json[kMembers].arrayObject as? [[String:AnyObject]]{
            print_debug(memberArray)
            for m in memberArray{
                let member = TeamMember(dictionary: m)
                self.members.append(member)
            }
        }
        super.init()
    }

}


class TeamMember: NSObject {

//    "joinAt" : "2016-12-09T12:17:50.043Z",
//    "id" : "584aa0edfa5b9e5b7f2aa30d",
//    "level" : 2,
//    "email" : "test-level2@gmail.com",
//    "countryName" : "India",
//    "countryCode" : "IN",
//    "name" : "Test Level2"

    let kJoinAt = "joinAt"
    let kID = "id"
    let kLevel = "level"
    let kName = "name"
    let kEmail = "email"
    let kCountryName = "countryName"
    let kCountryCode = "countryCode"

    var joinAt:String = ""
    var ID:String = ""
    var level:String = ""
    var name:String = ""
    var email:String = ""
    var countryName:String = ""
    var countryCode:String = ""
    var mobile:String = "N/A"



    init(json : JSON){
        if let _jointAt = json[kJoinAt].string as String?{
            self.joinAt = _jointAt
        }

        if let _id = json[kID].string as String?{
            self.ID = _id
        }

        if let _level = json[kLevel].string as String?{
            self.level = _level
        }else if let _level = json[kLevel].int as Int?{
            self.level = "\(_level)"
        }
        if let _email = json[kEmail].string as String?{
            self.email = _email
        }
        if let _countryName = json[kCountryName].string as String?{
            self.countryName = _countryName
        }
        if let _countyCode = json[kCountryCode].string as String?{
            self.countryCode = _countyCode
        }
        if let _name = json[kName].string as String?{
            self.name = _name
        }
        super.init()
    }

    init(dictionary : [String:AnyObject]){
        if let _jointAt = dictionary[kJoinAt] as? String{
            self.joinAt = _jointAt
        }

        if let _id = dictionary[kID] as? String{
            self.ID = _id
        }

        if let _level = dictionary[kLevel] as? String{
            self.level = _level
        }else if let _level = dictionary[kLevel] as? Int{
            self.level = "\(_level)"
        }

        if let _email = dictionary[kEmail] as? String{
            self.email = _email
        }

        if let _countryName = dictionary[kCountryName] as? String{
            self.countryName = _countryName
        }
        if let _countyCode = dictionary[kCountryCode] as? String{
            self.countryCode = _countyCode
        }
        if let _name = dictionary[kName] as? String{
            self.name = _name
        }
        super.init()
    }

    
}



