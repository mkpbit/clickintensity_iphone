//
//  LeadsViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class LeadsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var responseButton : UIButton!
    @IBOutlet weak var leadTableView : UITableView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var userEmailLabel : UILabel!
    @IBOutlet weak var userDOJLabel : UILabel!
    @IBOutlet weak var userPhoneNumberLabel : UILabel!
    var prospectResponse = ProspectResponse()
    var myProspect : ProspectModel!
    var savedResponses = [ProspectResponse]()

    //var responseArray =
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = myProspect.name
        CommonClass.makeViewCircularWithRespectToHeight(responseButton, borderColor: UIColor(colorLiteralRed: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 0.3), borderWidth: 0.9)
        self.userNameLabel.text = myProspect.name
        self.userEmailLabel.text = myProspect.email
        self.userPhoneNumberLabel.text = myProspect.mobile
        self.userDOJLabel.text = "DOJ (\(CommonClass.dateWithString(myProspect.createdAt)))"
        savedResponses = CoreDataHelper.sharedInstance.getAllResponseFromDataBaseWith(myProspect.ID)
        prospectResponse.email = myProspect.email
        prospectResponse.prospectID = myProspect.ID
        prospectResponse.name = myProspect.name
        prospectResponse.mobile = myProspect.mobile

        leadTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(sender:UIBarButtonItem){
        self.navigationController?.popViewControllerAnimated(true)
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0{
        if indexPath.row == 0
        {
            return 204
        }else if indexPath.row == 1
        {
            return 44
        }
        }else{
            return 66
        }
        return 0
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let frame = CGRectMake(0, 0, 0, 0)
        return UIView(frame: frame)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var noOfRows = 0
        if section == 0{
            noOfRows = 2
        }else{
            noOfRows = savedResponses.count
        }
        return noOfRows
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0{
        if indexPath.row == 0
        {
            if let cell = tableView.dequeueReusableCellWithIdentifier("ResponseSettingCell", forIndexPath: indexPath) as? ResponseSettingCell{
                cell.responseTextField.placeholder = "Select an Option"
                cell.dateTimeTextField.placeholder = "Date and Time"
                cell.responseDataSource = ["Call Back","Confirmed Joining","Details on Mail","Request to Meet"]
                cell.delegate=self
                cell.commentTextView.delegate = self
                cell.commentTextView.text = prospectResponse.comment
                cell.responseTextField.text = prospectResponse.responseType
                if prospectResponse.date != ""{
                    cell.dateTimeTextField.text = "\(prospectResponse.date) . \(prospectResponse.time)"
                }else{
                    cell.dateTimeTextField.text = ""
                }
                return cell
            }
        }else if indexPath.row == 1
        {
            if let cell = tableView.dequeueReusableCellWithIdentifier("SaveResponseCell", forIndexPath: indexPath) as? SaveResponseCell{
                cell.saveResponseButton.addTarget(self, action: #selector(onClickSaveResponse(_:)), forControlEvents: .TouchUpInside)
                cell.recordButton.addTarget(self, action: #selector(onClickSaveResponse(_:)), forControlEvents: .TouchUpInside)
                return cell
            }
        }
        }else{
            let pResponse = savedResponses[indexPath.row]
            if let cell = tableView.dequeueReusableCellWithIdentifier("ResponseRepresentingCell", forIndexPath: indexPath) as? ResponseRepresentingCell{
                cell.responseTypeLabel.text = pResponse.responseType
                cell.dateTimeLabel.text = dateStringFromDate(pResponse.createdAt)
                cell.commentLabel.text = pResponse.comment
                return cell
            }

        }
        return UITableViewCell()
    }

    func dateStringFromDate(date: NSDate) -> String{
        let outputFormatter: NSDateFormatter = NSDateFormatter()
        outputFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        outputFormatter.dateFormat = "dd-MM-YYYY . hh:mm a"
        let dateString: String = outputFormatter.stringFromDate(date)
        return dateString
    }

    func dateFromDateString(dateString: String) -> NSDate{
        let outputFormatter: NSDateFormatter = NSDateFormatter()
        let sourceTimeZone = NSTimeZone(abbreviation: "GMT")
        let myTimeZone = NSTimeZone.defaultTimeZone()
        outputFormatter.dateFormat = "dd-MM-YYYY HH:mm"
        // outputFormatter.locale = NSLocale.currentLocale()
        var date = NSDate()
        if let ldate = outputFormatter.dateFromString(dateString) as NSDate?{
            let sourceGMTOffset = sourceTimeZone!.secondsFromGMTForDate(ldate)
            let destinationGMTOffset = myTimeZone.secondsFromGMTForDate(ldate)
            let interval = destinationGMTOffset - sourceGMTOffset
            let destinationDate = NSDate(timeInterval: NSTimeInterval(interval), sinceDate: ldate)
            date = destinationDate
        }
        return date
    }


    @IBAction func onClickCall(button:UIButton){
        let actionSheetForCall = UIAlertController(title: "Please Select", message: "Calling Option", preferredStyle: .ActionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetForCall.addAction(cancelAction)

        let inAppCallAction: UIAlertAction = UIAlertAction(title: "In App Call", style: .Default)
        { action -> Void in
            if self.userPhoneNumberLabel.text != nil{
                let number = self.userPhoneNumberLabel.text!
                let phoneNymber  = number.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                self.onClickInAppCall(phoneNymber)
            }
        }
        actionSheetForCall.addAction(inAppCallAction)

        let callFromCellularAction: UIAlertAction = UIAlertAction(title: "Call From Cellular", style: .Default)
        { action -> Void in
            if self.userPhoneNumberLabel.text != nil{
                let number = self.userPhoneNumberLabel.text!
                let phoneNymber  = number.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                self.onClickCallFromCellular(phoneNymber)
            }
        }
        actionSheetForCall.addAction(callFromCellularAction)
        self.presentViewController(actionSheetForCall, animated: true, completion: nil)
    }

    func onClickInAppCall(number: String)
    {


    }

    func onClickCallFromCellular(phoneNumber: String)
    {
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
}

extension LeadsViewController: ResponseSelectionProtocol,UITextViewDelegate{
    @IBAction func onClickSaveResponse(sender:UIButton){
        if prospectResponse.responseType == ""{
            showErrorWithMessage("Please Select a response type")
            return
        }
        if prospectResponse.date == ""{
            showErrorWithMessage("Please add date")
            return
        }
        if prospectResponse.time == ""{
            showErrorWithMessage("Please add time")
            return
        }
        if prospectResponse.comment == ""{
            showErrorWithMessage("Please add a comment")
            return
        }
        
        if CoreDataHelper.sharedInstance.saveProspectResponse(prospectResponse){
            scheduleLocalNotification()
            self.resetRespons()
            savedResponses = CoreDataHelper.sharedInstance.getAllResponseFromDataBaseWith(myProspect.ID)
            self.leadTableView.reloadData()
        }
    }

    func resetRespons(){
        prospectResponse.comment = "Add a comment"
        prospectResponse.responseType = ""
        prospectResponse.date = ""
        prospectResponse.time = ""
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = self.leadTableView.cellForRowAtIndexPath(indexPath) as! ResponseSettingCell
        cell.commentTextView.text = "Add a comment"
        self.leadTableView.reloadData()
    }

    func resposeDateDidSelected(responseDate: String, responseTime: String) {
        prospectResponse.date = responseDate
        prospectResponse.time = responseTime
    }
    func resposeDateDidSelected(date: NSDate) {
        prospectResponse.scheduledDate = date
    }
    func resposeTypeDidSelected(responseType: String) {
        prospectResponse.responseType = responseType
        setupNotificationSettings(prospectResponse)
    }
    func textViewDidBeginEditing(textView: UITextView) {
        prospectResponse.comment = textView.text
    }
    func textViewDidChange(textView: UITextView) {
        prospectResponse.comment = textView.text
    }
    func textViewDidEndEditing(textView: UITextView) {
        prospectResponse.comment = textView.text
    }

    func setupNotificationSettings(pResponse:ProspectResponse) {
        let notificationSettings: UIUserNotificationSettings! = UIApplication.sharedApplication().currentUserNotificationSettings()

        if (notificationSettings.types == UIUserNotificationType.None){
            // Specify the notification types.
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Sound]


            // Specify the notification actions.
            let justInformAction = UIMutableUserNotificationAction()
            justInformAction.identifier = pResponse.responseType
            justInformAction.title = pResponse.responseType
            justInformAction.activationMode = UIUserNotificationActivationMode.Background
            justInformAction.destructive = false
            justInformAction.authenticationRequired = false

//            let modifyListAction = UIMutableUserNotificationAction()
//            modifyListAction.identifier = "editList"
//            modifyListAction.title = "Edit list"
//            modifyListAction.activationMode = UIUserNotificationActivationMode.Foreground
//            modifyListAction.destructive = false
//            modifyListAction.authenticationRequired = true
//
//            let trashAction = UIMutableUserNotificationAction()
//            trashAction.identifier = "trashAction"
//            trashAction.title = "Delete list"
//            trashAction.activationMode = UIUserNotificationActivationMode.Background
//            trashAction.destructive = true
//            trashAction.authenticationRequired = true

            let actionsArray = NSArray(objects: justInformAction)
            //let actionsArrayMinimal = NSArray(objects: trashAction, modifyListAction)

            // Specify the category related to the above actions.
            let shoppingListReminderCategory = UIMutableUserNotificationCategory()
            shoppingListReminderCategory.identifier = pResponse.responseType
            shoppingListReminderCategory.setActions(actionsArray as? [UIUserNotificationAction], forContext: UIUserNotificationActionContext.Default)
            //shoppingListReminderCategory.setActions(actionsArrayMinimal as? [UIUserNotificationAction], forContext: UIUserNotificationActionContext.Minimal)
            let categoriesForSettings = NSSet(objects: shoppingListReminderCategory)
            // Register the notification settings.
            let newNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: categoriesForSettings as? Set<UIUserNotificationCategory>)
            UIApplication.sharedApplication().registerUserNotificationSettings(newNotificationSettings)
        }
    }


    func scheduleLocalNotification() {
        let localNotification = UILocalNotification()
        localNotification.fireDate = fixNotificationDate(prospectResponse.scheduledDate!)
        localNotification.alertBody = prospectResponse.comment
        localNotification.alertAction = prospectResponse.responseType
        //localNotification.alertLaunchImage = "escortstar"
        localNotification.category = prospectResponse.responseType
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }


    func fixNotificationDate(dateToFix: NSDate) -> NSDate {
        let dateComponets: NSDateComponents = NSCalendar.currentCalendar().components([NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year, NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: dateToFix)

        dateComponets.second = 0

        let fixedDate: NSDate! = NSCalendar.currentCalendar().dateFromComponents(dateComponets)

        return fixedDate
    }

}
