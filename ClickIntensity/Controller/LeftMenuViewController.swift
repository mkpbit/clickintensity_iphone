//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//
import UIKit
import SDWebImage
import SafariServices
class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var slideOutAnimationEnabled : Bool!
    var leftMenuOptions = [String]()
    var didResourcesSelected = false
    var user : UserModel!
    override func viewDidLoad() {
        if self.user == nil{
            let userJson = UserModel.loadUserInfo()
            self.user = UserModel(json: userJson)
        }
        slideOutAnimationEnabled = false
        leftMenuOptions = ["Profile", "Dashboard","Frontline","Team", "Notification", "Add Prospect", "My Prospect", "Resources","Log Out"]
        super.viewDidLoad()
        SlideNavigationController.sharedInstance().enableShadow = true
        SlideNavigationController.sharedInstance().enableSwipeGesture = true
        SlideNavigationController.sharedInstance().portraitSlideOffset = UIScreen.mainScreen().bounds.size.width * 1 / 4
        self.tableView.separatorColor = UIColor.whiteColor()
        self.tableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0)
        self.automaticallyAdjustsScrollViewInsets = false
        let imageView: UIImageView = UIImageView(image: UIImage(named: "background"))
        imageView.backgroundColor = UIColor.blackColor()
        self.tableView.backgroundView = imageView
    }

    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view: UIView = UIView(frame: CGRectMake(0, 0, 0, 0))
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuOptions.count;
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 90
        }else if indexPath.row == 7{
            if !didResourcesSelected
            {
                return 44
            }
            else
            {
                return 252
            }
        }else{
            return 44
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCellWithIdentifier("MenuProfileCell", forIndexPath: indexPath) as! MenuProfileCell
            cell.DOJLabel.text = "DOJ (\(CommonClass.dateWithString(user.creatDate)))"
            cell.userNameLabel.text = user.name.capitalizedString
            cell.LDJNumberLabel.text = "\(user.userProfileID)"
            cell.profilePic.sd_setImageWithURL(NSURL(string: user.avatar), placeholderImage: UIImage(named: "prospect_dp"))
            return cell;

        }else if indexPath.row == 8{
             let cell = tableView.dequeueReusableCellWithIdentifier("MenuLogoutCell", forIndexPath: indexPath) as! MenuLogoutCell
             cell.menuOptionLabel.text = leftMenuOptions[indexPath.row]
             return cell

        }else if indexPath.row == 7{
            if !didResourcesSelected {
                let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as! MenuCell
                cell.menuOptionLabel.text = leftMenuOptions[indexPath.row]
                return cell
            }else{
                let cell = tableView.dequeueReusableCellWithIdentifier("MenuResourceCell", forIndexPath: indexPath) as! MenuResourceCell
                self.setupResouceActions(cell)
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as! MenuCell
            cell.menuOptionLabel.text = leftMenuOptions[indexPath.row]
            return cell
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc: UIViewController
        switch indexPath.row {
            case 0:
                //profile
                return
        // vc = mainStoryboard.instantiateViewControllerWithIdentifier("ProfileViewController")
            case 1:
            //Dashboard
                vc = mainStoryboard.instantiateViewControllerWithIdentifier("DashBoardViewController")
        // vc.navigationController!.title = nil
            case 2:
            //Leads
                vc = mainStoryboard.instantiateViewControllerWithIdentifier("FrontLineViewController") as! FrontLineViewController
                vc.title = "Frontline"
            case 3:
            //Notification
                vc = mainStoryboard.instantiateViewControllerWithIdentifier("TeamViewController") as! TeamViewController
            vc.title = "My Team"
//            case 4:
//            //Leads
//                vc = mainStoryboard.instantiateViewControllerWithIdentifier("MyLeadsViewController") as! MyLeadsViewController
//                vc.title = "Leads"
            case 4:
                //Notification
                vc = mainStoryboard.instantiateViewControllerWithIdentifier("NotificationViewController")
            case 5:
                //Add Prospect
                vc = mainStoryboard.instantiateViewControllerWithIdentifier("AddProspectViewController")

            case 6:
            //My Prospect
                vc = mainStoryboard.instantiateViewControllerWithIdentifier("MyProspectViewController")
            case 7:
            //Resources
                didResourcesSelected = !didResourcesSelected
                self.tableView.reloadData()
            return
            default:
                self.logout()
                return
        }
        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    //MARK:- Resource cell buttons actions
    func setupResouceActions(cell : MenuResourceCell) {
        cell.prospectingVideosButton.addTarget(self, action: #selector(onClickProspectingVideos(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.trainingVideosButton.addTarget(self, action: #selector(onClickTrainingVideos(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.audiosButton.addTarget(self, action: #selector(onClickAudios(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.imagesButton.addTarget(self, action: #selector(onClickImages(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.facebookPostsButton.addTarget(self, action: #selector(onClickFacebookPosts(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.emailCopiesButton.addTarget(self, action: #selector(onClickEmailCopies(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.oneLinerButton.addTarget(self, action: #selector(onClickOneLiners(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    @IBAction func onClickProspectingVideos(sender: UIButton){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("ProspectingVideosViewController") as! ProspectingVideosViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)

    }
    @IBAction func onClickTrainingVideos(sender: UIButton){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("TrainingVideosViewController") as! TrainingVideosViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)

    }
    @IBAction func onClickAudios(sender: UIButton){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("AudiosViewController") as! AudiosViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        
    }
    @IBAction func onClickImages(sender: UIButton){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("ImagesViewController") as! ImagesViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)

    }
    @IBAction func onClickFacebookPosts(sender: UIButton){
        //let urlString = "https://www.facebook.com/cicorporate"
        //let svc = SFSafariViewController(URL: NSURL(string:urlString)!, entersReaderIfAvailable: true)
        //svc.delegate = self
        //self.presentViewController(svc, animated: true, completion: nil)
         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = mainStoryboard.instantiateViewControllerWithIdentifier("FacebookPostsViewController") as! FacebookPostsViewController

        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        
    }
    @IBAction func onClickEmailCopies(sender: UIButton){
        SlideNavigationController.sharedInstance().toggleRightMenu()
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("EmailCopiesViewController") as! EmailCopiesViewController
//        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)

    }
    @IBAction func onClickOneLiners(sender: UIButton){
        SlideNavigationController.sharedInstance().toggleRightMenu()

//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("OneLinersViewController") as! OneLinersViewController
//        SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)

    }

    func logout() {
        kUserDefaults.setBool(false, forKey: kIsLoggedIN)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let leftMenu: LeftMenuViewController = (mainStoryboard.instantiateViewControllerWithIdentifier("LeftMenuViewController") as! LeftMenuViewController)
        let signInVc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SignInViewController")
        let appDelegate: AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        SlideNavigationController.sharedInstance().closeMenuWithCompletion {
            appDelegate.window!.rootViewController = nil
            let navController: SlideNavigationController = SlideNavigationController(rootViewController: signInVc)
            navController.rightMenu = leftMenu
            navController.menuRevealAnimationDuration = 0.18
            appDelegate.window!.rootViewController = navController
        }
    }
}
