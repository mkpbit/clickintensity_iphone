//
//  FrontLineViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class FrontLineViewController: UIViewController,SlideNavigationControllerDelegate {
    @IBOutlet weak var frontLineTableView : UITableView!
    var frontLine : FrontLineModel?
    var user : UserModel!
    var isPushed = false
    var isNewDataLoading = false
    var pageNumber = 1


    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(TeamViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        return refreshControl
    }()

    func handleRefresh(refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.frontLine?.members.removeAll()
        isNewDataLoading = true
        frontLineTableView.reloadData()
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            self.loadFrontLine(token, userID: self.user.ID, pageNumber: pageNumber, recordPerPage: 50, listOrder: ListOrder.Ascending, fromDate: self.user.creatDate, toDate: NSDate().iso8601, listedUsersTeamMaximumSize: 100, listUsersRank: 0, listUsersMinimumGoldPacks: 0, listUsersMaximumGoldPacks: 20, listUsersMinimumSilverPacks: 0, listUsersMaximumSilverPacks: 20, searchFieldName: "0", searchFieldValue: "0")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        frontLineTableView.tableFooterView = UIView()
        let userJson = UserModel.loadUserInfo()
        user = UserModel(json: userJson)
        self.frontLineTableView.addSubview(refreshControl)
        if frontLine == nil{
            frontLine = FrontLineModel()
        }
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
                    self.loadFrontLine(token, userID: self.user.ID, pageNumber: pageNumber, recordPerPage: 50, listOrder: ListOrder.Ascending, fromDate: self.user.creatDate, toDate: NSDate().iso8601, listedUsersTeamMaximumSize: 100, listUsersRank: 0, listUsersMinimumGoldPacks: 0, listUsersMaximumGoldPacks: 20, listUsersMinimumSilverPacks: 0, listUsersMaximumSilverPacks: 20, searchFieldName: "0", searchFieldValue: "0")
          }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        if !isPushed{
            let frame = CGRectMake(0, 0, 44, 44)
            let button = UIButton(frame:  frame)
            button.setTitle(" ", forState: .Normal)
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            // button.setImage(UIImage(named: "backArrow"), forState: .Normal)
            //button.addTarget(self, action: #selector(onClickBack(_:)), forControlEvents: .TouchUpInside)
            let leftBarButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = leftBarButton
        }else{
            let frame = CGRectMake(0, 0, 44, 44)
            let button = UIButton(frame:  frame)
            button.setImage(UIImage(named: "backArrow"), forState: .Normal)
            button.addTarget(self, action: #selector(onClickBack(_:)), forControlEvents: .TouchUpInside)
            let leftBarButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = leftBarButton
        }
    }

    @IBAction func onClickBack(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return !isPushed

    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if frontLine != nil{
            return frontLine!.members.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("FrontLineCell", forIndexPath: indexPath) as? FrontLineCell{
            let member = frontLine!.members[indexPath.row]
            cell.imageView?.sd_setImageWithURL(NSURL(string:member.email), placeholderImage: UIImage(named: "frontline_dp"))
            cell.DOJLabel.text = CommonClass.dateWithString(member.joinAt)
            cell.userNameLabel.text = member.name
            cell.emailLabel.text = member.email
            return cell
        }
        return UITableViewCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let member = frontLine!.members[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("LeadsViewController") as! LeadsViewController
        let prospect = ProspectModel()
        prospect.ID = member.ID
        prospect.name = member.name
        prospect.createdAt = member.joinAt
        prospect.email = member.email
        prospect.mobile = "G Level\(member.gLevel)"
        vc.myProspect = prospect
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {

        if scrollView == frontLineTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if pageNumber >= 1{
                            pageNumber+=1
                        }

                        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
                            self.loadFrontLine(token, userID: self.user.ID, pageNumber: pageNumber, recordPerPage: 50, listOrder: ListOrder.Ascending, fromDate: self.user.creatDate, toDate: NSDate().iso8601, listedUsersTeamMaximumSize: 100, listUsersRank: 0, listUsersMinimumGoldPacks: 0, listUsersMaximumGoldPacks: 20, listUsersMinimumSilverPacks: 0, listUsersMaximumSilverPacks: 20, searchFieldName: "0", searchFieldValue: "0")
                        }
                    }
                }
            }
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FrontLineViewController{


    func loadFrontLine(token:String,userID: String, pageNumber: Int, recordPerPage: Int, listOrder: ListOrder, fromDate: String, toDate: String, listedUsersTeamMaximumSize: Int, listUsersRank: Int, listUsersMinimumGoldPacks: Int, listUsersMaximumGoldPacks: Int, listUsersMinimumSilverPacks: Int, listUsersMaximumSilverPacks: Int, searchFieldName: String, searchFieldValue: String){

        DashBoardService.sharedInstance.getFrontLine(token, userID: userID, pageNumber: pageNumber, recordPerPage: recordPerPage, listOrder: listOrder, fromDate: fromDate, toDate: toDate, listedUsersTeamMaximumSize: listedUsersTeamMaximumSize, listUsersRank: listUsersRank, listUsersMinimumGoldPacks: listUsersMinimumGoldPacks, listUsersMaximumGoldPacks: listUsersMaximumGoldPacks, listUsersMinimumSilverPacks: listUsersMinimumSilverPacks, listUsersMaximumSilverPacks: listUsersMaximumSilverPacks, searchFieldName: searchFieldName, searchFieldValue: searchFieldValue) { (responseFrontLine) in
            if self.refreshControl.refreshing{
                self.refreshControl.endRefreshing()
            }

            self.isNewDataLoading = false
            if let frntline = responseFrontLine as? FrontLineModel{
                if frntline.members.count>0{
                    self.frontLine?.members.appendContentsOf(frntline.members)
                    self.frontLineTableView.reloadData()
                }else{
                    self.pageNumber -= 1
                }
            }
        }
    }
    
}

