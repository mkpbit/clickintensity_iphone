//
//  SponserModel.swift
//  ClickIntensity
//
//  Created by TecOrb on 14/07/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class SponserModel : NSObject {
    //Keys
    let kID = "id"
    let kName = "name"
    let kCountry = "country"
    let kRefUser = "refUser"
    //Properties
    var ID : String = ""
    var name : String = ""
    var country : String = ""
    var refUser : String = ""


    override init() {
        super.init()
    }


    init(json : JSON){
        if let dictionary  = json["user"].dictionaryObject as [String: AnyObject]?{

            if let userId = dictionary[kID] as? NSInteger{
                self.ID = "\(userId)"
            }else if let userId = dictionary[kID] as? String{
                self.ID = userId
            }

            if let sName = dictionary[kName] as? String{
                self.name = sName
            }
            if let sCountry = dictionary[kCountry] as? String{
                self.country = sCountry
            }
            if let ref = dictionary[kRefUser] as? String{
                self.refUser = ref
            }
        }
        super.init()
    }

    init(dictionary : [String:AnyObject]){

        if let userId = dictionary[kID] as? NSInteger{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }

        if let sName = dictionary[kName] as? String{
            self.name = sName
        }
        if let sCountry = dictionary[kCountry] as? String{
            self.country = sCountry
        }
        if let ref = dictionary[kRefUser] as? String{
            self.refUser = ref
        }

        super.init()
    }

}
