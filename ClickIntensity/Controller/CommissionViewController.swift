//
//  CommissionViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 04/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class CommissionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var aTableView : UITableView!
    @IBOutlet weak var daysContainerView : UIView!
    @IBOutlet weak var oneDayButton : UIButton!
    @IBOutlet weak var weekButton : UIButton!
    @IBOutlet weak var monthButton : UIButton!
    @IBOutlet weak var yearButton : UIButton!
    var isNewDataLoading = false

    @IBOutlet weak var summaryCountLabel : UILabel!
    var totalCommissionCount = 0
    @IBOutlet weak var oneDayLabel : UILabel!
    @IBOutlet weak var weekLabel : UILabel!
    @IBOutlet weak var monthLabel : UILabel!
    @IBOutlet weak var yearLabel : UILabel!

    @IBOutlet weak var daysViewHeight: NSLayoutConstraint!
    var pageNumber = 1
    var latestUsersArray = [LatestSignUpModel]()
    var totalCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(latestSignUpLoaded(_:)), name: LETEST_REGISTRATION_LOADED_NOTIFICATION, object:nil)
        self.oneDayLabel.text = "\(0)"
        self.weekLabel.text = "\(0)"
        self.monthLabel.text = "\(0)"
        self.yearLabel.text = "\(0)"
        self.summaryCountLabel.attributedText = self.attributtedTextForNKCountLabel("Your total commission till the date is \"\(self.totalCommissionCount)\" after joining Click Intensity.")

        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            DashBoardService.sharedInstance.getCommissionCountsByDaysWithToken(token, completionBlock: { (oneDayCount, weekCount, monthCont, yearCount) in
                self.oneDayLabel.text = "\(oneDayCount)"
                self.weekLabel.text = "\(weekCount)"
                self.monthLabel.text = "\(monthCont)"
                self.yearLabel.text = "\(yearCount)"
                self.totalCommissionCount = yearCount
                self.summaryCountLabel.attributedText = self.attributtedTextForNKCountLabel("Your total commission till the date is \"\(self.totalCommissionCount)\" after joining Click Intensity.")

            })
        }

        self.addShadowOn(oneDayButton)
    }

    func attributtedTextForNKCountLabel(text:String)-> NSMutableAttributedString{
        let string: NSMutableAttributedString = NSMutableAttributedString(string: text)
        let textColor = UIColor(red: 50.0/255.0, green: 58.0/255.0, blue: 71.0/255.0, alpha: 1.0)

        string.addAttribute(NSForegroundColorAttributeName, value: textColor , range: NSMakeRange(0, "Your total commission till the date is \"\(self.totalCommissionCount)\" after joining Click Intensity.".characters.count))

        string.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(13), range: NSMakeRange(0, "Your total commission till the date is ".characters.count))

        string.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(14), range: NSMakeRange("Your total commission till the date is ".characters.count-1,"\"\(self.totalCommissionCount)\" ".characters.count))

        string.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(13), range: NSMakeRange("Your total commission till the date is \"\(self.totalCommissionCount)\" ".characters.count-1,"after joining Click Intensity.".characters.count))

        return string
    }
    

    override func viewWillAppear(animated: Bool) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.latestUsersArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("DashBoardCell", forIndexPath: indexPath) as? DashBoardCell{
            let user = self.latestUsersArray[indexPath.row]
            cell.userNameLabel.text = user.name.capitalizedString
            cell.userDetailsLabel.text = "DOJ (\(CommonClass.dateWithString(user.signUpDate)))"
            return cell
        }
        return UITableViewCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
    }


    func latestSignUpLoaded(notification:NSNotification){
        if notification.name == LETEST_REGISTRATION_LOADED_NOTIFICATION{
            if let userInfo =  notification.userInfo as? [String: AnyObject]{
                if let page = userInfo["page"] as? Int{
                    self.pageNumber = page
                }
                if let users = userInfo["users"] as? [LatestSignUpModel]{
                    latestUsersArray.appendContentsOf(users)
                    self.aTableView.reloadData()
                }
            }
        }
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {

        if scrollView == aTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if pageNumber >= 1{
                            pageNumber+=1
                        }
                        self.loadLatestRegistration()
                    }
                }
            }
        }
    }

    
    func loadLatestRegistration(){
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            DashBoardService.sharedInstance.getLatestRegistrationsWithToken(token, pageNumber: self.pageNumber, completionBlock: { (responseArr) in
                self.isNewDataLoading = false
                if let latestRegArr = responseArr as? [LatestSignUpModel]{
                    // self.latestUsersArray.appendContentsOf(latestRegArr)
                    NSNotificationCenter.defaultCenter().postNotificationName(LETEST_REGISTRATION_LOADED_NOTIFICATION, object: nil, userInfo: ["users":latestRegArr,"page":self.pageNumber])
                }
            })
        }
    }


    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }


    //Days selection Actions
    @IBAction func onclickOneDayButton(sender:UIButton){
        self.toggleDaysSelection(sender)
    }
    @IBAction func onclickWeekButton(sender:UIButton){
        self.toggleDaysSelection(sender)
    }
    @IBAction func onclickMonthButton(sender:UIButton){
        self.toggleDaysSelection(sender)
    }
    @IBAction func onclickYearButton(sender:UIButton){
        self.toggleDaysSelection(sender)
    }


    func toggleDaysSelection(sender : UIButton){
        sender.selected = true
        self.addShadowOn(sender)
        if sender == oneDayButton{
            weekButton.selected = false
            self.removeShadowOn(weekButton)
            monthButton.selected = false
            self.removeShadowOn(monthButton)
            yearButton.selected = false
            self.removeShadowOn(yearButton)
        }else if sender == weekButton{
            oneDayButton.selected = false
            self.removeShadowOn(oneDayButton)
            monthButton.selected = false
            self.removeShadowOn(monthButton)
            yearButton.selected = false
            self.removeShadowOn(yearButton)
        }else if sender == monthButton{
            weekButton.selected = false
            self.removeShadowOn(weekButton)
            oneDayButton.selected = false
            self.removeShadowOn(oneDayButton)
            yearButton.selected = false
            self.removeShadowOn(yearButton)
        }else{
            weekButton.selected = false
            self.removeShadowOn(weekButton)
            monthButton.selected = false
            self.removeShadowOn(monthButton)
            oneDayButton.selected = false
            self.removeShadowOn(oneDayButton)
        }

    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    func addShadowOn(button:UIButton) {
        button.layer.shadowRadius = 2.0
        button.layer.shadowColor = UIColor.blackColor().CGColor;
        button.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.cornerRadius = 2
        button.layer.masksToBounds = false
    }
    func removeShadowOn(button:UIButton) {
        button.layer.shadowRadius = 0.0
        button.layer.shadowColor = UIColor.clearColor().CGColor;
        button.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        button.layer.shadowOpacity = 0.5
        button.layer.cornerRadius = 0
        button.layer.masksToBounds = false
    }


    

    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(scrollView.panGestureRecognizer.translationInView(scrollView.superview).y > 0 )
        {
            UIView.animateKeyframesWithDuration(1, delay: 0.3, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: {
                self.daysViewHeight.constant = 10
                UIView.animateWithDuration(2, animations: {
                    self.daysViewHeight.constant = 70
                })
                }, completion: nil)
        }
        else
        {
            UIView.animateKeyframesWithDuration(1, delay: 0.3, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: {
                self.daysViewHeight.constant = 20

                UIView.animateWithDuration(2, animations: {
                    self.daysViewHeight.constant = 0
                })
                }, completion: nil)
            
        }
    }
    
}
