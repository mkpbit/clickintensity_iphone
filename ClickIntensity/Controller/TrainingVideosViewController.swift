//
//  TrainingVideosViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 15/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import CollectionViewWaterfallLayout
import SafariServices

class TrainingVideosViewController:UIViewController,SlideNavigationControllerDelegate,UICollectionViewDataSource,CollectionViewWaterfallLayoutDelegate,SFSafariViewControllerDelegate{
    var videoArr = [VideoResource]()
    var isNewDataLoading = false
    var pageNumber = 1

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.headerInset = UIEdgeInsetsMake(0, 0, 0, 0)
        layout.headerHeight = 0
        layout.footerHeight = 0
        layout.minimumColumnSpacing = 5
        layout.minimumInteritemSpacing = 5

        collectionView.collectionViewLayout = layout
        collectionView.registerClass(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionHeader, withReuseIdentifier: "Header")
        collectionView.registerClass(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionFooter, withReuseIdentifier: "Footer")
        self.loadVideosFromServer(self.pageNumber)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }

    func loadVideosFromServer(pageNumber: Int){
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            CommonClass.showLoader()
            ProspectService.sharedInstance.getResourceVideosWith(token, pageNumber: self.pageNumber, subType: "daily-training", completionBlock: { (responseVideos, rowsCount) in
                CommonClass.hideLoader()

                self.isNewDataLoading = false
                if let rVideos = responseVideos as? [VideoResource]{
                    if rVideos.count > 0{
                        self.videoArr.appendContentsOf(rVideos)
                        self.collectionView.reloadData()
                    }else{
                        self.pageNumber -= 1
                    }
                }
            })
        }
    }

    //MARK: - UICollectionViewDataSource Method
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoArr.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProspectingVideoCell", forIndexPath: indexPath) as! ProspectingVideoCell
        let video = videoArr[indexPath.row]
        cell.videoNameLabel.text = video.title
        cell.videoThumb.setIndicatorStyle(.Gray)
        cell.videoThumb.setShowActivityIndicatorView(true)
        cell.videoThumb.sd_setImageWithURL(NSURL(string: video.thumbnailURL))
        CommonClass.makeViewCircularWithCornerRadius(cell.videoThumb, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 5)
        cell.shareButton.addTarget(self, action: #selector(onClickShareButton(_:)), forControlEvents: .TouchUpInside)
        cell.moreButton.addTarget(self, action: #selector(onClickShareButton(_:)), forControlEvents: .TouchUpInside)
        self.addShadowOn(cell)
        
        self.addShadowOn(cell)
        return cell
    }

    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        var reusableView: UICollectionReusableView? = nil

        if kind == CollectionViewWaterfallElementKindSectionHeader {
            reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath)

            if let view = reusableView {
                view.backgroundColor = UIColor.redColor()
            }
        }
        else if kind == CollectionViewWaterfallElementKindSectionFooter {
            reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Footer", forIndexPath: indexPath)
            if let view = reusableView {
                view.backgroundColor = UIColor.blueColor()
            }
        }

        return reusableView!
    }

    // MARK: WaterfallLayoutDelegate

    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.item == 0{
            let width = (self.view.frame.size.width/2.0) - 15.0
            let height = width*16/15
            return CGSize(width: width, height:height)
        }else{
            let width = (self.view.frame.size.width/2.0) - 15.0
            let height = width*22/15
            return CGSize(width: width, height:height)
        }
    }

    func addShadowOn(view : UIView) {
        view.layer.shadowRadius = 1.0
        view.layer.shadowColor = UIColor.blackColor().CGColor;
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowOpacity = 0.5
        view.layer.cornerRadius = 3
        view.layer.masksToBounds = false
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let video = videoArr[indexPath.row]
        self.openWithSafariVC(video)
    }


    @IBAction func onClickShareButton(sender:UIButton){
        if let indexPath = sender.collectionViewIndexPath(self.collectionView) as NSIndexPath?{
            let video = videoArr[indexPath.row]
            let textToShare = "Check this video, you will definitily get some benifit!"
            if let videoURL = NSURL(string: video.videoURL) {
                let objectsToShare = [textToShare, videoURL]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                activityVC.popoverPresentationController?.sourceView = sender
                self.presentViewController(activityVC, animated: true, completion: nil)
            }

        }
    }

    func openWithSafariVC(video: VideoResource)
    {
        let svc = SFSafariViewController(URL: NSURL(string: video.videoURL)!, entersReaderIfAvailable: true)
        svc.delegate = self
        self.presentViewController(svc, animated: true, completion: nil)
    }
    func safariViewControllerDidFinish(controller: SFSafariViewController) {

    }
    func safariViewController(controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        //        if scrollView == collectionView{
        //            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        //            {
        //                if !isNewDataLoading{
        //                    if CommonClass.isConnectedToNetwork{
        //                        isNewDataLoading = true
        //                        if pageNumber >= 1{
        //                            pageNumber+=1
        //                        }
        //                        self.loadVideosFromServer(pageNumber)
        //                    }
        //                }
        //            }
        //        }
    }
    
}
