//
//  MemberModel.swift
//  ClickIntensity
//
//  Created by TecOrb on 18/07/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class MemberModel: NSObject {
    //Keys


    let kID = "id"
    let kName = "name"
    let kSponsorID = "sponsorId"
    let KJoitAt = "joinAt"
    let kGLevel = "glevel"
    let kCountry = "country"
    let kRank = "rank"
    let kStatus = "status"
    let kSilverPacks = "silverpacks"
    let kGoldPacks = "goldpacks"
    let kUsers = "users"
    let kEmail = "email"
    let kSponsorUserName = "sponsorUsername"
    let kSponsorUserID = "sponsorUserId"
    let kUserName = "username"
    let kMobile = "mobile"

    //Properties
    var ID : String = ""
    var name : String = ""
    var country : String = ""
    var sponserID : String = ""
    var joinAt : String = ""
    var gLevel : String = ""
    var rank : String = ""
    var status : String = ""
    var silverPacks : String = ""
    var goldPacks : String = ""
    var users : String = ""
    var email : String = ""
    var sponsorUserName : String = ""
    var sponsorUserID : String = ""
    var userName : String = ""
    var mobile : String = ""

    override init() {
        super.init()
    }

    init(json : JSON){
        if let spnsrID = json[kSponsorID].string as String?{
            self.sponserID = spnsrID
        }
        if let dictionary  = json["memberInfo"].dictionaryObject as [String: AnyObject]?{
            if let mId = dictionary[kID] as? NSInteger{
                self.ID = "\(mId)"
            }else if let mId = dictionary[kID] as? String{
                self.ID = mId
            }
            if let mName = dictionary[kName] as? String{
                self.name = mName
            }
            if let mCountry = dictionary[kCountry] as? String{
                self.country = mCountry
            }
            if let mjoinat = dictionary[KJoitAt] as? String{
                self.joinAt = mjoinat
            }
            if let mglevel = dictionary[kGLevel] as? String{
                self.gLevel = mglevel
            }
            if let mrank = dictionary[kRank] as? String{
                self.rank = mrank
            }
            if let mstatus = dictionary[kStatus] as? String{
                self.status = mstatus
            }
            if let mgoldpacks = dictionary[kGoldPacks] as? String{
                self.goldPacks = mgoldpacks
            }
            if let msilverpacks = dictionary[kSilverPacks] as? String{
                self.silverPacks = msilverpacks
            }
            if let musers = dictionary[kUsers] as? String{
                self.users = musers
            }
            if let memail = dictionary[kEmail] as? String{
                self.email = memail
            }
            if let msponsoruserid = dictionary[kSponsorUserID] as? String{
                self.sponsorUserID = msponsoruserid
            }
            if let msponsorusername = dictionary[kSponsorUserName] as? String{
                self.sponsorUserName = msponsorusername
            }
            if let musername = dictionary[kUserName] as? String{
                self.userName = musername
            }
            if let mmobile = dictionary[kMobile] as? String{
                self.mobile = mmobile
            }
        }else if let dictionary  = json["member"].dictionaryObject as [String: AnyObject]?{
            if let mId = dictionary[kID] as? NSInteger{
                self.ID = "\(mId)"
            }else if let mId = dictionary[kID] as? String{
                self.ID = mId
            }
            if let mName = dictionary[kName] as? String{
                self.name = mName
            }
            if let mCountry = dictionary[kCountry] as? String{
                self.country = mCountry
            }
            if let mjoinat = dictionary[KJoitAt] as? String{
                self.joinAt = mjoinat
            }
            if let mglevel = dictionary[kGLevel] as? String{
                self.gLevel = mglevel
            }else if let mglevel = dictionary[kGLevel] as? Int{
                self.gLevel = "\(mglevel)"
            }
            if let mrank = dictionary[kRank] as? String{
                self.rank = mrank
            }else if let mrank = dictionary[kRank] as? Int{
                self.rank = "\(mrank)"
            }
            if let mstatus = dictionary[kStatus] as? String{
                self.status = mstatus
            }
            if let mgoldpacks = dictionary[kGoldPacks] as? String{
                self.goldPacks = mgoldpacks
            }else if let mgoldpacks = dictionary[kGoldPacks] as? Int{
                self.goldPacks = "\(mgoldpacks)"
            }
            if let msilverpacks = dictionary[kSilverPacks] as? String{
                self.silverPacks = msilverpacks
            }else if let msilverpacks = dictionary[kSilverPacks] as? Int{
                self.silverPacks = "\(msilverpacks)"
            }
            if let musers = dictionary[kUsers] as? String{
                self.users = musers
            }
            if let memail = dictionary[kEmail] as? String{
                self.email = memail
            }
            if let msponsoruserid = dictionary[kSponsorUserID] as? String{
                self.sponsorUserID = msponsoruserid
            }
            if let msponsorusername = dictionary[kSponsorUserName] as? String{
                self.sponsorUserName = msponsorusername
            }
            if let musername = dictionary[kUserName] as? String{
                self.userName = musername
            }
            if let mmobile = dictionary[kMobile] as? String{
                self.mobile = mmobile
            }
        }
        super.init()
    }

    init(dict : [String:AnyObject]){

        if let spnsrID = dict[kSponsorID] as? String{
            self.sponserID = spnsrID
        }

        if let dictionary  = dict["memberInfo"] as? [String: AnyObject]{
            if let mId = dictionary[kID] as? NSInteger{
                self.ID = "\(mId)"
            }else if let mId = dictionary[kID] as? String{
                self.ID = mId
            }
            if let mName = dictionary[kName] as? String{
                self.name = mName
            }
            if let mCountry = dictionary[kCountry] as? String{
                self.country = mCountry
            }
            if let mjoinat = dictionary[KJoitAt] as? String{
                self.joinAt = mjoinat
            }
            if let mglevel = dictionary[kGLevel] as? String{
                self.gLevel = mglevel
            }else if let mglevel = dictionary[kGLevel] as? Int{
                self.gLevel = "\(mglevel)"
            }
            if let mrank = dictionary[kRank] as? String{
                self.rank = mrank
            }else if let mrank = dictionary[kRank] as? Int{
                self.rank = "\(mrank)"
            }
            if let mstatus = dictionary[kStatus] as? String{
                self.status = mstatus
            }
            if let mgoldpacks = dictionary[kGoldPacks] as? String{
                self.goldPacks = mgoldpacks
            }else if let mgoldpacks = dictionary[kGoldPacks] as? Int{
                self.goldPacks = "\(mgoldpacks)"
            }
            if let msilverpacks = dictionary[kSilverPacks] as? String{
                self.silverPacks = msilverpacks
            }else if let msilverpacks = dictionary[kSilverPacks] as? Int{
                self.silverPacks = "\(msilverpacks)"
            }
            if let musers = dictionary[kUsers] as? String{
                self.users = musers
            }
            if let memail = dictionary[kEmail] as? String{
                self.email = memail
            }
            if let msponsoruserid = dictionary[kSponsorUserID] as? String{
                self.sponsorUserID = msponsoruserid
            }
            if let msponsorusername = dictionary[kSponsorUserName] as? String{
                self.sponsorUserName = msponsorusername
            }
            if let musername = dictionary[kUserName] as? String{
                self.userName = musername
            }
            if let mmobile = dictionary[kMobile] as? String{
                self.mobile = mmobile
            }
        }else if let dictionary  = dict["member"] as? [String: AnyObject]{
            if let mId = dictionary[kID] as? NSInteger{
                self.ID = "\(mId)"
            }else if let mId = dictionary[kID] as? String{
                self.ID = mId
            }
            if let mName = dictionary[kName] as? String{
                self.name = mName
            }
            if let mCountry = dictionary[kCountry] as? String{
                self.country = mCountry
            }
            if let mjoinat = dictionary[KJoitAt] as? String{
                self.joinAt = mjoinat
            }
            if let mglevel = dictionary[kGLevel] as? String{
                self.gLevel = mglevel
            }else if let mglevel = dictionary[kGLevel] as? Int{
                self.gLevel = "\(mglevel)"
            }
            if let mrank = dictionary[kRank] as? String{
                self.rank = mrank
            }else if let mrank = dictionary[kRank] as? Int{
                self.rank = "\(mrank)"
            }
            if let mstatus = dictionary[kStatus] as? String{
                self.status = mstatus
            }
            if let mgoldpacks = dictionary[kGoldPacks] as? String{
                self.goldPacks = mgoldpacks
            }else if let mgoldpacks = dictionary[kGoldPacks] as? Int{
                self.goldPacks = "\(mgoldpacks)"
            }
            if let msilverpacks = dictionary[kSilverPacks] as? String{
                self.silverPacks = msilverpacks
            }else if let msilverpacks = dictionary[kSilverPacks] as? Int{
                self.silverPacks = "\(msilverpacks)"
            }
            if let musers = dictionary[kUsers] as? String{
                self.users = musers
            }
            if let memail = dictionary[kEmail] as? String{
                self.email = memail
            }
            if let msponsoruserid = dictionary[kSponsorUserID] as? String{
                self.sponsorUserID = msponsoruserid
            }
            if let msponsorusername = dictionary[kSponsorUserName] as? String{
                self.sponsorUserName = msponsorusername
            }
            if let musername = dictionary[kUserName] as? String{
                self.userName = musername
            }
            if let mmobile = dictionary[kMobile] as? String{
                self.mobile = mmobile
            }
        }
        super.init()
    }
}

class MemberModelParser: NSObject {
    //keys
    let kFlagURL = "flagUrl"
    let kMaxUsers = "maxusers"
    let kMaxLevel = "maxlevel"
    let kMaxGoldPacks = "maxgoldpacks"
    let kMaxSilverPacks = "maxsilverpacks"
    let kTotalUsers = "totalusers"
    let kMembers = "members"
    //values
    var flagURL : String = ""
    var maxUsers : String = ""
    var maxLevel : String = ""
    var maxGoldPacks : String = ""
    var maxSilverPacks : String = ""
    var totalUsers : String = ""
    var members = [MemberModel]()

    init(json : JSON){
        if let flagurl = json[kFlagURL].stringValue as String?{
            self.flagURL = flagurl
        }
        if let mxusers = json[kMaxUsers].stringValue as String?{
            self.maxUsers = mxusers
        }
        if let mxlevels = json[kMaxLevel].stringValue as String?{
            self.maxLevel = mxlevels
        }
        if let mxGoldpacks = json[kMaxGoldPacks].stringValue as String?{
            self.maxGoldPacks = mxGoldpacks
        }
        if let mxsilverpacks = json[kMaxSilverPacks].stringValue as String?{
            self.maxSilverPacks = mxsilverpacks
        }
        if let totalusrs = json[kTotalUsers].stringValue as String?{
            self.totalUsers = totalusrs
        }

        if let membersJSON  = json[kMembers] as JSON?{
            self.members.removeAll()
            for (_, subJSON) in membersJSON{
                 let member = MemberModel(json: subJSON)
                self.members.append(member)
            }
        }
        super.init()
    }

}
