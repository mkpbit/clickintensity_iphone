//
//  ProspectResponse.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/01/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ProspectResponse: NSObject {
    let kProspectID = "prospectID"
    let kName = "name"
    let kEmail = "email"
    let kMobile = "mobile"
    let kResponseType = "responseType"
    let kDate = "sdate"
    let kTime = "stime"
    let kComment = "comment"
    let kCreatedAt = "createdAt"

    var prospectID:String = ""
    var name:String = ""
    var email:String = ""
    var mobile:String = ""
    var responseType:String = ""
    var date:String = ""
    var time:String = ""
    var comment:String = ""
    var createdAt : NSDate = NSDate()
    var scheduledDate : NSDate?

    override init() {
        super.init()
    }
}
