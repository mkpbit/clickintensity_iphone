//
//  ProfilePicNewProspectCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class ProfilePicNewProspectCell: UITableViewCell {
    @IBOutlet weak var profileImageView : UIImageView!
    @IBOutlet weak var addProfilePicButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.profileImageView, borderColor: UIColor(red: 75.0/255.0, green: 90.0/255.0, blue: 104.0/255.0, alpha: 1.0), borderWidth: 0.5, cornerRadius: self.profileImageView.frame.size.height/2)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
