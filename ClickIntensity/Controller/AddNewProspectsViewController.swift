//
//  AddNewProspectsViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import KVNProgress
class AddNewProspectsViewController: UIViewController {
    let kName = "name"
    let kFirstName = "fname"
    let kLastName = "lname"
    let kEmail = "email"
    let kMobile = "mobile"
    let kActive = "active"
    let kStatus = "status"
    let kProfilePic = "profilePic"
    var user = UserModel()
    let placeHolderArr = ["","First Name","Last Name", "Email","Mobile Number","Status(Hot/Cool)"]
    @IBOutlet weak var prospectTableView: UITableView!
    var imagePickerController : UIImagePickerController!
    var profileImage = UIImage(named: "prospect_dp")
    var params = [String:String]()
    var status = true
    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = UserModel.loadUserInfo()
        self.user = UserModel(json: userjson)
        params = ["fname":"","lname":"","email":"","mobile":"","active":"","status":""]
        prospectTableView.setNeedsLayout()
        prospectTableView.layoutIfNeeded()
    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    override func viewDidLayoutSubviews() {
        prospectTableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickDoneTick(sender:UIBarButtonItem){
        self.createProspectWith(self.params)
    }

    @IBAction func onClickBackButton(sender:UIBarButtonItem){
        self.navigationController?.popViewControllerAnimated(true)
    }

    func createProspectWith(params:[String:String]){
        var myParams = [String:String]()
        //        {"userid": "584a8d5ff617ecf329d9751f", "name" : "Arvnd Kumar", "email":"abcd@gmail.com", "mobile" : "+91-9876543210" }
        // var name =  params["fname"]
        myParams.updateValue("\(params["fname"]!) \(params["lname"]!)", forKey: "name")
        myParams.updateValue(params["email"]! , forKey: "email")
        myParams.updateValue(params["mobile"]!, forKey: "mobile")
        myParams.updateValue(self.user.ID, forKey: "userid")


        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
        ProspectService.sharedInstance.createProspectsWith(token, params:myParams, completionBlock: { (response) in
            if let _ = response as AnyObject?{
                KVNProgress.showSuccessWithStatus("Done", completion: {
                    self.navigationController?.popViewControllerAnimated(true)
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = mainStoryboard.instantiateViewControllerWithIdentifier("MyProspectViewController")
                    SlideNavigationController.sharedInstance().popToRootAndSwitchToViewController(vc, withSlideOutAnimation: true, andCompletion: nil)
                })
            }
        })
        }
    }
}

extension AddNewProspectsViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeHolderArr.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCellWithIdentifier("ProfilePicNewProspectCell", forIndexPath: indexPath) as! ProfilePicNewProspectCell
            cell.profileImageView.image = self.profileImage
            cell.addProfilePicButton.addTarget(self, action: #selector(onClickFileAddImageButton(_:)), forControlEvents: .TouchUpInside)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCellWithIdentifier("SwitchCell", forIndexPath: indexPath) as! SwitchCell
            cell.mySwitch.on = status
            cell.mySwitch.addTarget(self, action: #selector(onStatusChanged(_:)), forControlEvents: .ValueChanged)
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("TextFieldCell", forIndexPath: indexPath) as! TextFieldCell

            switch indexPath.row {
            case 1:
                cell.textField.keyboardType = .Default
                cell.textField.autocapitalizationType = .Words
            case 2:
                cell.textField.keyboardType = .Default
                cell.textField.autocapitalizationType = .Words

            case 3:
                cell.textField.keyboardType = .EmailAddress
                cell.textField.autocapitalizationType = .None

            case 4:
                cell.textField.keyboardType = .PhonePad
                cell.textField.autocapitalizationType = .Words

            default:
                cell.textField.keyboardType = .Default
                cell.textField.autocapitalizationType = .Words

            }

            CommonClass.setPlaceHolder(cell.textField, placeHolderString: placeHolderArr[indexPath.row], withColor: UIColor(red: 69.0/255.0, green: 84.0/255.0, blue: 97.0/255.0, alpha: 1.0))
            cell.textField.addTarget(self, action: #selector(textDidChange(_:)), forControlEvents: .EditingChanged)
            cell.textField.delegate = self
            return cell
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var heightOfRow : CGFloat = 0
        if indexPath.row == 0{
            heightOfRow = 130
        }else{
            heightOfRow = 44
        }
        return heightOfRow
    }

    @IBAction func onStatusChanged(sender: UISwitch){
        self.status = sender.on
        params.updateValue(self.status ? "cold" : "hot", forKey: kStatus)
    }
}

extension AddNewProspectsViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(prospectTableView) as NSIndexPath?{
            switch indexPath.row {
            case 1:
                params.updateValue(textField.text!, forKey: kFirstName)
            case 2:
                params.updateValue(textField.text!, forKey: kLastName)
            case 3:
                params.updateValue(textField.text!, forKey: kEmail)
            case 4:
                params.updateValue(textField.text!, forKey: kMobile)
            default:
                break
            }
        }
    }

    @IBAction func textDidChange(textField:UITextField){
        if let indexPath = textField.tableViewIndexPath(prospectTableView) as NSIndexPath?{
            switch indexPath.row {
            case 1:
                params.updateValue(textField.text!, forKey: kFirstName)
            case 2:
                params.updateValue(textField.text!, forKey: kLastName)
            case 3:
                params.updateValue(textField.text!, forKey: kEmail)
            case 4:
                params.updateValue(textField.text!, forKey: kMobile)
            default:
                break
            }
        }

    }

    func textFieldDidEndEditing(textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(prospectTableView) as NSIndexPath?{
            switch indexPath.row {
            case 1:
                params.updateValue(textField.text!, forKey: kFirstName)
            case 2:
                params.updateValue(textField.text!, forKey: kLastName)
            case 3:
                params.updateValue(textField.text!, forKey: kEmail)
            case 4:
                params.updateValue(textField.text!, forKey: kMobile)
            default:
                break
            }
        }

    }
}

extension AddNewProspectsViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    @IBAction func onClickFileAddImageButton(sender:UIButton){
        let actionSheetForCall = UIAlertController(title: "Choose Option", message: "", preferredStyle: .ActionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetForCall.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Gallery", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
                self.imagePickerController.allowsEditing = true
                self.presentViewController(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheetForCall.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera;
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
                self.imagePickerController.allowsEditing = true
                self.presentViewController(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheetForCall.addAction(openCameraAction)
        self.presentViewController(actionSheetForCall, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.profileImage = tempImage
            //self.params.updateValue(tempImage, forKey: kProfilePic)
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.profileImage = tempImage
            // self.params.updateValue(tempImage, forKey: kProfilePic)
        }
        picker.dismissViewControllerAnimated(true, completion: {
            self.prospectTableView.reloadData()
        })
    }
}