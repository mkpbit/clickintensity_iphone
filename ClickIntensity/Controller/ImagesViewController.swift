//
//  ImagesViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 15/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class ImagesViewController: UIViewController,SlideNavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
