//
//  AddProspectCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 07/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//HeaderViewCell

import UIKit

class AddProspectCell: UITableViewCell {
    @IBOutlet weak var userImageView : UIImageView!
    @IBOutlet weak var radioButton : UIButton!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var secondaryTextLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // self.radioButton.selected = selected
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircular(userImageView, borderColor: UIColor.grayColor(), borderWidth: 4)
    }
    
}


class HeaderViewCell: UITableViewCell {
    @IBOutlet weak var addNewButton : UIButton!
    @IBOutlet weak var iconImageView : UIImageView!

    override func prepareForReuse() {
        addNewButton.setTitle("", forState: .Normal)
        iconImageView.image = nil
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override func layoutSubviews() {
        //  CommonClass.makeViewCircular(userImageView, borderColor: UIColor.clearColor(), borderWidth: 6)
    }
    
}
