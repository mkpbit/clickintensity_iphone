//
//  UserModel.swift
//  ClickIntensity
//
//  Created by TecOrb on 14/07/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserModel: NSObject {
    let kRole = "role"
    let kCategories = "categories"
    let kCreatDate = "createdat"
    let kAdvCashEdit = "advcashedit"
    let kPayzaEdit = "payzaedit"
    let kBitCoinEdit = "bitcoinedit"
    let kIsBlocked = "isBlocked"
    let kConfirmSponsor = "confirmSponsor"
    let kPersonalDoc = "personaldoc"
    let kPhotoID = "photoid"
    let kBitcoin = "bitcoin"
    let kBitCoinScreenShot = "bitcoinscreenshot"
    let kExpiryTime = "expiryTime"
    let kCurrentCXView = "currentcxview"
    let kAdvCash = "advcash"
    let kAccountName = "accountname"
    let kAccountNumber = "accountno"
    let kBankName = "bankname"
    let kBranch = "branch"
    let kCode = "code"
    let kGovidTaxID = "govidtaxid"
    let kQuickyPay = "quickypay"
    let kPayza = "payza"
    let kPayPal = "paypal"
    let kAvatar = "avatar"
    let kSponser = "sponsor"
    let kVarified = "verified"
    let kCity = "city"
    let kUserName = "username"
    let kPinCode = "pincode"
    let kState = "state"
    let kAddress = "address"
    let kMobile = "mobile"
    let kName = "name"
    let kCountryName = "countryName"
    let kCountryCode = "countryCode"
    let kIP = "ip"
    let kEmail = "email"
    let kProvider = "provider"
    let kUserProfileID = "userProfileId"
    let kID = "_id"
    let kCurrentTime = "currentTime"

    //Proerties
    var role = ""
    var categories = [String]()
    var creatDate = ""
    var advCashEdit = false
    var payzaEdit = false
    var bitCoinEdit = false
    var isBlocked = false

    var confirmSponsor = false
    var personalDoc : String = ""
    var photoID : String = ""
    var bitcoin : String = ""
    var bitCoinScreenShot : String = ""
    var expiryTime : String = ""
    var currentCXView : String = ""
    var advCash : String = ""
    var accountName : String = ""
    var accountNumber : String = ""
    var bankName : String = ""
    var branch : String = ""
    var code : String = ""
    var govidTaxID : String = ""
    var quickyPay : String = ""
    var payza : String = ""
    var payPal : String = ""
    var avatar : String = ""
    var sponser : String = ""
    var varified : Bool = false
    var city : String = ""
    var userName : String = ""
    var pinCode : String = ""
    var state : String = ""
    var address : String = ""
    var mobile : String = ""
    var name : String = ""
    var countryName : String = ""
    var countryCode: String = ""
    var IP : String = ""
    var email : String = ""
    var provider : String = ""
    var userProfileID: String = ""
    var ID : String = ""
    var currentTime : String = ""

    override init() {
        super.init()
    }

    init(json : JSON){
        if let dictionary  = json["user"].dictionaryObject as [String: AnyObject]?{

            if let userId = dictionary[kID] as? NSInteger{
                self.ID = "\(userId)"
            }else if let userId = dictionary[kID] as? String{
                self.ID = userId
            }

            if let email = dictionary[kEmail] as? String{
                self.email = email
            }

            if let r = dictionary[kRole] as? String{
                self.role = r
            }

            if let cats = dictionary[kCategories] as? [String]{
                self.categories = [String]()
                for cat in cats{
                    self.categories.append(cat)
                }
            }

            if let advCE = dictionary[kAdvCashEdit] as? Bool{
                self.advCashEdit = advCE
            }
            if let pEdit = dictionary[kPayzaEdit] as? Bool{
                self.payzaEdit = pEdit
            }
            if let bcEdit = dictionary[kBitCoinEdit] as? Bool{
                self.bitCoinEdit = bcEdit
            }
            if let isblocked = dictionary[kIsBlocked] as? Bool{
                self.isBlocked = isblocked
            }
            if let cnfrmSpomser = dictionary[kConfirmSponsor] as? Bool{
                self.confirmSponsor = cnfrmSpomser
            }

            if let pd = dictionary[kPersonalDoc] as? String{
                self.personalDoc = pd
            }
            if let photoid = dictionary[kPhotoID] as? String{
                self.photoID = photoid
            }
            if let bitcoin = dictionary[kBitcoin] as? String{
                self.bitcoin = bitcoin
            }
            if let bitcoinScrShot = dictionary[kBitCoinScreenShot] as? String{
                self.bitCoinScreenShot = bitcoinScrShot
            }
            if let expTime = dictionary[kExpiryTime] as? String{
                self.expiryTime = expTime
            }
            if let crntCXV = dictionary[kCurrentCXView] as? String{
                self.currentCXView = crntCXV
            }
            if let advcash = dictionary[kAdvCash] as? String{
                self.advCash = advcash
            }
            if let acntName = dictionary[kAccountName] as? String{
                self.accountName = acntName
            }
            if let acntNumber = dictionary[kAccountNumber] as? String{
                self.accountNumber = acntNumber
            }
            if let photoid = dictionary[kPhotoID] as? String{
                self.photoID = photoid
            }
            if let bankname = dictionary[kBankName] as? String{
                self.bankName = bankname
            }
            if let brnch = dictionary[kBranch] as? String{
                self.branch = brnch
            }
            if let code = dictionary[kCode] as? String{
                self.code = code
            }

            if let govidtaxid = dictionary[kGovidTaxID] as? String{
                self.govidTaxID = govidtaxid
            }
            if let payza = dictionary[kPayza] as? String{
                self.payza = payza
            }
            if let paypal = dictionary[kPayPal] as? String{
                self.payPal = paypal
            }

            if let avtar = dictionary[kAvatar] as? String{
                self.avatar = avtar
            }
            if let sponser = dictionary[kSponser] as? String{
                self.sponser = sponser
            }
            if let varified = dictionary[kVarified] as? Bool{
                self.varified = varified
            }
            if let cty = dictionary[kCity] as? String{
                self.city = cty
            }
            if let uname = dictionary[kUserName] as? String{
                self.userName = uname
            }
            if let pincode = dictionary[kPinCode] as? String{
                self.pinCode = pincode
            }
            if let stt = dictionary[kState] as? String{
                self.state = stt
            }
            if let addrss = dictionary[kAddress] as? String{
                self.address = addrss
            }
            if let mbl = dictionary[kMobile] as? String{
                self.mobile = mbl
            }
            if let nm = dictionary[kName] as? String{
                self.name = nm
            }
            if let cName = dictionary[kCountryName] as? String{
                self.countryName = cName
            }
            if let ccode = dictionary[kCountryCode] as? String{
                self.countryCode = ccode
            }
            if let ip = dictionary[kIP] as? String{
                self.IP = ip
            }
            if let provider = dictionary[kProvider] as? String{
                self.provider = provider
            }
            if let uPID = dictionary[kUserProfileID] as? String{
                self.userProfileID = uPID
            }else  if let uPID = dictionary[kUserProfileID] as? NSInteger{
                self.userProfileID = "\(uPID)"
            }
            if let ctime = dictionary[currentTime] as? String{
                self.currentTime = ctime
            }
            
            if let createDate = dictionary[kCreatDate] as? String{
                self.creatDate = createDate
            }
        }
        super.init()
    }

    init(dictionary : [String:AnyObject]){

        if let userId = dictionary[kID] as? NSInteger{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }

        if let email = dictionary[kEmail] as? String{
            self.email = email
        }

        if let r = dictionary[kRole] as? String{
            self.role = r
        }

        if let cats = dictionary[kCategories] as? [String]{
            self.categories = [String]()
            for cat in cats{
                self.categories.append(cat)
            }
        }
        if let advCE = dictionary[kAdvCashEdit] as? Bool{
            self.advCashEdit = advCE
        }
        if let pEdit = dictionary[kPayzaEdit] as? Bool{
            self.payzaEdit = pEdit
        }
        if let bcEdit = dictionary[kBitCoinEdit] as? Bool{
            self.bitCoinEdit = bcEdit
        }
        if let isblocked = dictionary[kIsBlocked] as? Bool{
            self.isBlocked = isblocked
        }
        if let cnfrmSpomser = dictionary[kConfirmSponsor] as? Bool{
            self.confirmSponsor = cnfrmSpomser
        }

        if let pd = dictionary[kPersonalDoc] as? String{
            self.personalDoc = pd
        }
        if let photoid = dictionary[kPhotoID] as? String{
            self.photoID = photoid
        }
        if let bitcoin = dictionary[kBitcoin] as? String{
            self.bitcoin = bitcoin
        }
        if let bitcoinScrShot = dictionary[kBitCoinScreenShot] as? String{
            self.bitCoinScreenShot = bitcoinScrShot
        }
        if let expTime = dictionary[kExpiryTime] as? String{
            self.expiryTime = expTime
        }
        if let crntCXV = dictionary[kCurrentCXView] as? String{
            self.currentCXView = crntCXV
        }
        if let advcash = dictionary[kAdvCash] as? String{
            self.advCash = advcash
        }
        if let acntName = dictionary[kAccountName] as? String{
            self.accountName = acntName
        }
        if let acntNumber = dictionary[kAccountNumber] as? String{
            self.accountNumber = acntNumber
        }
        if let photoid = dictionary[kPhotoID] as? String{
            self.photoID = photoid
        }
        if let bankname = dictionary[kBankName] as? String{
            self.bankName = bankname
        }
        if let brnch = dictionary[kBranch] as? String{
            self.branch = brnch
        }
        if let code = dictionary[kCode] as? String{
            self.code = code
        }

        if let govidtaxid = dictionary[kGovidTaxID] as? String{
            self.govidTaxID = govidtaxid
        }
        if let payza = dictionary[kPayza] as? String{
            self.payza = payza
        }
        if let paypal = dictionary[kPayPal] as? String{
            self.payPal = paypal
        }

        if let avtar = dictionary[kAvatar] as? String{
            self.avatar = avtar
        }
        if let sponser = dictionary[kSponser] as? String{
            self.sponser = sponser
        }
        if let varified = dictionary[kVarified] as? Bool{
            self.varified = varified
        }
        if let cty = dictionary[kCity] as? String{
            self.city = cty
        }
        if let uname = dictionary[kUserName] as? String{
            self.userName = uname
        }
        if let pincode = dictionary[kPinCode] as? String{
            self.pinCode = pincode
        }
        if let stt = dictionary[kState] as? String{
            self.state = stt
        }
        if let addrss = dictionary[kAddress] as? String{
            self.address = addrss
        }
        if let mbl = dictionary[kMobile] as? String{
            self.mobile = mbl
        }
        if let nm = dictionary[kName] as? String{
            self.name = nm
        }
        if let cName = dictionary[kCountryName] as? String{
            self.countryName = cName
        }
        if let ccode = dictionary[kCountryCode] as? String{
            self.countryCode = ccode
        }
        if let ip = dictionary[kIP] as? String{
            self.IP = ip
        }
        if let provider = dictionary[kProvider] as? String{
            self.provider = provider
        }
        
        if let uPID = dictionary[kUserProfileID] as? String{
            self.userProfileID = uPID
        }else  if let uPID = dictionary[kUserProfileID] as? NSInteger{
            self.userProfileID = "\(uPID)"
        }

        if let ctime = dictionary[currentTime] as? String{
            self.currentTime = ctime
        }
        if let createDate = dictionary[kCreatDate] as? String{
            self.creatDate = createDate
        }
        super.init()
    }

    func saveUserInfo(user:UserModel) {
        let documentPath = NSHomeDirectory() + "/Documents/"
        var userInfo = [String:AnyObject]()
        userInfo.updateValue(user.role, forKey:kRole)
        userInfo.updateValue(user.categories, forKey:kCategories)
        userInfo.updateValue(user.creatDate, forKey:kCreatDate)
        userInfo.updateValue(user.advCashEdit, forKey:kAdvCashEdit)
        userInfo.updateValue(user.payzaEdit, forKey:kPayzaEdit)
        userInfo.updateValue(user.bitCoinEdit, forKey:kBitCoinEdit)
        userInfo.updateValue(user.isBlocked, forKey:kIsBlocked)
        userInfo.updateValue(user.confirmSponsor, forKey:kConfirmSponsor)
        userInfo.updateValue(user.personalDoc, forKey:kPersonalDoc)
        userInfo.updateValue(user.photoID, forKey:kPhotoID)
        userInfo.updateValue(user.bitcoin, forKey:kBitcoin)
        userInfo.updateValue(user.bitCoinScreenShot, forKey:kBitCoinScreenShot)
        userInfo.updateValue(user.expiryTime, forKey:kExpiryTime)
        userInfo.updateValue(user.currentCXView, forKey:kCurrentCXView)
        userInfo.updateValue(user.advCash, forKey:kAdvCash)
        userInfo.updateValue(user.accountName, forKey:kAccountName)
        userInfo.updateValue(user.accountNumber, forKey:kAccountNumber)
        userInfo.updateValue(user.bankName, forKey:kBankName)
        userInfo.updateValue(user.branch, forKey:kBranch)
        userInfo.updateValue(user.code, forKey:kCode)
        userInfo.updateValue(user.govidTaxID, forKey:kGovidTaxID)
        userInfo.updateValue(user.quickyPay, forKey:kQuickyPay)
        userInfo.updateValue(user.payza, forKey:kPayza)
        userInfo.updateValue(user.payPal, forKey:kPayPal)
        userInfo.updateValue(user.avatar, forKey:kAvatar)
        userInfo.updateValue(user.sponser, forKey:kSponser)
        userInfo.updateValue(user.varified, forKey:kVarified)
        userInfo.updateValue(user.city, forKey:kCity)
        userInfo.updateValue(user.userName, forKey:kUserName)
        userInfo.updateValue(user.pinCode, forKey:kPinCode)
        userInfo.updateValue(user.state, forKey:kState)
        userInfo.updateValue(user.address, forKey:kAddress)
        userInfo.updateValue(user.mobile, forKey:kMobile)
        userInfo.updateValue(user.name, forKey:kName)
        userInfo.updateValue(user.countryName, forKey:kCountryName)
        userInfo.updateValue(user.countryCode, forKey:kCountryCode)
        userInfo.updateValue(user.IP, forKey:kIP)
        userInfo.updateValue(user.email, forKey:kEmail)
        userInfo.updateValue(user.provider, forKey:kProvider)
        userInfo.updateValue(user.userProfileID, forKey:kUserProfileID)
        userInfo.updateValue(user.ID, forKey:kID)
        userInfo.updateValue(user.currentTime, forKey:kCurrentTime)
        let map = ["user":userInfo]
        do {
            let data = try JSON(map).rawData(options: [.PrettyPrinted])
            let path = documentPath + "user"
            data.writeToFile(path, atomically: true)
        }catch{
        }
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    class func loadUserInfo()->JSON {
        let documentPath = NSHomeDirectory() + "/Documents/"
        let path = documentPath + "user"
        if let data = NSData(contentsOfFile: path) {
            let json = JSON(data: data)
            return json
        }
        return JSON(data: NSData())
    }

}
