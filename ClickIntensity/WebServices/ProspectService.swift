//
//  ProspectService.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import TSMessages
import KVNProgress
import SwiftyJSON

class ProspectService {
    static let sharedInstance = ProspectService()
    private init(){}
    func createProspectsWith(token:String,params:[String:AnyObject] ,completionBlock:(AnyObject?) -> Void){
        CommonClass.showLoaderWithStatus("Creating..")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        //head.updateValue("application/json", forKey: "Content-Type")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        print_debug("Hitting url : \(CREATE_PROSPECT) \n with headers: \(head) \n and params :\(params)")

        Alamofire.request(.POST, CREATE_PROSPECT ,parameters:params,encoding: .JSON, headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("prospect json is \n \(json)")

                    if let userArray = json.arrayObject as? [[String:AnyObject]]{
                        var uArray = [UserModel]()
                        for user in userArray{
                            uArray.append(UserModel(dictionary: user))
                        }
                        completionBlock(uArray)
                    }else if let dict = json.dictionaryObject as [String:AnyObject]?{
                        let user = UserModel(dictionary: dict)
                        completionBlock(user)
                    }

                }else{
                    showErrorWithMessage("Couldn't access Dashboard")
                    completionBlock(nil)
                }

            case .Failure(let error):
                completionBlock(nil)
                showErrorWithMessage(error.localizedFailureReason!)
            }
        }
    }

    func getProspectsListWith(token:String,pageNumber:Int,completionBlock:(AnyObject?,rowsCount:Int) -> Void){
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        let param = ["page":"\(pageNumber)"]

        print_debug("Hitting url : \(LIST_PROSPECT) \n with headers: \(head) \n and params :\(param)")

        Alamofire.request(.GET, LIST_PROSPECT ,parameters:param,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("prospect json is \n \(json)")
                    let prospectParser = ProspectParser(json: json)
                    completionBlock(prospectParser.prospects, rowsCount: prospectParser.rowsCount)

                }else{
                    showErrorWithMessage("Couldn't access Prospects")
                    completionBlock(nil,rowsCount: 0)
                }
                
            case .Failure(let error):
                completionBlock(nil,rowsCount: 0)
                showErrorWithMessage(error.localizedFailureReason ?? "")
            }
        }
    }

    func getResourceVideosWith(token:String,pageNumber:Int,subType:String,completionBlock:(AnyObject?,rowsCount:Int) -> Void){
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        let param = ["page":"\(pageNumber)","subtype":subType,"type":"video"]
        print_debug("Hitting url : \(RESOURCE_VIDEO_URL) \n with headers: \(head) \n and params :\(param)")

        Alamofire.request(.GET, RESOURCE_VIDEO_URL ,parameters:param,headers: head).responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("videos json is \n \(json)")
                    let videoParser = VideoResourceParser(json: json)
                    completionBlock(videoParser.videos, rowsCount: videoParser.videos.count)
                }else{
                    showErrorWithMessage("Couldn't accessed")
                    completionBlock(nil,rowsCount: 0)
                }

            case .Failure(let error):
                completionBlock(nil,rowsCount: 0)
                showErrorWithMessage(error.localizedFailureReason ?? "")
            }
        }
    }


}
