//
//  ForgotPasswordViewController.swift
//  Sinr
//
//  Created by TecOrb on 27/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var okayButton : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default

    }
    override func viewDidLayoutSubviews() {
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickCancel(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onClickForgotPassword(sender:UIButton){
         showErrorWithMessage(FUNCTIONALITY_PENDING_MESSAGE)
//        if let email = emailTextField.text{
//            if CommonClass.validateEmail(email){
//                if !CommonClass.isConnectedToNetwork{
//                    showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
//                    return
//                }
//                CommonClass.showLoader()
//                LoginService.forgotPasswordForEmail(email, completionBlock: { response in
//                    CommonClass.hideLoader()
//                    if response != nil{
//                        let json = JSON(response!)
//                        print("JSON: \(json)")
//                        showSuccessWithMessage("\(json)")
//                    }
//                })
//            }else{
//                showErrorWithMessage("Email is not correct")
//            }
//        }else{
//            showErrorWithMessage("Email can't be empty")
//        }
    }

}
