//
//  NotificationViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class NotificationViewController : UIViewController,SlideNavigationControllerDelegate {
    @IBOutlet weak var notificationTableView : UITableView!
    var notificationArray = [Notification]()
    var totalCount:Int = 0
    var pageNumber = 1
    var isNewDataLoading = false

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(TeamViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        return refreshControl
    }()

    func handleRefresh(refreshControl: UIRefreshControl) {
        pageNumber = 1
        notificationArray.removeAll()
        isNewDataLoading = true
        notificationTableView.reloadData()
        self.loadNotificationsWith(self.pageNumber)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.notificationTableView.addSubview(refreshControl)
        self.loadNotificationsWith(self.pageNumber)
        // Do any additional setup after loading the view.

    }

    func loadNotificationsWith(pageNumber:Int){
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            DashBoardService.sharedInstance.getAllNotificationsWithToken(token, isViewed: true, type: nil, pageNumber: self.pageNumber, completionBlock: { (notificationsResponse, total) in
                self.isNewDataLoading = false
                if self.refreshControl.refreshing{
                    self.refreshControl.endRefreshing()
                }
                self.totalCount = total
                if let newArray = notificationsResponse as? [Notification]{
                    self.notificationArray.appendContentsOf(newArray)
                    self.notificationTableView.reloadData()
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let notification = notificationArray[indexPath.row]
        if let cell = tableView.dequeueReusableCellWithIdentifier("NotificationCell", forIndexPath: indexPath) as? NotificationCell{
            cell.notificationTextLabel.text = notification.subject.capitalizedString
            cell.userNameLabel.text = notification.sender.name.capitalizedString
            return cell
        }
        return NotificationCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let notification = notificationArray[indexPath.row]

        let popup =  PopupController
            .create(self)
        let container = self.storyboard?.instantiateViewControllerWithIdentifier("NotificationDetailsViewController") as! NotificationDetailsViewController
        container.notification = notification
        container.size = self.view.frame.size


        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(container)
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if !(notificationArray.count<=totalCount){
            return
        }
        if scrollView == notificationTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if pageNumber >= 1{
                            pageNumber+=1
                        }

                        if let _ = kUserDefaults.valueForKey(kUserToken) as? String{
                            self.loadNotificationsWith(self.pageNumber)
                        }
                    }
                }
            }
        }
    }


    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
