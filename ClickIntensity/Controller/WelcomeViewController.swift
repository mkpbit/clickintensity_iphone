//
//  WelcomeViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 01/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController,UIScrollViewDelegate,InfinitePagingViewDelegate {
    @IBOutlet weak var aScrollView: UIScrollView!
    @IBOutlet weak var aPageControl: UIPageControl!
    var myTimer: NSTimer? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        aScrollView.delegate = self
        self.navigationController?.navigationBar.hidden = true
        self.pagingSetUp()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        //timer funtioning start
        myTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(gotoNextPage), userInfo: nil, repeats: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func pagingSetUp()
    {
        aScrollView.contentSize = CGSizeMake(self.view.frame.size.width * 4, self.aScrollView.bounds.height)
        aScrollView.pagingEnabled = true
        for i in 0 ..< 4
        {
            var frame : CGRect = aScrollView.frame
            frame.origin.x = self.view.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size.width = self.aScrollView.bounds.size.width
            frame.size.height = self.aScrollView.bounds.size.height
            let page : UIImageView = UIImageView()
            page.frame = frame
            page.autoresizingMask = [.FlexibleWidth,.FlexibleHeight]
            switch (i)
            {
            case 0:
                page.image = UIImage(named: "ci1.png")
                page.contentMode = UIViewContentMode.ScaleToFill
                aScrollView.addSubview(page)
            case 1:
                page.image = UIImage(named: "ci2.png")
                page.contentMode = UIViewContentMode.ScaleToFill
                aScrollView.addSubview(page)
            case 2:
                page.image = UIImage(named: "ci3.png")
                page.contentMode = UIViewContentMode.ScaleToFill
                aScrollView.addSubview(page)
            case 3:
                let signInVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
                self.addChildViewController(signInVC)
                let p = signInVC.view
                p.frame = frame
                aScrollView.addSubview(p)
            default:
                break;
            }

        }
        aScrollView.contentOffset = CGPointMake(0, aScrollView.frame.origin.y);
        self.view.bringSubviewToFront(aPageControl)
        aPageControl.addTarget(self, action: #selector(changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }

    //MARK: -InfinitePagingViewDelegate
    func pagingView(pagingView: InfinitePagingView!, didEndDecelerating scrollView: UIScrollView!, atPageIndex pageIndex: Int) {
        self.aPageControl.currentPage = pageIndex
    }

    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(aPageControl.currentPage) * aScrollView.frame.size.width
        self.aScrollView.setContentOffset(CGPointMake(x, 0), animated: true)
        let pageNumber = round(aScrollView.contentOffset.x / aScrollView.frame.size.width)
        if pageNumber == 3{
            aPageControl.hidden = true
        }
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        if pageNumber == 3{
            aPageControl.hidden = true
        }
        self.aPageControl.currentPage = Int(pageNumber)
    }

    func gotoNextPage() {
        var pageNumber = aPageControl.currentPage
        if pageNumber <= 2 {
            pageNumber = pageNumber + 1
            if pageNumber == 3{
                aPageControl.hidden = true
            }
            self.aPageControl.currentPage = Int(pageNumber)
            self.changePage(aPageControl)
        }else{
            aPageControl.hidden = true
            //pageNumber = 0
            //self.aPageControl.currentPage = Int(pageNumber)
            // self.changePage(aPageControl)
            myTimer?.invalidate()
        }

    }
}
