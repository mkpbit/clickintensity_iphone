//
//  DashBoardService.swift
//  ClickIntensity
//
//  Created by TecOrb on 16/07/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import TSMessages
import KVNProgress
import SwiftyJSON

enum ListOrder:String{
    case Ascending = "ASC"
    case Descending = "DESC"
}

class DashBoardService {

    static let sharedInstance = DashBoardService()
    private init(){}

    func getFrontLineAndTeamCountWithToken(token:String,completionBlock:(frontLineCount:Int,teamCount:Int) -> Void){
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        let param = ["TIME_STRING":"all"]

        print_debug("Hitting url : \(TEAM_FRONTLINE_COUNT) \n with headers: \(head) \n and params : \(param)")

        Alamofire.request(.GET, TEAM_FRONTLINE_COUNT,parameters:param,headers: head).responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Response of url : \(TEAM_FRONTLINE_COUNT) \n \(json)")

                    if let frntLineCount = json["totalFirstLevelSignups"].int as Int?{
                        if let teamCount = json["totalSignups"].int as Int?{
                            completionBlock(frontLineCount: frntLineCount,teamCount: teamCount)
                        }else{
                            showErrorWithMessage("Couldn't access Front Line and Team")
                            completionBlock(frontLineCount: 0,teamCount: 0)
                        }
                    }else{
                        showErrorWithMessage("Couldn't access Front Line and Team")
                        completionBlock(frontLineCount: 0,teamCount: 0)
                    }
                }else{
                    showErrorWithMessage("Couldn't access Dashboard")
                    completionBlock(frontLineCount: 0,teamCount: 0)
                }

            case .Failure(let error):
                completionBlock(frontLineCount: 0,teamCount: 0)
                showErrorWithMessage(error.description ?? "")
            }
        }
    }

    func getUserDashBoardWithToken(token:String,completionBlock:(AnyObject?) -> Void){
        CommonClass.showLoaderWithStatus("Accessing DashBoard..")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        print_debug("Hitting url : \(USER_DASHBOARD) \n with headers: \(head) \n and params : nil")

        Alamofire.request(.GET, USER_DASHBOARD,parameters:nil,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    if let dict = json.dictionaryObject as [String:AnyObject]?{
                        let user = UserModel(dictionary: dict)
                            print_debug("user json is \n \(json)")
                            if !user.ID.isEmpty{
                                user.saveUserInfo(user)
                            completionBlock(user)
                            }else{
                                showErrorWithMessage("Couldn't access Dashboard")
                                completionBlock(nil)
                        }

                    }else{
                        showErrorWithMessage("Couldn't access Dashboard")
                        completionBlock(nil)
                    }

                }else{
                    showErrorWithMessage("Couldn't access Dashboard")
                    completionBlock(nil)
                }

            case .Failure(let error):
                completionBlock(nil)
                showErrorWithMessage(error.localizedFailureReason!)
            }
        }
    }

    func getRegistrationCountsByDaysWithToken(token:String,completionBlock:(oneDayCount:Int,weekCount:Int,monthCont:Int,yearCount:Int) -> Void){
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        print_debug("Hitting url : \(REGISTRATION_COUNT_BY_DAYS) \n with headers: \(head) \n and params :nil")

        Alamofire.request(.GET, REGISTRATION_COUNT_BY_DAYS,parameters:nil,headers: head).responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Registration counts json:\n \(json)")

                    if let dict = json["totalUsers"].dictionary as [String:JSON]?{
                        var onedCount = 0
                        var wCount = 0
                        var mCount = 0
                        var yCount = 0
                        if let pd = dict["previousDay"]!.dictionary as [String:JSON]?{
                            if let odc = pd["totalSignups"]!.int as Int?{
                                onedCount = odc
                            }
                        }

                        if let pd = dict["last7Days"]!.dictionary as [String:JSON]?{
                            if let odc = pd["totalSignups"]!.int as Int?{
                                wCount = odc
                            }
                        }

                        if let pd = dict["last30Days"]!.dictionary as [String:JSON]?{
                            if let odc = pd["totalSignups"]!.int as Int?{
                                mCount = odc
                            }
                        }

                        if let pd = dict["last365Days"]!.dictionary as [String:JSON]?{
                            if let odc = pd["totalSignups"]!.int as Int?{
                                yCount = odc
                            }
                        }

                        completionBlock(oneDayCount: onedCount,weekCount: wCount,monthCont: mCount,yearCount: yCount)


                    }else{
                        showErrorWithMessage("Couldn't access Dashboard")
                        completionBlock(oneDayCount: 0,weekCount: 0,monthCont: 0,yearCount: 0)
                    }

                }else{
                    showErrorWithMessage("Couldn't access Dashboard")
                    completionBlock(oneDayCount: 0,weekCount: 0,monthCont: 0,yearCount: 0)
                }

            case .Failure(let error):
                completionBlock(oneDayCount: 0,weekCount: 0,monthCont: 0,yearCount: 0)
                showErrorWithMessage(error.localizedFailureReason ?? "")
            }
        }
    }


    func getSalesCountsByDaysWithToken(token:String,completionBlock:(silverSales : (oneDayCount:Int,weekCount:Int,monthCont:Int,yearCount:Int),goldSales : (oneDayCount:Int,weekCount:Int,monthCont:Int,yearCount:Int)) -> Void){
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        print_debug("Hitting url : \(SALES_STATUS_URL) \n with headers: \(head) \n and params :nil")

        Alamofire.request(.GET, SALES_STATUS_URL,parameters:nil,headers: head).responseJSON { response in
            var silverSalesCount = (oneDayCount:0,weekCount:0,monthCont:0,yearCount:0)
            var goldSalesCount = (oneDayCount:0,weekCount:0,monthCont:0,yearCount:0)
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Sales counts json:\n \(json)")
                    if let salesDict = json["sales"].dictionary as [String:JSON]?{
                        if let goldDict = salesDict["gold"]!.dictionaryObject as [String:AnyObject]?{
                            if let lastDay = goldDict["lastDay"] as? Int{
                                goldSalesCount.oneDayCount = lastDay
                            }
                            if let last7Days = goldDict["last7Days"] as? Int{
                                goldSalesCount.weekCount = last7Days
                            }
                            if let last30Days = goldDict["last30Days"] as? Int{
                                goldSalesCount.monthCont = last30Days
                            }
                            if let last365Days = goldDict["last365Days"] as? Int{
                                goldSalesCount.yearCount = last365Days
                            }
                        }

                        if let silverDict = salesDict["silver"]!.dictionaryObject as [String:AnyObject]?{
                            if let lastDay = silverDict["lastDay"] as? Int{
                                silverSalesCount.oneDayCount = lastDay
                            }
                            if let last7Days = silverDict["last7Days"] as? Int{
                                silverSalesCount.weekCount = last7Days
                            }
                            if let last30Days = silverDict["last30Days"] as? Int{
                                silverSalesCount.monthCont = last30Days
                            }
                            if let last365Days = silverDict["last365Days"] as? Int{
                                silverSalesCount.yearCount = last365Days
                            }

                        }
                        completionBlock(silverSales: silverSalesCount,goldSales: goldSalesCount)
                    }else{
                        showErrorWithMessage("Couldn't access Dashboard")
                        completionBlock(silverSales: silverSalesCount,goldSales: goldSalesCount)
                    }

                }else{
                    showErrorWithMessage("Couldn't access Dashboard")
                    completionBlock(silverSales: silverSalesCount,goldSales: goldSalesCount)
                }

            case .Failure(let error):
                completionBlock(silverSales: silverSalesCount,goldSales: goldSalesCount)
                showErrorWithMessage(error.localizedFailureReason ?? "")
            }
        }
    }

     func getCommissionCountsByDaysWithToken(token:String,completionBlock:(oneDayCount:Int,weekCount:Int,monthCont:Int,yearCount:Int) -> Void){
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")

        print_debug("Hitting url : \(COMMISSION_COUNT_BY_DAYS) \n with headers: \(head) \n and params :nil")

        Alamofire.request(.GET, COMMISSION_COUNT_BY_DAYS,parameters:nil,headers: head).responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("commission counts json:\n \(json["totalCoins"])")

                    if let dict = json["totalCoins"].arrayObject as? [[String : AnyObject]]{
                        var onedCount = 0
                        var wCount = 0
                        var mCount = 0
                        var yCount = 0
                        if let odc = dict[0]["previousDay"] as? Int{
                            onedCount = odc
                        }
                        if let odc = dict[0]["last7Days"] as? Int{
                            wCount = odc
                        }
                        if let odc = dict[0]["last30Days"] as? Int{
                            mCount = odc
                        }
                        if let odc = dict[0]["last365Days"] as? Int{
                            yCount = odc
                        }

                        completionBlock(oneDayCount: onedCount,weekCount: wCount,monthCont: mCount,yearCount: yCount)


                    }else{
                        showErrorWithMessage("Couldn't access Dashboard")
                        completionBlock(oneDayCount: 0,weekCount: 0,monthCont: 0,yearCount: 0)
                    }

                }else{
                    showErrorWithMessage("Couldn't access Dashboard")
                    completionBlock(oneDayCount: 0,weekCount: 0,monthCont: 0,yearCount: 0)
                }

            case .Failure(let error):
                completionBlock(oneDayCount: 0,weekCount: 0,monthCont: 0,yearCount: 0)
                showErrorWithMessage(error.localizedFailureReason ?? "")
            }
        }
    }

//API URI: /api/genealogys/list- view/MEMBER_LEVEL/USER_ID_RECEIVED_BY_ME/VIEW_PAGE/PAGE_LIMIT/list/LIST_ORDER/0/FROM_DATE/TO_DATE/LISTED_USERS_TEAM_MIN_SIZE_IN _NUMBER/LISTED_USERS_TEAM_MAX_SIZE_IN_NUMBER/LIST_USERS_RANK_ IN_NUMBER/LIST_USERS_MIN_GOLD_PACKS_IN_NUMBER/LIST_USERS_MAX_ GOLD_PACKS_NUMBER/LIST_USERS_MIN_SILVER_PACKS_IN_NUMBER/LIST_ USERS_MAX_SILVER_PACKS_IN_NUMBER/SEARCH_FIELD_NAME/SEARCH_FIE LD_VALUE
//        let FRONT_LINE_URL = "\(BASE_URL)/api/genealogys/list-view/1/\(userID)/\(pageNumber)/\(recordPerPage)/list/\(listOrder.rawValue)/0/\(fromDate)/\(toDate)/\(listedUsersTeamMinimumSize)/\(listedUsersTeamMaximumSize)/\(listUsersRank)/\(listUsersMinimumGoldPacks)/\(listUsersMaximumGoldPacks)/\(listUsersMinimumSilverPacks)/\(listUsersMaximumSilverPacks)/\(searchFieldName)/\(searchFieldValue)"

    // http://45.55.35.34/api/genealogys/list-view/1/576bf2563d90dcc32ac899fb/1/list/ASC/0/2016-01-29T14:23:34.869Z/2016-08-29T14:23:34.869Z/0/0/0/0/50/0/50/0/0


    func getFrontLine(token:String,userID:String,pageNumber:Int,recordPerPage:Int,listOrder:ListOrder,fromDate:String,toDate:String,listedUsersTeamMinimumSize:Int = 0,listedUsersTeamMaximumSize:Int,listUsersRank:Int,  listUsersMinimumGoldPacks:Int,listUsersMaximumGoldPacks:Int,listUsersMinimumSilverPacks:Int,listUsersMaximumSilverPacks:Int,searchFieldName:String,searchFieldValue:String, completionBlock:(AnyObject?) -> Void){
        //CommonClass.showLoaderWithStatus("")

        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")

         let FRONT_LINE_URL = "\(BASE_URL)api/genealogys/list-view/1/\(userID)/\(pageNumber)/list/\(listOrder.rawValue)/0/\(fromDate)/\(toDate)/\(listedUsersTeamMinimumSize)/\(listedUsersTeamMaximumSize)/\(listUsersRank)/\(listUsersMinimumGoldPacks)/\(listUsersMaximumGoldPacks)/\(listUsersMinimumSilverPacks)/\(listUsersMaximumSilverPacks)/\(searchFieldName)/\(searchFieldValue)"
        print_debug("hitting with header:\n \(head)\n url:\n \(FRONT_LINE_URL)")

        print_debug("Hitting url : \(FRONT_LINE_URL) \n with headers: \(head) \n and params :nil")

        Alamofire.request(.GET, FRONT_LINE_URL,parameters:nil,headers: head).responseJSON { response in
            //CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("frontLine json is \(json)")
                    if let frontLine = FrontLineModel(json: json) as FrontLineModel?{
                        completionBlock(frontLine)
                    }else{
                        showErrorWithMessage("Error from server")
                    }
                }

            case .Failure(let error):
                showErrorWithMessage(error.description)
            }
        }
    }


    func getLatestRegistrationsWithToken(token:String,pageNumber:Int,completionBlock:(AnyObject?) -> Void){
        //CommonClass.showLoaderWithStatus("")
        let param = ["page":"\(pageNumber)"]
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")

        print_debug("Hitting url : \(REGISTRATION_LIST_URL) \n with headers: \(head) \n and params :\(param)")

        Alamofire.request(.GET, REGISTRATION_LIST_URL,parameters:param,headers: head).responseJSON { response in
            //CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Response of url : \(REGISTRATION_LIST_URL) \n \(json)")
                    let latestRegParser = LatestSignUpParser(json: json)
                    completionBlock(latestRegParser.latestSignUps)
                }else{
                    showErrorWithMessage("Couldn't access Dashboard")
                    completionBlock(nil)
                }

            case .Failure(let error):
                completionBlock(nil)
                showErrorWithMessage(error.description ?? "")
            }
        }
    }


    func getTeamWithToken(token:String,pageNUmber:Int,completionBlock:(AnyObject?) -> Void){
        //CommonClass.showLoaderWithStatus("Accessing Team..")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        let param = ["page":"\(pageNUmber)"]

        print_debug("Hitting url : \(TEAM_URL) \n with headers: \(head) \n and params :\(param)")

        Alamofire.request(.GET, TEAM_URL,parameters:param,headers: head).responseJSON { response in
            //CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Team json is \(json)")
                    if let team = TeamModel(json: json) as TeamModel?{
                        completionBlock(team)
                    }else{
                        showErrorWithMessage("Error from server")
                    }
                }
            case .Failure(let error):
                showErrorWithMessage(error.description ?? "Error from server")
            }
        }
    }

    func getAllNotificationsWithToken(token:String,isViewed:Bool?,type:String?,pageNumber:Int?,completionBlock:(AnyObject?,total:Int) -> Void){
        // CommonClass.showLoaderWithStatus("Accessing Notifications..")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        var params = [String:String]()
        if type != nil{
            params.updateValue(type!, forKey: "type")
        }
        if pageNumber != nil{
            params.updateValue("\(pageNumber!)", forKey: "pageNumber")
        }
        if isViewed != nil{
            params.updateValue(isViewed! ? "True" : "False", forKey: "pageNumber")
        }


        Alamofire.request(.GET, NOTIFICATION_LIST_URL,parameters:params,headers: head).responseJSON { response in
            // CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("notifications list json is\n \(json)\n")
                   let notificationParser = NotificationParser(json: json)
                    completionBlock(notificationParser.notifications,total: notificationParser.total)
                }
            case .Failure(let error):
                completionBlock(nil,total:0)

                showErrorWithMessage(error.description ?? "Error from server")
            }
        }
    }


    func getNotificationDetailWithToken(token:String,notificatinID:String,completionBlock:(AnyObject?) -> Void){
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        head.updateValue("text/html", forKey: "Content-Type")
        head.updateValue("Bearer \(token)", forKey: "Authorization")
        let urlToHit = "\(NOTIFICATION_LIST_URL)/\(notificatinID)"
        print_debug("Hitting url : \(urlToHit) \n with headers: \(head))")
        Alamofire.request(.GET, urlToHit,parameters:nil,headers: head).responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("message Detail json is \(json)")

                    if let message = Notification(json: json) as Notification?{
                        completionBlock(message)
                    }else{
                        showErrorWithMessage("Error from server")
                        completionBlock(nil)
                    }
                }
            case .Failure(let error):
                completionBlock(nil)
                showErrorWithMessage(error.description ?? "Error from server")
            }
        }
    }


}
