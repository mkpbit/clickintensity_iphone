//
//  InfoScreenViewController.swift
//  Sinr
//
//  Created by TecOrb on 25/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON
import Google
class InfoScreenViewController: UIViewController,UIScrollViewDelegate,InfinitePagingViewDelegate,GIDSignInDelegate,GIDSignInUIDelegate{
    @IBOutlet weak var aScrollView: UIScrollView!
    @IBOutlet weak var aPageControl: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: GIDSignInButton!
    @IBOutlet weak var loginButton: UIButton!

    //  @IBOutlet weak var googleSignInButton: UIButton!

    var myTimer: NSTimer? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        //googleButton.style = GIDSignInButtonStyle.Standard
        googleButton.colorScheme = GIDSignInButtonColorScheme.Dark
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().clientID = kGoogleClientId
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        
        aPageControl.addTarget(self, action: #selector(InfoScreenViewController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
        aScrollView.delegate = self
        self.pagingSetUp()
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(loginButton, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(googleButton, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(skipButton, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(facebookButton, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 4)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //timer funtioning start
        myTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(gotoNextPage), userInfo: nil, repeats: true)

    }
    override func viewWillDisappear(animated: Bool) {
        myTimer?.invalidate()
    }

    //Google signIn delegate methods
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        var params = [String:String]()
        if (error == nil)
        {

            if let userId = user.userID {
                params.updateValue(userId, forKey: kSocialID)
            }else{
                showErrorWithMessage("Your google id didn't get, please review your google profile")
                return;
            }

            //let idToken = user.authentication.idToken
            // let fullName = user.profile.name

            if let firstName = user.profile.givenName{
                params.updateValue(firstName, forKey: kFirstName)
            }else{
                showErrorWithMessage("Didn't get your First Name")
                return
            }
            if let lastName = user.profile.familyName{
                params.updateValue(lastName, forKey: kLastName)
            }else{
                showErrorWithMessage("Didn't get your Last Name")
                return
            }
            if let email = user.profile.email{
                params.updateValue(email, forKey: kEmail)
            }else{
                showErrorWithMessage("Didn't get your Email")
                return
            }
            if let profilePicUrl = user.profile.imageURLWithDimension(400){
                params.updateValue(profilePicUrl.absoluteString, forKey:kProfilePic)
            }
            if !CommonClass.isConnectedToNetwork{
                showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
                return;
            }else{
                CommonClass.showLoader()
                LoginService.loginWithSocialParams(params, loginType: SocialLoginType.Google, completionBlock: { response in
                    CommonClass.hideLoader()
                    if response != nil{
                        let json = JSON(response!)
                        print("JSON: \(json)")
                        showSuccessWithMessage("\(json)")
                    }
                })
            }

        }
        else
        {
            print("\(error.localizedDescription)")
        }
    }
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    @IBAction func onClickFacebookButton(sender: UIButton){
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.SystemAccount
        login.logInWithReadPermissions(["public_profile", "email"], fromViewController: self, handler: {(result, error) in
            if error != nil {
                print_debug("Error ======>  \(error.description)")
            }
            else if result.isCancelled {
            }
            else {
                CommonClass.showLoader()
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]).startWithCompletionHandler({(connection, result, error) -> Void in
                    if error != nil{
                        CommonClass.hideLoader()
                        print_debug("Error ======>  \(error.description)")
                    }else{
                        var params = [String:String]()
                        // params.updateValue("", forKey: kContact)
                        if let firstName = result.valueForKeyPath("first_name") as? String{
                            params.updateValue(firstName, forKey: kFirstName)
                        }else{
                            params.updateValue(" ", forKey: kFirstName)
                        }
                        if let lastName = result.valueForKeyPath("last_name") as? String{
                            params.updateValue(lastName, forKey: kLastName)
                        }else{
                            params.updateValue(" ", forKey: kLastName)
                        }
                        if let fbID = result.valueForKeyPath("id") as? String{
                            params.updateValue(fbID, forKey: kSocialID)
                        }
                        if let gender = result.valueForKeyPath("gender") as? String{
                            params.updateValue(gender, forKey: kGender)
                        }
                        if let email = result.valueForKeyPath("email") as? String{
                            params.updateValue(email, forKey: kEmail)
                        }
                        if let profilePicUrl =  result.valueForKeyPath("picture.data.url") as? String{
                            params.updateValue(profilePicUrl, forKey: kProfilePic)
                        }
                        if !CommonClass.isConnectedToNetwork{
                            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
                            return;
                        }else{
                            LoginService.loginWithSocialParams(params, loginType: SocialLoginType.Facebook, completionBlock: { response in
                                CommonClass.hideLoader()
                                if response != nil{
                                    let json = JSON(response!)
                                    print("JSON: \(json)")
                                    showSuccessWithMessage("\(json)")
                                }
                            })
                        }
                    }
                })
            }
            
        })
    }

    func pagingSetUp()
    {
        aScrollView.contentSize = CGSizeMake(self.view.frame.size.width * 5, self.aScrollView.bounds.height)
        aScrollView.pagingEnabled = true
        for i in 0 ..< 5
        {
            var frame : CGRect = aScrollView.frame
            frame.origin.x = self.view.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size.width = self.aScrollView.bounds.size.width
            frame.size.height = self.aScrollView.bounds.size.height
            let page : UIImageView = UIImageView()
            page.frame = frame
            switch (i)
            {
            case 0:
                page.image = UIImage(named: "0.png")
            case 1:
                page.image = UIImage(named: "1.png")
            case 2:
                page.image = UIImage(named: "2.png")
            case 3:
                page.image = UIImage(named: "3.png")
            case 4:
                page.image = UIImage(named: "4.png")
            default:
                break;
            }
            page.contentMode = UIViewContentMode.ScaleToFill
            aScrollView.addSubview(page)
        }
        aScrollView.contentOffset = CGPointMake(0, aScrollView.frame.origin.y);
    }

    //MARK: -InfinitePagingViewDelegate
    func pagingView(pagingView: InfinitePagingView!, didEndDecelerating scrollView: UIScrollView!, atPageIndex pageIndex: Int) {
        self.aPageControl.currentPage = pageIndex;
    }

    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(aPageControl.currentPage) * aScrollView.frame.size.width
        self.aScrollView.setContentOffset(CGPointMake(x, 0), animated: true)
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.aPageControl.currentPage = Int(pageNumber)
    }

    func gotoNextPage() {
        var pageNumber = aPageControl.currentPage
        if pageNumber <= 3 {
            pageNumber = pageNumber + 1
        }else{
            pageNumber = 0;
        }
        self.aPageControl.currentPage = Int(pageNumber)
        self.changePage(aPageControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}