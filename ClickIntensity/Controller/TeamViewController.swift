//
//  TeamViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 04/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class TeamViewController : UIViewController,SlideNavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var teamTableView : UITableView!
    var teamArray = [TeamMember]()
    var totalRecords = 0
    var limit = 25
    var pageNumber = 1
    var isNewDataLoading = false
    var isPushed = false

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(TeamViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        return refreshControl
    }()

    func handleRefresh(refreshControl: UIRefreshControl) {
        pageNumber = 1
        teamArray.removeAll()
        isNewDataLoading = true
        teamTableView.reloadData()
        self.loadTeamMembersWithToken(self.pageNumber)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        teamTableView.tableFooterView = UIView()
        self.teamTableView.addSubview(refreshControl)
        self.loadTeamMembersWithToken(pageNumber)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        if !isPushed{
            let frame = CGRectMake(0, 0, 44, 44)
            let button = UIButton(frame:  frame)
            button.setTitle(" ", forState: .Normal)
            let leftBarButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = leftBarButton
        }else{
            let frame = CGRectMake(0, 0, 44, 44)
            let button = UIButton(frame:  frame)
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            button.setImage(UIImage(named: "backArrow"), forState: .Normal)
            button.addTarget(self, action: #selector(onClickBack(_:)), forControlEvents: .TouchUpInside)
            let leftBarButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = leftBarButton
        }
    }

    @IBAction func onClickBack(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }


    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return !isPushed
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("FrontLineCell", forIndexPath: indexPath) as? FrontLineCell{
            let member = teamArray[indexPath.row]
            cell.imageView?.sd_setImageWithURL(NSURL(string:member.email), placeholderImage: UIImage(named: "frontline_dp"))
            cell.DOJLabel.text = "DOJ (\(CommonClass.dateWithString(member.joinAt)))"
            cell.userLevel.text = "(Level \(member.level))"
            cell.userNameLabel.text = member.name
            cell.emailLabel.text = member.email
            return cell
        }
        return UITableViewCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let member = teamArray[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("LeadsViewController") as! LeadsViewController
        let prospect = ProspectModel()
        prospect.ID = member.ID
        prospect.name = member.name
        prospect.createdAt = member.joinAt
        prospect.email = member.email
        prospect.mobile = "Level\(member.level)"
        vc.myProspect = prospect
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView == teamTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if pageNumber >= 1{
                            pageNumber+=1
                        }
                        self.loadTeamMembersWithToken(pageNumber)
                    }
                }
            }
        }
    }
    

    func loadTeamMembersWithToken(pageNumber:Int){
        if self.teamArray.count < 10{
            self.pageNumber = 1
        }

        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            DashBoardService.sharedInstance.getTeamWithToken(token, pageNUmber: pageNumber, completionBlock: { (responseTeamModel) in
                self.isNewDataLoading = false
                if self.refreshControl.refreshing{
                    self.refreshControl.endRefreshing()
                }
                if let teamModel = responseTeamModel as? TeamModel{
                    if teamModel.members.count<10 && self.pageNumber > 1{
                        self.pageNumber -= 1
                    }
                    self.teamArray.appendContentsOf(teamModel.members)
                    self.teamTableView.reloadData()
                }
            })
        }

    }
    
}
