//
//  TextFieldCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 14/07/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {
    @IBOutlet weak var textField : UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state PhoneNumberCell
    }

}


class SignUpButtonCell: UITableViewCell {
    @IBOutlet weak var signupButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

class PhoneNumberCell: UITableViewCell {
    @IBOutlet weak var countryCodeTextField : UITextField!
    @IBOutlet weak var phoneNumberTextField : UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
