//
//  SignupViewController.swift
//  Sinr
//
//  Created by TecOrb on 25/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController,CountryPhoneCodePickerDelegate {
    //Keys to hadle TableView
    let SIGN_UP_FIELD = ["Sponser ID","Sponser Name","User Name","Full Name","9012345678","Country","Email","Password","Confirmed Password","Sign UP"]


    let kSponserID = "sponser"
    let kSponserName = "sponserName"
    let kUserName = "username"
    let kUserFullName = "name"
    let kCountryCode = "countryCode"
    let kPhoneNumber = "mobile"
    let kCountryName = "countryName"
    let kCountry = "country"
    let kEmail = "email"
    let kPassword = "password"
    let kConfirmedPassword = "confirmedPassword"
    let kRefarralHash = "referralHash"
    /////End of Keys


    //properties

    var countryNamePicker : CountryPicker!
    var countryCodePicker : CountryPicker!
    var sponser : SponserModel!
    var tokenOfStep3 : String!
    var isUserNameValid: Bool!
    var userToken : String!



    var params = [String:AnyObject]()
    @IBOutlet weak var signUpTableView : UITableView!
    var countryCodeTextFiled : UITextField!
    var countryNameTextFiled : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.signUpTableView.dataSource = self
        self.signUpTableView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default

    }

    @IBAction func onClickSignIn(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func onClickCancel(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func onClickSignUp(sender: UIButton){
        self.view.endEditing(true)
        let validation = self.validateParams(self.params)
        if !validation.isValidate{
            showErrorWithMessage(validation.message)
            return
        }
        CommonClass.showLoader()
        SignUpService.sharedInstance.registerSponserWithParams(self.sponser.ID, sponserName: self.sponser.name) { (responseError) in
            if let error = responseError as? Bool{
                if error{
                    return
                }
                var paramForSignUP = [String:AnyObject]()
                paramForSignUP.updateValue(self.params[self.kEmail] as! String, forKey: "email")
                paramForSignUP.updateValue("\(self.params[self.kCountry] as! String)-\(self.params[self.kPhoneNumber] as! String)", forKey: "mobile")
                paramForSignUP.updateValue(self.params[self.kUserFullName] as! String, forKey: "name")
                paramForSignUP.updateValue(self.params[self.kPassword] as! String, forKey: "password")
                paramForSignUP.updateValue(self.params[self.kUserName] as! String, forKey: "username")
                paramForSignUP.updateValue(self.params[self.kCountry] as! String, forKey: "country")
                paramForSignUP.updateValue(self.params[self.kCountryCode] as! String, forKey: "countryCode")
                paramForSignUP.updateValue(self.params[self.kCountryName] as! String, forKey: "countryName")
                paramForSignUP.updateValue(self.sponser.refUser, forKey: "referralHash")
                SignUpService.sharedInstance.signUpUserWithParams(paramForSignUP) { (userToken) in
                    if let token = userToken as? String{
                        if !token.containsString(" "){
                            self.userToken = token
                            kUserDefaults.setValue(token, forKey: kUserToken)
                            let alert = UIAlertController(title: "Message!", message:"You have successfully signed up. Please go through your inbox to varify account.", preferredStyle: .Alert)
                            let okay: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
                                alert.dismissViewControllerAnimated(true, completion: nil)
                                self.navigationController?.popViewControllerAnimated(true)
                            }
                            alert.addAction(okay)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }else{
                showErrorWithMessage("Sponser couldn't lock!")
            }

        }//



    }
}

extension SignupViewController:UITableViewDataSource,UITableViewDelegate{

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 9{
            return 85
        }
        return 44
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SIGN_UP_FIELD.count
    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 4{
            if let cell = tableView.cellForRowAtIndexPath(indexPath) as? PhoneNumberCell{
                cell.phoneNumberTextField.keyboardType = UIKeyboardType.PhonePad
                cell.phoneNumberTextField.placeholder = "9012345678"
                cell.phoneNumberTextField.text = self.params[kPhoneNumber] as? String
                cell.countryCodeTextField.text = self.params[kCountry] as? String
                cell.countryCodeTextField.keyboardType = UIKeyboardType.NumbersAndPunctuation
                self.setUpCountryCodePicke(cell.countryCodeTextField)
            }
        }else if indexPath.row == 5{
            if let cell = tableView.cellForRowAtIndexPath(indexPath) as? TextFieldCell{
                cell.textField.delegate = self
                cell.textField.placeholder = SIGN_UP_FIELD[indexPath.row]
                cell.textField.text = params[kCountryName]  as? String
                cell.textField.keyboardType = UIKeyboardType.Default
                self.setUpCountryNamePicke(cell.textField)
            }
        }else if indexPath.row == 9{
            if let cell = tableView.cellForRowAtIndexPath(indexPath) as? SignUpButtonCell{
                cell.signupButton.addTarget(self, action: #selector(onClickSignUp(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            }
        }else{
            //text fildcell
            if let cell = tableView.cellForRowAtIndexPath(indexPath) as? TextFieldCell{
                cell.textField.placeholder = SIGN_UP_FIELD[indexPath.row]
                switch indexPath.row {
                case 0:
                    cell.textField.text = params[kSponserID]  as? String
                case 1:
                    cell.textField.enabled = false
                    cell.textField.text = params[kSponserName]  as? String
                case 2:
                    cell.textField.text = params[kUserName]  as? String
                case 3:
                    cell.textField.text = params[kUserFullName]  as? String
                case 6:
                    cell.textField.keyboardType = UIKeyboardType.EmailAddress
                    cell.textField.text = params[kEmail]  as? String

                case 7:
                    cell.textField.keyboardType = UIKeyboardType.Default
                    cell.textField.secureTextEntry = true
                    cell.textField.text = params[kPassword]  as? String

                case 8:
                    cell.textField.keyboardType = UIKeyboardType.Default
                    cell.textField.secureTextEntry = true
                    cell.textField.text = params[kConfirmedPassword]  as? String
                default:
                    break
                }
            }
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 4{
            let cell = tableView.dequeueReusableCellWithIdentifier("PhoneNumberCell", forIndexPath: indexPath) as! PhoneNumberCell
            cell.phoneNumberTextField.keyboardType = UIKeyboardType.PhonePad
            cell.phoneNumberTextField.placeholder = "9012345678"
            cell.phoneNumberTextField.text = self.params[kPhoneNumber] as? String
            cell.countryCodeTextField.text = self.params[kCountry] as? String
            cell.countryCodeTextField.keyboardType = UIKeyboardType.NumbersAndPunctuation
            self.setUpCountryCodePicke(cell.countryCodeTextField)
            cell.phoneNumberTextField.delegate = self
            cell.countryCodeTextField.delegate = self

            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCellWithIdentifier("TextFieldCell", forIndexPath: indexPath) as! TextFieldCell
            cell.textField.delegate = self
            cell.textField.placeholder = SIGN_UP_FIELD[indexPath.row]
            cell.textField.text = params[kCountryName]  as? String
            cell.textField.keyboardType = UIKeyboardType.Default
            self.setUpCountryNamePicke(cell.textField)
            return cell
        }else if indexPath.row == 9{
            let cell = tableView.dequeueReusableCellWithIdentifier("SignUpButtonCell", forIndexPath: indexPath) as! SignUpButtonCell
            cell.signupButton.addTarget(self, action: #selector(onClickSignUp(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            return cell
        }else{
            //text fildcell
            let cell = tableView.dequeueReusableCellWithIdentifier("TextFieldCell", forIndexPath: indexPath) as! TextFieldCell
            cell.textField.delegate = self
            cell.textField.placeholder = SIGN_UP_FIELD[indexPath.row]
            switch indexPath.row {
            case 0:
                cell.textField.text = params[kSponserID]  as? String
            case 1:
                cell.textField.enabled = false
                cell.textField.text = params[kSponserName]  as? String
            case 2:
                cell.textField.text = params[kUserName]  as? String
                cell.textField.autocapitalizationType = UITextAutocapitalizationType.None
            case 3:
                cell.textField.text = params[kUserFullName]  as? String
                cell.textField.autocapitalizationType = UITextAutocapitalizationType.Words
            case 6:
                cell.textField.keyboardType = UIKeyboardType.EmailAddress
                cell.textField.secureTextEntry = false
                cell.textField.text = params[kEmail]  as? String

            case 7:
                cell.textField.keyboardType = UIKeyboardType.Default
                cell.textField.secureTextEntry = true
                cell.textField.text = params[kPassword]  as? String

            case 8:
                cell.textField.keyboardType = UIKeyboardType.Default
                cell.textField.secureTextEntry = true
                cell.textField.text = params[kConfirmedPassword]  as? String
            default:
                break
            }
            return cell
        }
    }



    func setUpCountryCodePicke(textField:UITextField){
        self.countryCodePicker = CountryPicker()
        let locale = NSLocale.currentLocale()
        let code = locale.objectForKey(NSLocaleCountryCode) as! String
        countryCodePicker.countryPhoneCodeDelegate = self
        countryCodePicker.setCountry(code)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action:#selector(countryCodeResignResponder(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action:nil)
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let array = [spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.backgroundColor = UIColor.lightTextColor()
        textField.inputView = self.countryCodePicker
        textField.inputAccessoryView = toolbar;
        countryCodeTextFiled = textField
        countryCodeTextFiled.delegate = self

    }
    func setUpCountryNamePicke(textField:UITextField){
        self.countryNamePicker = CountryPicker()
        let locale = NSLocale.currentLocale()
        let code = locale.objectForKey(NSLocaleCountryCode) as! String
        countryNamePicker.countryPhoneCodeDelegate = self
        countryNamePicker.setCountry(code)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action:#selector(countryNameResignResponder(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action:nil)
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let array = [spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.backgroundColor = UIColor.lightTextColor()
        textField.inputView = self.countryNamePicker
        textField.inputAccessoryView = toolbar;
        countryNameTextFiled = textField
        countryNameTextFiled.delegate = self
    }

    func countryPhoneCodePicker(picker: CountryPicker, didSelectCountryCountryWithName name: String, countryCode: String, phoneCode: String) {
        if picker == self.countryNamePicker{
            countryNameTextFiled.text = name
        }else if picker == self.countryCodePicker{
            countryCodeTextFiled.text = phoneCode
        }
    }

    @IBAction func countryCodeResignResponder(sender:UIBarButtonItem){
        let index = self.countryCodePicker.selectedRowInComponent(0)
        let country = countryCodePicker.countries[index]
        self.params.updateValue(country.phoneCode!, forKey: self.kCountry)
        self.countryPhoneCodePicker(countryCodePicker, didSelectCountryCountryWithName: country.name!, countryCode: country.code!, phoneCode: country.phoneCode!)
        self.view.endEditing(true)
    }
    @IBAction func countryNameResignResponder(sender: UIBarButtonItem){
        let index = self.countryNamePicker.selectedRowInComponent(0)
        let country = countryNamePicker.countries[index]
        self.params.updateValue(country.name!, forKey: self.kCountryName)
        self.params.updateValue(country.code!, forKey: self.kCountryCode)

        self.countryPhoneCodePicker(countryNamePicker, didSelectCountryCountryWithName: country.name!, countryCode: country.code!, phoneCode: country.phoneCode!)
        self.view.endEditing(true)
    }
}

extension SignupViewController:UITextFieldDelegate{

    func validateParams(dictionary : [String:AnyObject]) -> (isValidate:Bool,message:String) {
        var validated = true
        var msg = ""

        if let uname = dictionary[kUserName] as? String{
            if uname.isEmpty || uname.containsString(" "){
                validated = false
                msg = "User Name Cann't be Empty"
                return(validated,msg)
            }
        }else{
            validated = false
            msg = "User Name Cann't be Empty"
            return(validated,msg)
        }

        if let uname = dictionary[kUserFullName] as? String{
            if uname.isEmpty{
                validated = false
                msg = "User Full Name Cann't be Empty"
                return(validated,msg)
            }
        }else{
            validated = false
            msg = "User Full Name Cann't be Empty"
            return(validated,msg)
        }

        if let uname = dictionary[kCountryCode] as? String{
            if uname.isEmpty{
                validated = false
                msg = "Country Code Cann't be Empty"
                return(validated,msg)
            }
        }else{
            validated = false
            msg = "Country Code Cann't be Empty"
            return(validated,msg)
        }

        if let uname = dictionary[kPhoneNumber] as? String{
            if uname.isEmpty{
                validated = false
                msg = "Phone Number Cann't be Empty"
                return(validated,msg)
            }
        }else{
            validated = false
            msg = "Phone Number Cann't be Empty"
            return(validated,msg)
        }

        if let uname = dictionary[kCountryName] as? String{
            if uname.isEmpty{
                validated = false
                msg = "Country Name Cann't be Empty"
                return(validated,msg)
            }
        }else{
            validated = false
            msg = "Country Name Cann't be Empty"
            return(validated,msg)
        }

        if let uname = dictionary[kEmail] as? String{
            if uname.isEmpty{
                validated = false
                msg = "Email Cann't be Empty"
                return(validated,msg)
            }
        }else{
            validated = false
            msg = "Email Cann't be Empty"
            return(validated,msg)
        }

        if let uname = dictionary[kPassword] as? String{
            if uname.isEmpty{
                validated = false
                msg = "Password Cann't be Empty"
                return(validated,msg)
            }
        }else{
            validated = false
            msg = "Password Cann't be Empty"
            return(validated,msg)
        }

        if let uname = dictionary[kConfirmedPassword] as? String{
            if uname.isEmpty{
                validated = false
                msg = "Please Confirm the password"
                return(validated,msg)
            }else{
                if uname != (dictionary[kPassword] as! String){
                    validated = false
                    msg = "Confirmed Password didn't match"
                    return(validated,msg)
                }
            }
        }else{
            validated = false
            msg = "Please Confirm the password"
            return(validated,msg)
        }

        return(validated,msg)
    }

    func textFieldDidEndEditing(textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.signUpTableView) as NSIndexPath?{
            switch indexPath.row {
            case 0:
                self.params.updateValue(textField.text!, forKey: kSponserID)
                if textField.text?.characters.count>0{
                    SignUpService.sharedInstance.getSponserWith(textField.text!, completionBlock: { (sponser) in
                        self.sponser = sponser as! SponserModel
                        var message : String!
                        var title : String!
                        if !self.sponser.ID.isEmpty{
                            message = "ID: \(self.sponser.ID)\nName: \(self.sponser.name)\nCountry: \(self.sponser.country)"
                            title = "Your Sponser"
                            self.params.updateValue(self.sponser.ID, forKey: self.kSponserID)
                            self.params.updateValue(self.sponser.name, forKey: self.kSponserName)
                            self.signUpTableView.reloadData()

                        }else{
                            title = "Sorry!"
                            message = "No Sponser found!"
                            self.params.updateValue("", forKey: self.kSponserID)
                            self.params.updateValue("", forKey: self.kSponserName)
                            self.signUpTableView.reloadData()

                        }

                        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)

                        let okayAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: { (okayAction) in
                            self.params.updateValue(self.sponser.ID, forKey: self.kSponserID)
                            self.params.updateValue(self.sponser.name, forKey: self.kSponserName)
                            alert.dismissViewControllerAnimated(true, completion: {
                                self.signUpTableView.reloadData()
                            })
                        })

                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive, handler: { (action) in
                            self.params.updateValue("", forKey: self.kSponserID)
                            self.params.updateValue("", forKey: self.kSponserName)
                            alert.dismissViewControllerAnimated(true, completion: {
                                self.signUpTableView.reloadData()
                            })
                        })
                        alert.addAction(okayAction)
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    })


                }

            case 1:
                self.params.updateValue(self.sponser.name, forKey: self.kSponserName)
            case 2:
                //validation of username
                if !textField.text!.isEmpty{
                    self.params.updateValue(textField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()), forKey: self.kUserName)
                    SignUpService.sharedInstance.validateUserWithUserName(textField.text!.lowercaseString, completionBlock: { (isValid) in
                        var message : String!
                        var title : String!
                        if let userDidValidate = isValid as? Bool{
                            if userDidValidate {
                                message = "\(textField.text!) is available"
                                title = "Available"
                                self.params.updateValue(textField.text!.lowercaseString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()), forKey: self.kUserName)
                            }else{
                                message = "\(textField.text!) is not available.\nPlease try for another user name"
                                title = "Not Available"
                                self.params.updateValue("", forKey: self.kUserName)
                            }
                            self.signUpTableView.reloadData()
                        }

                        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)

                        let okayAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: { (okayAction) in
                            alert.dismissViewControllerAnimated(true, completion: {
                                self.signUpTableView.reloadData()
                            })
                        })

                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive, handler: { (action) in
                            alert.dismissViewControllerAnimated(true, completion: {
                                self.signUpTableView.reloadData()
                            })
                        })
                        alert.addAction(okayAction)
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    })
                }
            case 3:
                self.params.updateValue(textField.text!, forKey: self.kUserFullName)
            case 4:
                if let cell = textField.tableViewCell() as? PhoneNumberCell{
                    self.params.updateValue(cell.phoneNumberTextField.text!, forKey: self.kPhoneNumber)
                    self.params.updateValue(cell.countryCodeTextField.text!, forKey: self.kCountry)
                }
            case 5:
                self.params.updateValue(textField.text!, forKey: self.kCountryName)
            case 6:
                self.params.updateValue(textField.text!, forKey: self.kEmail)
            case 7:
                self.params.updateValue(textField.text!, forKey: self.kPassword)
            case 8:
                self.params.updateValue(textField.text!, forKey: self.kConfirmedPassword)
            default:
                break
            }
        }else if textField == countryCodeTextFiled{
            self.params.updateValue(textField.text!, forKey: self.kCountryCode)

        }else if textField == countryNameTextFiled{
            self.params.updateValue(textField.text!, forKey: self.kCountryName)
        }
    }
}
