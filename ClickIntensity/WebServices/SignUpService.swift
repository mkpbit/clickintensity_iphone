//
//  SignUpService.swift
//  ClickIntensity
//
//  Created by TecOrb on 14/07/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import Foundation
import Alamofire
import TSMessages
import KVNProgress
import SwiftyJSON

// headers.updateValue("application/json", forKey: "Accept")
// headers.updateValue("Bearer QWxhZGRpbjpvcGVuIHNlc2FtZQ==", forKey: "Authorization")

class SignUpService {
    static let sharedInstance = SignUpService()
    private init(){}

     func getSponserWith(sponserID:String,completionBlock:(AnyObject?) -> Void){
        let params = ["sponsor":sponserID.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())]
        CommonClass.showLoaderWithStatus("")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        print_debug("Hitting url : \(VARIFY_SPONSER) \n with headers: \(head) \n and params : \(params)")

        Alamofire.request(.POST, VARIFY_SPONSER, parameters: params,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                        print_debug("sponser json is \n \(json)")
                        let sponser = SponserModel(json: json)
                        completionBlock(sponser)
                }
            case .Failure(let error):
                showErrorWithMessage(error.localizedFailureReason!)
            }
        }
    }

     func validateUserWithUserName(userName:String,completionBlock:(AnyObject?) -> Void){
        let params = ["username":userName.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())]
        CommonClass.showLoaderWithStatus("")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        print_debug("Hitting url : \(VALIDATE_USER_NAME) \n with headers: \(head) \n and params : \(params)")

        Alamofire.request(.POST, VALIDATE_USER_NAME, parameters: params,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("userValidation json is \n \(json)")
                   if let didValidate = json["isValid"].bool as Bool?{
                        completionBlock(didValidate)
                    }
                }
            case .Failure(let error):
                showErrorWithMessage(error.localizedFailureReason!)
            }
        }
    }

     func signUpUserWithParams(param:[String:AnyObject],completionBlock:(AnyObject?) -> Void){

        print_debug("Hitting with params:\n\(param)")
        CommonClass.showLoaderWithStatus("Signing up..")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        print_debug("Hitting url : \(SIGN_UP_USER) \n with headers: \(head) \n and params : \(param)")
        Alamofire.request(.POST, SIGN_UP_USER, parameters: param,headers: head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("userToken is \n \(json)")
                    if let token = json["token"].string as String?{
                        completionBlock(token)
                    }else{
                        completionBlock(nil)
                    }
                }
            case .Failure(let error):
                print_debug(error)
                completionBlock(nil)
                showErrorWithMessage(error.description ?? "")
            }
        }
    }

     func registerSponserWithParams(sponserID:String,sponserName:String,completionBlock:(AnyObject?) -> Void){

        //CommonClass.updateLoaderWithStatus("Locking Sponser..")
        var head = [String:String]()
        head.updateValue("application/json", forKey: "Accept")
        print_debug("Hitting with Params:\n id:\(sponserID),name:\(sponserName) and header:\(head)")
        Alamofire.upload(.POST, REGISTER_SPONSER, headers: head, multipartFormData: { (data) in
            if let sID = sponserID.dataUsingEncoding(NSUTF8StringEncoding){
                data.appendBodyPart(data: sID, name: "id")
                if let sName = sponserName.dataUsingEncoding(NSUTF8StringEncoding){
                    data.appendBodyPart(data: sName, name: "name")
                }
            }
            }) { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .Success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("register sponser json is \n \(json)")
                                if let error = json["error"].boolValue as Bool?{
                                    completionBlock(error)
                                }
                            }
                        case .Failure(let error):
                            completionBlock(true)
                            showErrorWithMessage(error.localizedFailureReason!)
                        }
                    }
                case .Failure( _):
                    completionBlock(true)
                    // print(encodingError)
                }
        }
    }




}
