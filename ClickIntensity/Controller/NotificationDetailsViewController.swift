//
//  NotificationDetailsViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 07/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class NotificationDetailsViewController:  UIViewController, PopupContentViewController {
    var notification : Notification!
    var closeHandler: (() -> Void)?
    var size : CGSize!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var senderLabel: UILabel!
    var htmlContent = "<html><head><title>Loading</title></head><body>Please Wait</body>"

    @IBOutlet weak var okayButton : UIButton!
    @IBOutlet weak var webView : UIWebView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadNotificationDetails(notification)
        self.titleLabel.text = notification.subject.capitalizedString
        self.senderLabel.text = "From: \(notification.sender.name.capitalizedString)"
        self.webView.scalesPageToFit = true
        self.webView.contentMode = UIViewContentMode.ScaleAspectFit
        CommonClass.makeViewCircularWithCornerRadius(okayButton, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 5)
        webView.loadHTMLString(htmlContent, baseURL: nil)
    }

    func loadNotificationDetails(notification:Notification){
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
           DashBoardService.sharedInstance.getNotificationDetailWithToken(token, notificatinID: self.notification.messageID, completionBlock: { (messageResponse) in
            if let detailedMessage = messageResponse as? Notification{
                //self.notification = detailedMessage
                self.htmlContent = detailedMessage.messageContent//"<html><head><title></title></head><body>\(detailedMessage.messageContent)</body></html>"
                self.webView.loadHTMLString(self.htmlContent, baseURL: nil)
                // self.webView.reload()
                // self.webView.loadHTMLString(notification.messageContent, baseURL: nil)

            }
           })
        }


    }

    func webViewDidFinishLoad(webView: UIWebView) {
        self.webView.scalesPageToFit = true
        self.webView.contentMode = UIViewContentMode.ScaleAspectFit
//        let zoom = webView.bounds.size.width / webView.scrollView.contentSize.width
//        webView.scrollView.setZoomScale(zoom, animated: true)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //textView.scrollRangeToVisible(NSMakeRange(0, 0))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    class func instance() -> NotificationDetailsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewControllerWithIdentifier("NotificationDetailsViewController") as! NotificationDetailsViewController
    }

    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSizeMake(self.size.width - 20, self.size.height*5/6)
    }

    @IBAction func didTapCloseButton(sender: AnyObject) {
        closeHandler?()
    }
    
}
