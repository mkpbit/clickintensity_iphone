//
//  FrontLineModel.swift
//  ClickIntensity
//
//  Created by TecOrb on 31/08/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class FrontLineModel: NSObject {
    let kFlagUrl = "flagUrl"
    let kMaxUsers = "maxusers"
    let kMaxRank = "maxrank"
    let kMaxLevel = "maxlevel"
    let kMaxGoldPacks = "maxgoldpacks"
    let kMaxSilverPacks = "maxsilverpacks"
    let kTotalUsers = "totalusers"
    let kMembers = "members"

    //Properties

    var flagUrl = ""
    var maxUsers = ""
    var maxRank = ""
    var maxLevel = ""
    var maxGoldPacks = ""
    var maxSilverPacks = ""
    var totalUsers = ""
    var members = [MemberModel]()


    override init() {
        super.init()
    }

    init(json : JSON){
        if let furl = json[kFlagUrl].string as String?{
            self.flagUrl = furl
        }

        if let maxusers = json[kMaxUsers].string as String?{
            self.maxUsers = maxusers
        }else if let maxusers = json[kMaxUsers].intValue as NSInteger?{
            self.maxUsers = "\(maxusers)"
        }

        if let maxrank = json[kMaxRank].string as String?{
            self.maxRank = maxrank
        }else if let maxrank = json[kMaxRank].intValue as NSInteger?{
            self.maxRank = "\(maxrank)"
        }

        if let maxlevel = json[kMaxLevel].string as String?{
            self.maxLevel = maxlevel
        }else if let maxlevel = json[kMaxLevel].intValue as NSInteger?{
            self.maxLevel = "\(maxlevel)"
        }

        if let maxgoldpacks = json[kMaxGoldPacks].string as String?{
            self.maxGoldPacks = maxgoldpacks
        }else if let maxgoldpacks = json[kMaxGoldPacks].intValue as NSInteger?{
            self.maxGoldPacks = "\(maxgoldpacks)"
        }

        if let maxsilverpacks = json[kMaxSilverPacks].string as String?{
            self.maxSilverPacks = maxsilverpacks
        }else if let maxsilverpacks = json[kMaxSilverPacks].intValue as NSInteger?{
            self.maxSilverPacks = "\(maxsilverpacks)"
        }

        if let totalusers = json[kTotalUsers].string as String?{
            self.totalUsers = totalusers
        }else if let totalusers = json[kTotalUsers].intValue as NSInteger?{
            self.totalUsers = "\(totalusers)"
        }

        if let memberArray = json[kMembers].arrayObject as! [[String:AnyObject]]?{
            for m in memberArray{
                let member = MemberModel(dict: m)
                self.members.append(member)
            }
        }
        super.init()
    }
}

class FrontLineParser: NSObject {
    let kFlagURL = "flagUrl"
    let kMaxRank = "maxrank"//" = 0,
    let kMaxUsers = "maxusers"//: 5,
    let kMaxLevel = "maxlevel"//: 1,
    let kMaxGoldenPacks = "maxgoldpacks"//: 1,
    let kMaxSilverPacks = "maxsilverpacks"//: 1,
    let kTotalUsers = "totalusers"//: 17,
    let kMembers = "members"

    var flagURL : String = ""
    var maxRank : Int = 0
    var maxUsers : Int = 0
    var maxLevel : Int = 1
    var maxGoldenPacks : Int = 1
    var maxSilverPacks : Int = 1
    var totalUsers : Int = 17
    var frontLineMembers = [FrontLineModel]()

    override init() {
        super.init()
    }

    init(json : JSON){
        if let furl = json[kFlagURL].string as String?{
            self.flagURL = furl
        }

        if let maxRnk = json[kMaxRank].int as Int?{
            self.maxRank = maxRnk
        }
        if let maxUsrs = json[kMaxUsers].int as Int?{
            self.maxUsers = maxUsrs
        }

        if let maxlvl = json[kMaxLevel].int as Int?{
            self.maxLevel = maxlvl
        }

        if let maxgldPacks = json[kMaxGoldenPacks].int as Int?{
            self.maxGoldenPacks = maxgldPacks
        }

        if let maxslvrPacks = json[kMaxSilverPacks].int as Int?{
            self.maxSilverPacks = maxslvrPacks
        }

        if let ttlUsrs = json[kTotalUsers].int as Int?{
            self.totalUsers = ttlUsrs
        }

        if let mmbrs = json[kMembers].array as [JSON]?{
            frontLineMembers.removeAll()
            for m in mmbrs{
                let member = FrontLineModel(json: m)
                frontLineMembers.append(member)
            }
        }


        super.init()

    }


}
