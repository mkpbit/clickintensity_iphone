//
//  MyProspectViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class MyProspectViewController: UIViewController,SlideNavigationControllerDelegate {
    @IBOutlet weak var myProspectTableView : UITableView!
    var pageNumber = 1
    var prospectArray = [ProspectModel]()
    var totalRows = 0
    var isNewDataLoading = false

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(TeamViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        return refreshControl
    }()

    func handleRefresh(refreshControl: UIRefreshControl) {
        pageNumber = 1
        prospectArray.removeAll()
        isNewDataLoading = true
        myProspectTableView.reloadData()
        self.loadMyProspectsWithPageNumber(self.pageNumber)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        myProspectTableView.tableFooterView = UIView()
        myProspectTableView.addSubview(refreshControl)
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        // Do any additional setup after loading the view.
        self.loadMyProspectsWithPageNumber(pageNumber)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }

    @IBAction func onclickDealButton(sender:UIButton){
        if let cell = sender.tableViewCell() as? MyProspectCell{
            cell.dealButton.selected = !cell.dealButton.selected
        }
    }

    func loadMyProspectsWithPageNumber(pageNumber :Int) {
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
        ProspectService.sharedInstance.getProspectsListWith(token, pageNumber: pageNumber, completionBlock: { (responseArr, rowsCount) in
            if self.refreshControl.refreshing{
                self.refreshControl.endRefreshing()
            }
            self.isNewDataLoading = false
            if let prospArr = responseArr as? [ProspectModel]{
                if prospArr.count == 0 && self.pageNumber > 1{
                    self.pageNumber -= 1
                }
                self.totalRows = rowsCount
                self.prospectArray.appendContentsOf(prospArr)
                self.myProspectTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber -= 1
                }
            }
        })
        }
    }
}

extension MyProspectViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prospectArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let prospect = prospectArray[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("MyProspectCell", forIndexPath: indexPath) as! MyProspectCell
        cell.userNameLabel.text = prospect.name
        cell.emailLabel.text = prospect.email
        cell.dealButton.selected = (prospect.status.lowercaseString == "cold") ? false : true
        cell.dealButton.addTarget(self, action: #selector(onclickDealButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        //showErrorWithMessage(FUNCTIONALITY_PENDING_MESSAGE)
        let prospect = prospectArray[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("LeadsViewController") as! LeadsViewController
        vc.myProspect = prospect
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView == myProspectTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if pageNumber >= 1{
                        pageNumber+=1
                        }
                        self.loadMyProspectsWithPageNumber(pageNumber)
                    }
                }
            }
        }
    }
}
