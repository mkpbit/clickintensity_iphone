//
//  NKProfileContainerView.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class NKProfileContainerView: UIView {

    override func drawRect(rect: CGRect)
    {
        drawRingFittingInsideView()
        setNeedsDisplay()
    }

    internal func drawRingFittingInsideView()->()
    {
        let halfSize:CGFloat = min( frame.size.width/2, frame.size.height/2)
        let desiredLineWidth:CGFloat = 0.5    // your desired value

        let circlePathOuter = UIBezierPath(
            arcCenter: CGPoint(x:halfSize,y:halfSize),
            radius: CGFloat( halfSize - (desiredLineWidth/2) ),
            startAngle: CGFloat(0),
            endAngle:CGFloat(M_PI * 2),
            clockwise: true)
        let circlePathInner = UIBezierPath(
            arcCenter: CGPoint(x:halfSize,y:halfSize),
            radius: CGFloat( halfSize - 3 - (desiredLineWidth/2)),
            startAngle: CGFloat(0),
            endAngle:CGFloat(M_PI * 2),
            clockwise: true)


        let shapeLayerOuter = CAShapeLayer()
        shapeLayerOuter.path = circlePathOuter.CGPath
        shapeLayerOuter.fillColor = UIColor.clearColor().CGColor
        shapeLayerOuter.strokeColor = UIColor(red: 69.0/255.0, green: 84.0/255.0, blue: 97.0/255.0, alpha: 1.0).CGColor
        shapeLayerOuter.lineWidth = desiredLineWidth
        layer.addSublayer(shapeLayerOuter)

        let shapeLayerInner = CAShapeLayer()
        shapeLayerInner.path = circlePathInner.CGPath
        shapeLayerInner.fillColor = UIColor.clearColor().CGColor
        shapeLayerInner.strokeColor = UIColor(red: 69.0/255.0, green: 84.0/255.0, blue: 97.0/255.0, alpha: 1.0).CGColor
        shapeLayerInner.lineWidth = desiredLineWidth
        layer.addSublayer(shapeLayerInner)
    }
}