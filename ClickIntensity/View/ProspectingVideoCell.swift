//
//  ProspectingVideoCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 15/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class ProspectingVideoCell: UICollectionViewCell {
    @IBOutlet weak var videoThumb : UIImageView!
    @IBOutlet weak var shareButton : UIButton!
    @IBOutlet weak var moreButton : UIButton!
    @IBOutlet weak var videoNameLabel : UILabel!
    
    override var bounds: CGRect {
        didSet {
            contentView.frame = bounds
        }
    }
    override func layoutSubviews() {

    }

    override func awakeFromNib() {
        self.contentView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
    }

}
