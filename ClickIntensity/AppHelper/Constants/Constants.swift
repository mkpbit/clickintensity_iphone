//
//  Constants.swift
//  Sinr
//
//  Created by TecOrb on 23/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import TSMessages

let kUserDefaults = NSUserDefaults.standardUserDefaults()
var DEVICE_TOKEN :String!
let kDeviceToken = "DeviceToken"
//"fname": "Abc",
//"lname": "xyz",
//"email": "abc@gmail.com",
//"gender": "male",
//"contact": "7834821711",
//"is_confirm": true
/*================== SOME GLOBALS FOR USERS ====================================*/
let kIsLoggedIN = "is_logged_in"
let kFirstName = "fname"
let kLastName = "lname"
let kEmail = "email"
let kGender = "gender"
let kContact = "contact"
let kPassword = "password"
let kIsConfirmed = "is_confirm"
let kSocialID = "provider_id"
let kProfilePic = "profile_pic"
let kUserToken = "token"
/*================== API URLs ====================================*/
//let BASE_URL = "http://45.55.35.34/"
//let BASE_URL = "https://api.staging.clickintensity.com/"

let BASE_URL = "http://staging.clickintensity.com/"
//varify sponserId
let VARIFY_SPONSER = "\(BASE_URL)api/utilities/verify-sponsor"
let VALIDATE_USER_NAME = "\(BASE_URL)api/users/verify-user-name"
let SIGN_UP_USER = "\(BASE_URL)api/users"
let CREATE_USER = "\(BASE_URL)api/genealogys/create"
let REGISTER_SPONSER = "\(BASE_URL)api/utilities/register-sponsor"

let LOGIN_USER = "\(BASE_URL)auth/local"
let SOCIAL_LOGIN_USER = "\(BASE_URL)auth/google/callback"
let SALES_STATUS_URL = "\(BASE_URL)api/utilities/sales-status"
let USER_DASHBOARD = "\(BASE_URL)api/users/me"
let CREATE_PROSPECT = "\(BASE_URL)api/prospects/create"
let LIST_PROSPECT = "\(BASE_URL)api/prospects"
let TEAM_FRONTLINE_COUNT = "\(BASE_URL)api/genealogys/total-signups/TIME_STRING"

let TEAM_URL = "\(BASE_URL)api/genealogys/all-members-list"

let REGISTRATION_LIST_URL = "\(BASE_URL)api/utilities/latest-signups"
let REGISTRATION_COUNT_BY_DAYS = "\(BASE_URL)api/genealogys/signup-counts"

let COMMISSION_LIST_URL = "\(BASE_URL)api/genealogys/all-members-list"
let COMMISSION_COUNT_BY_DAYS = "\(BASE_URL)api/campaigns/total-commission"

let SALES_LIST_URL = "\(BASE_URL)api/genealogys/all-members-list"
let SALES_COUNT_BY_DAYS = "\(BASE_URL)api/genealogys/all-members-list"

let NOTIFICATION_LIST_URL = "\(BASE_URL)api/team-communications/"
let MESSAGE_DETAIL_URL = "\(BASE_URL)api/team-communications/"

let RESOURCE_VIDEO_URL = "\(BASE_URL)api/utilities/static-content"

//var headers = [
//    "Accept": "application/json"
//]

let kGoogleClientId = "968376927549-vjd6ekt3r2hq21rfdi1tgptl1arp051u.apps.googleusercontent.com"
/*================== SOCIAL LOGIN TYPE ====================================*/
enum SocialLoginType: String {
    case Facebook = "Facebook"
    case Google = "Google"
}
let FACEBOOK_PAGE_URL = "https://www.facebook.com/cicorporate/posts/"

/*======================== NOTIFICATION ==================================*/
let TOTAL_SIGNUPS_LOADED_NOTIFICATION = "totalSignUpsLoadedNotification"
let ALL_USERS_LOADED_NOTIFICATION = "allUsersLoadedNotification"
let PROFILE_UPDATED_NOTIFICATION = "profileUpdatedNotification"

/*======================== CONSTANT MESSAGES ==================================*/

let NETWORK_NOT_CONNECTED_MESSAGE = "Network is not connected!"
let FUNCTIONALITY_PENDING_MESSAGE = "This functionality is under development. Please ignore it!"


/*============== SOCIAL MEDIA URL SCHEMES ==================*/

let FACEBOOK_URL_SCHEME = "fb599499053559542"
let SELF_URL_SCHEME = "com.tecorb.ClickIntensity"
let GOOGLE_URL_SCHEME = "com.googleusercontent.apps.968376927549-vjd6ekt3r2hq21rfdi1tgptl1arp051u"

/*============== SIGNUP FIELDS ==================*/


/*============== PRINTING IN DEBUG MODE ==================*/

func print_debug <T>(object : T){
    // print(object)
}

func print_log <T>(object : T){
    //NSLog("\(object)")
}


/*============== SHOW MESSAGE ==================*/
func showSuccessWithMessage(message: String)
{
    TSMessage.showNotificationWithTitle(message, type: TSMessageNotificationType.Success)
}
func showErrorWithMessage(message: String)
{
    TSMessage.showNotificationWithTitle(message, type: TSMessageNotificationType.Error)
}
func showWarningWithMessage(message: String)
{
    TSMessage.showNotificationWithTitle(message, type: TSMessageNotificationType.Warning)
}
func showMessage(message: String)
{
    TSMessage.showNotificationWithTitle(message, type: TSMessageNotificationType.Message)
}

