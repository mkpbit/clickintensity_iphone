//
//  MenuResourceCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 04/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class MenuResourceCell: UITableViewCell {
    @IBOutlet weak var prospectingVideosButton : UIButton!
    @IBOutlet weak var trainingVideosButton : UIButton!
    @IBOutlet weak var audiosButton : UIButton!
    @IBOutlet weak var imagesButton : UIButton!
    @IBOutlet weak var facebookPostsButton : UIButton!
    @IBOutlet weak var emailCopiesButton : UIButton!
    @IBOutlet weak var oneLinerButton : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
