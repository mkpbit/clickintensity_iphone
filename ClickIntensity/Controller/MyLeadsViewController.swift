//
//  MyLeadsViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 06/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class MyLeadsViewController: UIViewController,SlideNavigationControllerDelegate {
    @IBOutlet weak var frontLineTableView : UITableView!
    var frontLine : FrontLineModel?
    var user : UserModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationItem.leftBarButtonItem = nil
        frontLineTableView.tableFooterView = UIView()
        let userJson = UserModel.loadUserInfo()
        user = UserModel(json: userJson)
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            self.loadFrontLine(token, userID: self.user.ID, pageNumber: 1, recordPerPage: 50, listOrder: ListOrder.Ascending, fromDate: self.user.creatDate, toDate: NSDate().iso8601, listedUsersTeamMaximumSize: 100, listUsersRank: 0, listUsersMinimumGoldPacks: 0, listUsersMaximumGoldPacks: 20, listUsersMinimumSilverPacks: 0, listUsersMaximumSilverPacks: 20, searchFieldName: "0", searchFieldValue: "0")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickBack(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if frontLine != nil{
            return frontLine!.members.count
        }
        return 0
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("FrontLineCell", forIndexPath: indexPath) as? FrontLineCell{
            let member = frontLine!.members[indexPath.row]
            cell.imageView?.sd_setImageWithURL(NSURL(string:member.email), placeholderImage: UIImage(named: "frontline_dp"))
            cell.DOJLabel.text = CommonClass.dateWithString(member.joinAt)
            cell.userNameLabel.text = member.name
            cell.emailLabel.text = member.email
            return cell
        }
        return UITableViewCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        showErrorWithMessage(FUNCTIONALITY_PENDING_MESSAGE)
        //show the lead screen

    }


    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}

extension MyLeadsViewController{


    func loadFrontLine(token:String,userID: String, pageNumber: Int, recordPerPage: Int, listOrder: ListOrder, fromDate: String, toDate: String, listedUsersTeamMaximumSize: Int, listUsersRank: Int, listUsersMinimumGoldPacks: Int, listUsersMaximumGoldPacks: Int, listUsersMinimumSilverPacks: Int, listUsersMaximumSilverPacks: Int, searchFieldName: String, searchFieldValue: String){

        DashBoardService.sharedInstance.getFrontLine(token, userID: userID, pageNumber: pageNumber, recordPerPage: recordPerPage, listOrder: listOrder, fromDate: fromDate, toDate: toDate, listedUsersTeamMaximumSize: listedUsersTeamMaximumSize, listUsersRank: listUsersRank, listUsersMinimumGoldPacks: listUsersMinimumGoldPacks, listUsersMaximumGoldPacks: listUsersMaximumGoldPacks, listUsersMinimumSilverPacks: listUsersMinimumSilverPacks, listUsersMaximumSilverPacks: listUsersMaximumSilverPacks, searchFieldName: searchFieldName, searchFieldValue: searchFieldValue) { (responseFrontLine) in
            if let frntline = responseFrontLine as? FrontLineModel{
                self.frontLine = frntline
                self.frontLineTableView.reloadData()
            }
        }
    }
    
}

