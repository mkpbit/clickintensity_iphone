//
//  Extensions.swift
//  Sinr
//
//  Created by TecOrb on 24/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    //MARK: - method for UITableView
    func tableViewCell() -> UITableViewCell? {
        var tableViewcell : UIView? = self
        while(tableViewcell != nil)
        {
            if tableViewcell! is UITableViewCell {
                break
            }
            tableViewcell = tableViewcell!.superview
        }
        return tableViewcell as? UITableViewCell
    }

    func tableViewIndexPath(tableView: UITableView) -> NSIndexPath? {

        if let cell = self.tableViewCell() {

            return tableView.indexPathForCell(cell)
        }else {
            return nil
        }
    }

    //MARK: - method for UICollectionView
    func collectionViewCell() -> UICollectionViewCell? {

        var collectionViewcell : UIView? = self

        while(collectionViewcell != nil)
        {
            if collectionViewcell! is UICollectionViewCell {
                break
            }
            collectionViewcell = collectionViewcell!.superview
        }
        return collectionViewcell as? UICollectionViewCell
    }

    func collectionViewIndexPath(collectionView: UICollectionView) -> NSIndexPath? {
        if let cell = self.collectionViewCell(){
            return collectionView.indexPathForCell(cell)
        }else {
            return nil
        }
    }

    func addShadowView(width:CGFloat=0.2, height:CGFloat=0.2, Opacidade:Float=0.7, maskToBounds:Bool=false, radius:CGFloat=0.5){
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = Opacidade
        self.layer.masksToBounds = maskToBounds
    }
}

extension NSUserDefaults {

    class func save(value:AnyObject,forKey key:String)
    {
        NSUserDefaults.standardUserDefaults().setObject(value, forKey:key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    class func removeFromUserDefaultForKey(key:String)
    {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    class func getDataFromUserDefaultForKey(key:String) -> String
    {
        return NSUserDefaults.standardUserDefaults().objectForKey(key) as? String ?? ""
    }

    class func getAnyDataFromUserDefault(key:String) -> AnyObject
    {
        //println_debug("Any Data ---->  \(NSUserDefaults.standardUserDefaults().objectForKey(key) ?? 0)")
        return NSUserDefaults.standardUserDefaults().objectForKey(key) ?? false
    }

    class func userdefaultForArray(key:String) -> Array <AnyObject>
    {
        return NSUserDefaults.standardUserDefaults().objectForKey(key) as! Array
    }
    
}


extension String {

    func base64Encoded() -> String {
        let plainData = dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        return base64String!
    }

    func base64Decoded() -> String {
        let decodedData = NSData(base64EncodedString: self, options:NSDataBase64DecodingOptions(rawValue: 0))
        let decodedString = NSString(data: decodedData!, encoding: NSUTF8StringEncoding)
        return decodedString as! String
    }

    var dateFromISO8601: NSDate? {
        return NSDate.Formatter.iso8601.dateFromString(self)
    }

}

//extension String {
//    func contains(other: String) -> Bool{
//        let start = startIndex
//
//        repeat{
//
//            let subString = self[Range(start..<endIndex)]
//            if subString.hasPrefix(other){
//                return true
//            }
//        }while start != endIndex
//
//        return false
//    }
//
//    func containsIgnoreCase(other: String) -> Bool{
//        let start = startIndex
//        repeat{
//            let subString = self[Range(start..<endIndex)].lowercaseString
//            if subString.hasPrefix(other.lowercaseString){
//                return true
//            }
//        }while start != endIndex
//        
//        return false
//    }
//}
//


extension NSDate {
    struct Formatter {
        static let iso8601: NSDateFormatter = {
            let formatter = NSDateFormatter()
            formatter.calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierISO8601)
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
            return formatter
        }()
    }
    var iso8601: String {
        return Formatter.iso8601.stringFromDate(self)
    }


//    if let dateFromString = stringFromDate.dateFromISO8601 {
//        print(dateFromString.iso8601)      // "2016-06-18T05:18:27.935Z"
//    }

}
extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false

        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
            isGreater = true
        }

        //Return Result
        return isGreater
    }

    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false

        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedAscending {
            isLess = true
        }

        //Return Result
        return isLess
    }

    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false

        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedSame {
            isEqualTo = true
        }

        //Return Result
        return isEqualTo
    }

    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.dateByAddingTimeInterval(secondsInDays)

        //Return Result
        return dateWithDaysAdded
    }

    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: NSTimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.dateByAddingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}




class NKCountLabel : UILabel {

    var textInsets: UIEdgeInsets = UIEdgeInsetsZero {
        didSet { invalidateIntrinsicContentSize() }
    }

    override func textRectForBounds(bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var rect = textInsets.apply(bounds)
        rect = super.textRectForBounds(rect, limitedToNumberOfLines: numberOfLines)
        return textInsets.inverse.apply(rect)
    }

    override func drawTextInRect(rect: CGRect) {
        super.drawTextInRect(textInsets.apply(rect))
    }

}

@IBDesignable
extension NKCountLabel {

    // currently UIEdgeInsets is no supported IBDesignable type,
    // so we have to fan it out here:
    @IBInspectable
    var leftTextInset: CGFloat {
        set { textInsets.left = newValue }
        get { return textInsets.left }
    }

    @IBInspectable
    var rightTextInset: CGFloat {
        set { textInsets.right = newValue }
        get { return textInsets.right}
    }

    @IBInspectable
    var topTextInset: CGFloat {
        set { textInsets.top = newValue }
        get { return textInsets.top }
    }

    @IBInspectable
    var bottomTextInset: CGFloat {
        set { textInsets.bottom = newValue }
        get { return textInsets.bottom }
    }
    // Same for the right, top and bottom edges.
}

extension UIEdgeInsets {
    var inverse: UIEdgeInsets {
        return UIEdgeInsets(top: -top, left: -left, bottom: -bottom, right: -right)
    }

    func apply(rect: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(rect, self)
    }
}
