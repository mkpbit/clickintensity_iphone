//
//  NotificationCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var userImageView : UIImageView!
    @IBOutlet weak var selectionImageView : UIImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var notificationTextLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            self.selectionImageView.image = UIImage(named: "item_selected")
        }else{
            self.selectionImageView.image = nil
        }
    }

    override func layoutSubviews() {
        //  CommonClass.makeViewCircular(userImageView, borderColor: UIColor.clearColor(), borderWidth: 6)
    }

}
