//
//  CommonClass.swift
//  Sinr
//
//  Created by TecOrb on 23/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire
import SwiftyJSON
import KVNProgress
import TSMessages
import CFNetwork
class CommonClass: NSObject {

    class func dateWithString(dateString: String) -> String {

        let dayTimePeriodFormatter = NSDateFormatter()

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dfs = dateString.stringByReplacingOccurrencesOfString("T", withString: " ")
        let dArray  = dfs.componentsSeparatedByString(".")
        if dArray.count>0{
            if let d = dayTimePeriodFormatter.dateFromString(dArray[0]) as NSDate?{
                dayTimePeriodFormatter.dateFormat = "dd-MM-YYYY"
                let date = dayTimePeriodFormatter.stringFromDate(d)
                return date
            }
        }
        return " "
    }

    class func stringFromCurrentTime() -> String{
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dateString = dateFormatter.stringFromDate(date)
        return dateString
    }

  class func getWiFiAddress() -> String? {
        var address : String?

        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs> = nil
        if getifaddrs(&ifaddr) == 0 {

            // For each interface ...
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr.memory.ifa_next }

                let interface = ptr.memory

                // Check for IPv4 or IPv6 interface:
                let addrFamily = interface.ifa_addr.memory.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                    // Check interface name:
                    if let name = String.fromCString(interface.ifa_name) where name == "en0" {

                        // Convert interface address to a human readable string:
                        var addr = interface.ifa_addr.memory
                        var hostname = [CChar](count: Int(NI_MAXHOST), repeatedValue: 0)
                        getnameinfo(&addr, socklen_t(interface.ifa_addr.memory.sa_len),
                                    &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST)
                        address = String.fromCString(hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        
        return address
    }

    class func makeViewCircular(view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.CGColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.width/2
        view.layer.masksToBounds = true
    }
    class func makeViewCircularWithRespectToHeight(view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.CGColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.masksToBounds = true
    }

    class func makeViewCircularWithCornerRadius(view:UIView,borderColor:UIColor,borderWidth:CGFloat, cornerRadius: CGFloat)
    {
        view.layer.borderColor = borderColor.CGColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }


    class var isConnectedToNetwork : Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection) ? true : false
    }


    class func setPlaceHolder(textField: UITextField, placeHolderString placeHolder: String, withColor color: UIColor){
    let p = NSAttributedString(string: placeHolder, attributes: [NSForegroundColorAttributeName : color])
    textField.attributedPlaceholder = p;
    }

    class var isRunningSimulator: Bool
        {
        get
        {
            return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
            //return TARGET_IPHONE_SIMULATOR != 0 // Use this line in Xcode 6 or earlier
        }
    }

    //MARK:- Loader display Methods
    class func showLoader()
    {
        KVNProgress.show()
    }

    class func showLoaderWithStatus(status: String)
    {
        KVNProgress.showWithStatus(status)
    }

    class func updateLoaderWithStatus(status: String)
    {
        KVNProgress.updateStatus(status)
    }

    class func hideLoader()
    {
        KVNProgress.dismiss()
    }


    

    //MARK:- Email Validation
    class func isValidEmailAddress(emailStr: String) -> Bool
    {
        if((emailStr.isEmpty) || emailStr.characters.count == 0)
        {
            return false
        }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@",emailRegex)
        if(emailPredicate.evaluateWithObject(emailStr)){
            return true
        }
        return false
    }
    class func validateUserName(username: String) -> Bool {

        let MINIMUM_LENGTH_LIMIT_USERNAME = 1
        let MAXIMUM_LENGTH_LIMIT_USERNAME = 20

        let nameRegex = "[a-zA-Z _.@ ]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegex)

        if username.characters.count == 0
        {
            return false
        }

        else if username.characters.count < MINIMUM_LENGTH_LIMIT_USERNAME
        {
            return false
        }

        else if username.characters.count > MAXIMUM_LENGTH_LIMIT_USERNAME
        {
            return false
        }

        else if !nameTest.evaluateWithObject(username)
        {
            return false
        }

        else
        {
            return true
        }
    }

    //Password Validation
    class func validatePassword(password: String) -> Bool {

        let MINIMUM_LENGTH_LIMIT_PASSWORD = 6
        let MAXIMUM_LENGTH_LIMIT_PASSWORD = 20

        if password.characters.count == 0
        {
            return false
        }

        else if password.substringToIndex(password.startIndex.successor()) == " "
        {
            return false
        }

        else if password.characters.count < MINIMUM_LENGTH_LIMIT_PASSWORD
        {
            return false
        }

        else if password.characters.count > MAXIMUM_LENGTH_LIMIT_PASSWORD
        {
            return false
        }

        else
        {
            return true
        }
    }

    //Email Validation
    class func validateEmail(email : String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        if email.characters.count == 0
        {
            return false
        }

        else if !emailTest.evaluateWithObject(email)
        {
            return false
        }

        else
        {
            return true
        }
    }

    class func matchConfirmPassword(password :String , confirmPassword : String)-> Bool{


        if password==confirmPassword {
            return true
        }
        else{
            return false
        }
    }
    class func validatePhoneNumber(phone : String) -> Bool {
        let PHONE_REGEX = "^\\d{10}"
        let PhoneTest = NSPredicate(format:"SELF MATCHES %@", PHONE_REGEX)
        if !PhoneTest.evaluateWithObject(phone){
            return false

        }
        else{
            return true
        }
    }

    class func registerForRemoteNotification()
    {
        if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0
        {
            let pushSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Badge ,UIUserNotificationType.Sound ,UIUserNotificationType.Alert], categories: nil)

            UIApplication.sharedApplication().registerUserNotificationSettings(pushSettings)
        }
        else
        {
            UIApplication.sharedApplication().registerForRemoteNotifications()
        }

    }


    class func dictionToJSON(dictionay:[String:AnyObject]) -> NSData{
        var json : NSData?
        do{
            let jsonData = try NSJSONSerialization.dataWithJSONObject(dictionay, options: NSJSONWritingOptions.PrettyPrinted)
            json = jsonData//NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        }catch{
            json = NSData()
        }
        return json!
    }

}
