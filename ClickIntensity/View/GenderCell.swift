//
//  GenderCell.swift
//  Sinr
//
//  Created by TecOrb on 28/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class GenderCell: UITableViewCell {
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onClickMale(sender:UIButton){
        sender.selected = true
        femaleButton.selected = false
    }
    @IBAction func onClickFemale(sender:UIButton){
        sender.selected = true
        maleButton.selected = false
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
