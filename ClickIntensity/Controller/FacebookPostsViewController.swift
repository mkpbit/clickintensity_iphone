//
//  FacebookPostsViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 15/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import KVNProgress
class FacebookPostsViewController: UIViewController, SlideNavigationControllerDelegate,UIWebViewDelegate {
    @IBOutlet weak var fbWebView : UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL (string: FACEBOOK_PAGE_URL);
        let requestObj = NSURLRequest(URL: url!);
        fbWebView.delegate = self
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        fbWebView.loadRequest(requestObj);
        // Do any additional setup after loading the view.
    }
    func webViewDidStartLoad(webView: UIWebView) {
        CommonClass.showLoader()
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        CommonClass.hideLoader()
        KVNProgress.showErrorWithStatus(error?.localizedDescription)
    }

    func webViewDidFinishLoad(webView: UIWebView) {
        CommonClass.hideLoader()
        webView.scalesPageToFit = true
        webView.contentMode = .ScaleAspectFill
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
