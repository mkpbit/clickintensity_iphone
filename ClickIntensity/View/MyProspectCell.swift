//
//  MyProspectCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 07/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class MyProspectCell: UITableViewCell {
    @IBOutlet weak var userImageView : UIImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var emailLabel : UILabel!
    @IBOutlet weak var dealButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
