//
//  SignInViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 01/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import TwitterKit
import Twitter
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON
import Google


class SignInViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate {

    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.hidden = true
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().clientID = kGoogleClientId
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        let statusBar: UIView = UIApplication.sharedApplication().valueForKey("statusBar") as! UIView
        if statusBar.respondsToSelector(Selector("setBackgroundColor:")) {
            statusBar.backgroundColor = UIColor.clearColor()
        }
    }

    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        var token = ""
        if (error == nil)
        {
            if let _token = user.authentication.accessToken {
                token = _token
            }
            if !CommonClass.isConnectedToNetwork{
                showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
                return;
            }else{
                LoginService.sharedInstance.loginWithGoogleAuth(token, completionBlock: { (response) in
                    if response != nil{
                        let json = JSON(response!)
                        showSuccessWithMessage("\(json)")
                    }
                })
            }
        }
        else
        {
        }
    }
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
    }


    @IBAction func onClickSignIn(sender: UIButton){

        let email = emailTextField.text!
        let password = passwordTextField.text!
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        if email.characters.count == 0
        {
            showErrorWithMessage("Email cann't be empty")
            return
        }
        if !CommonClass.validateEmail(email){
            showErrorWithMessage("Email is not in correct format")
            return
        }

        if password.characters.count == 0
        {
            showErrorWithMessage("Password cann.t be empty")
            return
        }
        if !CommonClass.validatePassword(password){
            showErrorWithMessage("Enter a correct password")
            return
        }

        LoginService.sharedInstance.loginWithParams(email, password: password) { (userToken) in
            if let token = userToken as? String{
                kUserDefaults.setValue(token, forKey: kUserToken)
                DashBoardService.sharedInstance.getUserDashBoardWithToken(token, completionBlock: { (response) in

                    if let user = response as? UserModel{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let menu = mainStoryboard.instantiateViewControllerWithIdentifier("LeftMenuViewController") as! LeftMenuViewController
                        menu.user = user
                        let nav = mainStoryboard.instantiateViewControllerWithIdentifier("SlideNavigationController") as! SlideNavigationController
                        nav.rightMenu = menu
                        nav.enableSwipeGesture = true
                        UIApplication.sharedApplication().delegate?.window??.rootViewController = nav
                    }
                })
            }
        }

    }



    @IBAction func onClickTwitterButton(sender: UIButton){
        showErrorWithMessage(FUNCTIONALITY_PENDING_MESSAGE)
        return

//        if !CommonClass.isConnectedToNetwork{
//            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
//            return
//        }
//        Twitter.sharedInstance().logInWithMethods(.WebBased) { (session, error) in
//            if let unwrappedSession = session {
//                let userId = unwrappedSession.userID
//                print_debug("userid: \(userId)")
//                let client = TWTRAPIClient.clientWithCurrentUser()
//                let request = client.URLRequestWithMethod("GET",
//                                                          URL: "https://api.twitter.com/1.1/account/verify_credentials.json",
//                                                          parameters: ["include_email": "true", "skip_status": "true"],
//                                                          error: nil)
//                CommonClass.showLoader()
//
//                client.sendTwitterRequest(request) { response, data, connectionError in
//                    CommonClass.hideLoader()
//                    if connectionError != nil {
//                        print_debug("Error: \(connectionError)")
//                    }
//                    do {
//                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
//                        print_debug("json: \(json)")
//                    } catch let jsonError as NSError {
//                        print_debug("json error: \(jsonError.localizedDescription)")
//                    }
//                }
//            } else {
//                print_debug("Login error: %@", error!.localizedDescription);
//            }
//
//        }
    }

    @IBAction func onClickFacebookButton(sender: UIButton){
        showErrorWithMessage(FUNCTIONALITY_PENDING_MESSAGE)
        return
//        if !CommonClass.isConnectedToNetwork{
//            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
//            return
//        }
//        let login = FBSDKLoginManager()
//        login.loginBehavior = FBSDKLoginBehavior.SystemAccount
//        login.logInWithReadPermissions(["public_profile", "email"], fromViewController: self, handler: {(result, error) in
//            if error != nil {
//                print_debug("Error ======>  \(error.description)")
//            }
//            else if result.isCancelled {
//            }
//            else {
//                CommonClass.showLoader()
//                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]).startWithCompletionHandler({(connection, result, error) -> Void in
//                    if error != nil{
//                        CommonClass.hideLoader()
//                        print_debug("Error ======>  \(error.description)")
//                    }else{
//                        var params = [String:String]()
//                        if let firstName = result.valueForKeyPath("first_name") as? String{
//                            params.updateValue(firstName, forKey: kFirstName)
//                        }else{
//                            params.updateValue(" ", forKey: kFirstName)
//                        }
//                        if let lastName = result.valueForKeyPath("last_name") as? String{
//                            params.updateValue(lastName, forKey: kLastName)
//                        }else{
//                            params.updateValue(" ", forKey: kLastName)
//                        }
//                        if let fbID = result.valueForKeyPath("id") as? String{
//                            params.updateValue(fbID, forKey: kSocialID)
//                        }
//                        if let gender = result.valueForKeyPath("gender") as? String{
//                            params.updateValue(gender, forKey: kGender)
//                        }
//                        if let email = result.valueForKeyPath("email") as? String{
//                            params.updateValue(email, forKey: kEmail)
//                        }
//                        if let profilePicUrl =  result.valueForKeyPath("picture.data.url") as? String{
//                            params.updateValue(profilePicUrl, forKey: kProfilePic)
//                        }
//                        if !CommonClass.isConnectedToNetwork{
//                            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
//                            return;
//                        }else{
//                            LoginService.sharedInstance.loginWithSocialParams(params, loginType: SocialLoginType.Facebook , completionBlock: { response in
//                                CommonClass.hideLoader()
//                                if response != nil{
//                                    let json = JSON(response!)
//                                    showSuccessWithMessage("\(json)")
//                                }
//                            })
//                        }
//                    }
//                })
//            }
//
//        })
    }


    @IBAction func onClickGooglePlusButton(sender: UIButton){
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        GIDSignIn.sharedInstance().signIn()
    }
}
