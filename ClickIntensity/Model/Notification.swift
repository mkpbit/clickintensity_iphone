//
//  Notification.swift
//  ClickIntensity
//
//  Created by TecOrb on 12/10/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class Notification: NSObject {
    //keys for notification
    let kIsImpMessage = "impMessage"
    let kIsSpam = "isSpam"
    let kIsViewed = "isViewed"
    let kMessageID = "messageId"
    let kMessageDate = "msgDate"
    let kReceiverInfo = "receiverInfo"
    let kReceiverInfo1 = "receiver"

    let kSenderInfo = "senderInfo"
    let kReceiverID = "receiverId"
    let kSenderID = "senderId"
    let kSubject = "subject"
    let kMessageContent = "msgContent"

    //values
    var isImpMessage: Bool = false
    var isSpam: Bool = false
    var isViewed: Bool = false
    var messageID = ""
    var messageDate = ""
    var subject = ""
    var messageContent = ""
    var receiverID = ""
    var senderID = ""
    var receiver = SenderModel()
    var sender = SenderModel()

    override init() {
        super.init()
    }

    init(json:JSON) {

        if let _isImpMessage = json[kIsImpMessage].bool as Bool?{
            self.isImpMessage = _isImpMessage
        }
        if let _isSpam = json[kIsSpam].bool as Bool?{
            self.isSpam = _isSpam
        }
        if let _isViewed = json[kIsViewed].bool as Bool?{
            self.isViewed = _isViewed
        }

        if let _messageID = json[kMessageID].string as String?{
            self.messageID = _messageID
        }
        if let _senderID = json[kSenderID].string as String?{
            self.senderID = _senderID
        }
        if let _receiverID = json[kReceiverID].string as String?{
            self.receiverID = _receiverID
        }

        if let _messageDate = json[kMessageDate].string as String?{
            self.messageDate = _messageDate
        }
        if let _subject = json[kSubject].string as String?{
            self.subject = _subject
        }

        if let _subject = json[kSubject].string as String?{
            self.subject = _subject
        }

        if let _content = json[kMessageContent].rawString(NSUTF8StringEncoding, options: .PrettyPrinted) as String?{
            //  print("content json is \(_content)")
            self.messageContent = _content
        }


        if let _receiverInfo = json[kReceiverInfo].dictionaryObject as [String:AnyObject]?{
            self.receiver = SenderModel(dictionary: _receiverInfo)
        }else if let _receiverInfo = json[kReceiverInfo1].dictionaryObject as [String:AnyObject]?{
            self.receiver = SenderModel(dictionary: _receiverInfo)
        }

        if let _senderInfo = json[kSenderInfo].dictionaryObject as [String:AnyObject]?{
            self.sender = SenderModel(dictionary: _senderInfo)
        }
        super.init()
    }

    init(dictionary:[String:AnyObject]) {
        if let _isImpMessage = dictionary[kIsImpMessage] as? Bool{
            self.isImpMessage = _isImpMessage
        }
        if let _isSpam = dictionary[kIsSpam] as? Bool{
            self.isSpam = _isSpam
        }
        if let _isViewed = dictionary[kIsViewed] as? Bool{
            self.isViewed = _isViewed
        }

        if let _messageID = dictionary[kMessageID] as? String{
            self.messageID = _messageID
        }
        if let _messageDate = dictionary[kMessageDate] as? String{
            self.messageDate = _messageDate
        }
        if let _subject = dictionary[kSubject] as? String{
            self.subject = _subject
        }

        if let _receiverInfo = dictionary[kReceiverInfo] as? [String:AnyObject]{
            self.receiver = SenderModel(dictionary: _receiverInfo)
        }else if let _receiverInfo = dictionary[kReceiverInfo1] as? [String:AnyObject]{
            self.receiver = SenderModel(dictionary: _receiverInfo)
        }

        if let _senderInfo = dictionary[kSenderInfo] as? [String:AnyObject]{
            self.sender = SenderModel(dictionary: _senderInfo)
        }

        if let _senderID = dictionary[kSenderID] as? String{
            self.senderID = _senderID
        }
        if let _receiverID = dictionary[kReceiverID] as? String{
            self.receiverID = _receiverID
        }

//        if let _content = dictionary[kMessageContent].rawString(NSUTF8StringEncoding, options: .PrettyPrinted) as String?{
//            // print("content json is \(_content)")
//            self.messageContent = _content
//        }


        super.init()
    }


}

class SenderModel:NSObject{

    let kName = "name"
    let kEmail = "email"
    let kUserName = "username"

    var name : String = ""
    var email = ""
    var userName:String = ""

    override init() {
        super.init()
    }

    init(dictionary:[String:AnyObject]) {
        if let _name = dictionary[kName] as? String{
            self.name = _name
        }
        if let _email = dictionary[kEmail] as? String{
            self.email = _email
        }
        if let _userName = dictionary[kUserName] as? String{
            self.userName = _userName
        }
        super.init()
    }

}

class NotificationParser:NSObject{

    let kMessages = "messages"
    let kLimit = "limit"
    let kTotal = "total"

    var notifications = [Notification]()
    var total: Int = 0
    var limit:Int = 0

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let notes = json[kMessages].arrayObject as? [[String:AnyObject]]{
            self.notifications.removeAll()
            for noteDict in notes{
                let notification = Notification(dictionary: noteDict)
                self.notifications.append(notification)
            }
        }
        if let _total = json[kTotal].int as Int?{
            self.total = _total
        }
        if let _limit = json[kLimit].int as Int?{
            self.limit = _limit
        }
        super.init()
    }
}



