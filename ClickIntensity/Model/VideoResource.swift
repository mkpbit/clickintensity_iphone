//
//  VideoResource.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/01/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON


class VideoResource: NSObject {
    let kVideoURL = "videoUrl"
    let kPageURL = "pageUrl"
    let kTitle = "title"

    var videoURL:String = ""
    var pageURL:String = ""
    var title:String = ""
    var thumbnailURL: String = ""

    override init() {
        super.init()
    }
    init(json : JSON) {
        if let videoData = json["video"].dictionaryObject as [String:AnyObject]?{
            if let _title = videoData[kTitle] as? String{
                self.title = _title
            }
            if let _videoURL = videoData[kVideoURL] as? String{
                self.videoURL = _videoURL
            }

            if let _videoURL = videoData[kVideoURL] as? String{
                let ids = _videoURL.componentsSeparatedByString("/")
                if let id = ids.last as String?{
                    if id.containsString("watch?v="){
                      let pureID = id.stringByReplacingOccurrencesOfString("watch?v=", withString: "")
                        self.thumbnailURL =  "https://img.youtube.com/vi/\(pureID)/hqdefault.jpg"
                    }else{
                        self.thumbnailURL =  "https://img.youtube.com/vi/\(id)/hqdefault.jpg"
                    }
                }
            }

            if let _pageURL = videoData[kPageURL] as? String{
                self.pageURL = _pageURL
            }
        }
        super.init()
    }

    init(videoData:[String:AnyObject]) {
        if let _title = videoData[kTitle] as? String{
            self.title = _title
        }
        if let _videoURL = videoData[kVideoURL] as? String{
            self.videoURL = _videoURL
        }
        if let _pageURL = videoData[kPageURL] as? String{
            self.pageURL = _pageURL
        }

        if let _videoURL = videoData[kVideoURL] as? String{
            let ids = _videoURL.componentsSeparatedByString("/")
            if let id = ids.last as String?{
                if id.containsString("watch?v="){
                    let pureID = id.stringByReplacingOccurrencesOfString("watch?v=", withString: "")
                    self.thumbnailURL =  "https://img.youtube.com/vi/\(pureID)/hqdefault.jpg"
                }else{
                    self.thumbnailURL =  "https://img.youtube.com/vi/\(id)/hqdefault.jpg"
                }
            }
        }

        super.init()
    }

}

class VideoResourceParser: NSObject {
    let kVideo = "videos"
    var videos = [VideoResource]()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _videos = json[kVideo].arrayObject as? [[String:AnyObject]]{
            self.videos.removeAll()
            for _video in _videos{
                let video = VideoResource(videoData: _video)
                self.videos.append(video)
            }
        }
    }

}

