//
//  ProspectModel.swift
//  ClickIntensity
//
//  Created by TecOrb on 01/09/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
class ProspectModel: NSObject {
    let kID = "_id"
    let kUpdatedAt = "updatedAt"
    let kCreatedAt = "createdAt"
    let kName = "name"
    let kEmail = "email"
    let kMobile = "mobile"
    let kUserID = "userid"
    let kV = "__v"
    let kStatus = "status"
    let kActive = "active"


    var ID:String = ""
    var updatedAt:String = ""
    var createdAt:String = ""
    var name:String = ""
    var email:String = ""
    var mobile:String = ""
    var userID:String = ""
    var version:String = ""
    var status:String = ""
    var isActive : Bool = false
    var profileImage = UIImage()

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _id = json[kID].string as String?{
            self.ID = _id
        }else if let _id = json[kID].int as Int?{
            self.ID = "\(_id)"
        }

        if let _updatedAt = json[kUpdatedAt].string as String?{
            self.updatedAt = _updatedAt
        }

        if let _createdAt = json[kCreatedAt].string as String?{
            self.createdAt = _createdAt
        }

        if let _name = json[kName].string as String?{
            self.name = _name
        }

        if let _email = json[kEmail].string as String?{
            self.email = _email
        }
        if let _mobile = json[kMobile].string as String?{
            self.mobile = _mobile
        }

        if let _userid = json[kUserID].string as String?{
            self.userID = _userid
        }
        if let _version = json[kV].string as String?{
            self.version = _version
        }
        if let _status = json[kStatus].string as String?{
            self.status = _status
        }
        if let _isActive = json[kActive].bool as Bool?{
            self.isActive = _isActive
        }
        super.init()
    }

    init(dictionary : [String:AnyObject]) {
        if let _id = dictionary[kID] as? String{
            self.ID = _id
        }else if let _id = dictionary[kID] as? Int{
            self.ID = "\(_id)"
        }

        if let _updatedAt = dictionary[kUpdatedAt] as? String{
            self.updatedAt = _updatedAt
        }

        if let _createdAt = dictionary[kCreatedAt] as? String{
            self.createdAt = _createdAt
        }

        if let _name = dictionary[kName] as? String{
            self.name = _name
        }

        if let _email = dictionary[kEmail] as? String{
            self.email = _email
        }
        if let _mobile = dictionary[kMobile] as? String{
            self.mobile = _mobile
        }

        if let _userid = dictionary[kUserID] as? String{
            self.userID = _userid
        }
        if let _version = dictionary[kV] as? String{
            self.version = _version
        }
        if let _status = dictionary[kStatus] as? String{
            self.status = _status
        }
        if let _isActive = dictionary[kActive] as? Bool{
            self.isActive = _isActive
        }
        super.init()
    }
}

class ProspectParser: NSObject {
    //keys
    let kErrors = "errors"
    let kData = "data"
    let kLimit = "limit"
    let kRows = "rows"
    //properties
    var errors:AnyObject?
    var prospects = [ProspectModel]()
    var limitCount:Int = 0
    var rowsCount: Int = 0

    //initializer
    init(json :JSON) {

        if let data = json[kData].arrayObject as? [[String:AnyObject]]{
            for pdata in data{
                let prospect = ProspectModel(dictionary: pdata)
                self.prospects.append(prospect)
            }
        }

        if let lmt = json[kLimit].int as NSInteger?{
           self.limitCount = lmt
        }

        if let rws = json[kRows].int as NSInteger?{
            self.rowsCount = rws
        }
        super.init()
    }

    init(dictionary :[String:AnyObject]) {
        if let data = dictionary[kData] as? [[String:AnyObject]]{
            for pdata in data{
                let prospect = ProspectModel(dictionary: pdata)
                self.prospects.append(prospect)
            }
        }

        if let lmt = dictionary[kLimit] as? NSInteger{
            self.limitCount = lmt
        }

        if let rws = dictionary[kRows] as? NSInteger{
            self.rowsCount = rws
        }
        super.init()
    }
}
//{
//    "errors": false,
//    "data": [
//    {
//    "_id": "57bacb6b2181616e58d89b58", "updatedAt": "2016-08-22T09:52:43.000Z", "createdAt": "2016-08-22T09:52:43.000Z", "name": "Ravi Mehrotra",
//    "email": "ravi+id2131@allies.co.in", "mobile": "+91-8763478262",
//    "userid": "564dcd28377ddb15130cb8e2", "__v": 0,
//    "status": "cold",
//    "active": true
//    }, {
//    "_id": "57bacaeffbe67cd957634e64", "updatedAt": "2016-08-22T09:50:39.000Z", "createdAt": "2016-08-22T09:50:39.000Z", "name": "Ravi Mehrotra",
//    "email": "ravi+id2131@allies.co.in", "mobile": "+91-8763478262", "userid": "564dcd28377ddb15130cb8e2", "__v": 0,
//    "status": "cold",
//    "active": true
//    }
//    ],
//    "limit": 25,
//    "rows": 2
//}