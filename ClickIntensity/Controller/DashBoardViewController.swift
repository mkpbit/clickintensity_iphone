//
//  DashBoardViewController.swift
//  ClickIntensity
//
//  Created by TecOrb on 02/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
let LETEST_REGISTRATION_LOADED_NOTIFICATION = "LETEST_REGISTRATION_LOADED_NOTIFICATION"

class DashBoardViewController: UIViewController,SlideNavigationControllerDelegate,UIScrollViewDelegate {
    var navigationView : DashboardNavigationView?
    @IBOutlet weak var registrationView : UIView!
    @IBOutlet weak var commissionView : UIView!
    @IBOutlet weak var salesView : UIView!
    @IBOutlet weak var registrationButton : UIButton!
    @IBOutlet weak var commissionButton : UIButton!
    @IBOutlet weak var salesButton : UIButton!
    @IBOutlet weak var segmentView : UIView!
    @IBOutlet weak var scrollV : UIScrollView!
    @IBOutlet weak var underLineV : UIView!

    var frontLine : FrontLineModel!
    var user : UserModel!
    var pageNumber = 1
    var latestUsersArray = [LatestSignUpModel]()
    var isNewDataLoading = false
//    var currentPage = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().barStyle = .Black
        let registrationVC = self.storyboard?.instantiateViewControllerWithIdentifier("RegistrationViewController") as! RegistrationViewController
        let commissionVC = self.storyboard?.instantiateViewControllerWithIdentifier("CommissionViewController") as! CommissionViewController

        let salesVC = self.storyboard?.instantiateViewControllerWithIdentifier("SalesViewController") as! SalesViewController



        self.addChildViewController(registrationVC)
        self.addChildViewController(commissionVC)
        self.addChildViewController(salesVC)
        let startTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.0 * Double(NSEC_PER_SEC)))
        dispatch_after(startTime, dispatch_get_main_queue(), {() -> Void in
            self.LoadScolllView()
        })
        self.setUpDashBoardNavigationView("0", frontLineCount: "0")
        self.loadLatestRegistration()

    }

    func getFrontLineAndTeamCountFromServer(){
         if let token = kUserDefaults.valueForKey(kUserToken) as? String{
        DashBoardService.sharedInstance.getFrontLineAndTeamCountWithToken(token, completionBlock: { (frontLineCount, teamCount) in
            self.setUpDashBoardNavigationView("\(teamCount)", frontLineCount: "\(frontLineCount)")
        })
        }
    }

    func loadLatestRegistration(){
        if let token = kUserDefaults.valueForKey(kUserToken) as? String{
            DashBoardService.sharedInstance.getLatestRegistrationsWithToken(token, pageNumber: self.pageNumber, completionBlock: { (responseArr) in
                self.isNewDataLoading = false
                if let latestRegArr = responseArr as? [LatestSignUpModel]{
                    self.latestUsersArray.appendContentsOf(latestRegArr)
                    NSNotificationCenter.defaultCenter().postNotificationName(LETEST_REGISTRATION_LOADED_NOTIFICATION, object: nil, userInfo: ["users":latestRegArr,"page":self.pageNumber])
                }
            })
        }
    }
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    // MARK: - SlideNavigationControllerDelegate methods
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
//    override func preferredStatusBarStyle() -> UIStatusBarStyle {
//        return .LightContent
//    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationView = nil
        segmentView.backgroundColor = UIColor.init(patternImage: UIImage(named: "segmentview.png")!)
        self.getFrontLineAndTeamCountFromServer()
            let userJson = UserModel.loadUserInfo()
            user = UserModel(json: userJson)
         UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setUpDashBoardNavigationView(teamCount: String,frontLineCount: String) {
        let nib = NSBundle.mainBundle().loadNibNamed("DashboardNavigationView", owner: self, options: nil)
        navigationView = nib[0] as? DashboardNavigationView
        navigationView!.frame = CGRectMake(0, 0, self.view.frame.size.width-74, 44)
        navigationView?.teamCountLabel.text = teamCount
        navigationView?.frontLineCountLabel.text = frontLineCount
        navigationView!.teamButton.addTarget(self, action: #selector(onClickTeam(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        navigationView!.frontLineButton.addTarget(self, action: #selector(onClickFrontLine(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let leftBarButtonItem = UIBarButtonItem(customView: navigationView!)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    @IBAction func onClickTeam(sender: UIButton){
        let teamVC = self.storyboard?.instantiateViewControllerWithIdentifier("TeamViewController") as! TeamViewController
        teamVC.isPushed = true
        self.navigationController?.pushViewController(teamVC, animated: true)
    }

    @IBAction func onClickFrontLine(sender: UIButton){
        let frontLineVC = self.storyboard?.instantiateViewControllerWithIdentifier("FrontLineViewController") as! FrontLineViewController
        frontLineVC.isPushed = true
        self.navigationController?.pushViewController(frontLineVC, animated: true)
    }

    @IBAction func onClickRegistration(sender: UIButton){
        self.toggleSegmentButton(sender)
        self.raceScrollTo(CGPointMake(0, 0), withSnapBack: true, delegate: nil, callback: nil)
        self.raceTo(CGPointMake(registrationButton.superview!.frame.midX - underLineV.frame.size.width*1/2, 47), withSnapBack: true, delegate: nil, callbackmethod: nil)
    }
    @IBAction func onClickCommission(sender: UIButton){
        self.toggleSegmentButton(sender)
        self.raceScrollTo(CGPointMake(self.view.frame.size.width, 0), withSnapBack: true, delegate: nil, callback: nil)
        self.raceTo(CGPointMake(commissionButton.superview!.frame.midX - underLineV.frame.size.width*1/2, 47), withSnapBack: true, delegate: nil, callbackmethod: nil)
    }
    @IBAction func onClickSales(sender: UIButton){
        self.toggleSegmentButton(sender)
        self.raceScrollTo(CGPointMake(2*self.view.frame.size.width, 0), withSnapBack: true, delegate: nil, callback: nil)
       self.raceTo(CGPointMake(salesButton.superview!.frame.midX - underLineV.frame.size.width*1/2, 47), withSnapBack: true, delegate: nil, callbackmethod: nil)

    }

    func toggleSegmentButton(sender : UIButton){
        sender.selected = true
        if sender == registrationButton{
            commissionButton.selected = false
            salesButton.selected = false
        }else if sender == commissionButton{
            registrationButton.selected = false
            salesButton.selected = false
        }else{
            registrationButton.selected = false
            commissionButton.selected = false
        }
    }

   
    //scroll view setting
    func LoadScolllView() {
//        self.currentPage = 0
//        self.page = 0
        scrollV.delegate = nil
        scrollV.contentSize = CGSizeMake(self.view.frame.size.width * 3, scrollV.frame.size.height)
        for i in 0 ..< self.childViewControllers.count {
            self.loadScrollViewWithPage(i)
        }
        scrollV.delegate = self
    }

    func loadScrollViewWithPage(page: Int) {
        if page < 0 {
            return
        }
        if page >= self.childViewControllers.count {
            return
        }
        var registrationVC : RegistrationViewController
        var commissionVC : CommissionViewController
        var salesVC : SalesViewController
        var frame: CGRect = scrollV.frame
        switch page {
        case 0:
            registrationVC = self.childViewControllers[page] as! RegistrationViewController
            registrationVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            registrationVC.view.frame = frame
            scrollV.addSubview(registrationVC.view!)
        case 1:
            commissionVC = self.childViewControllers[page] as! CommissionViewController
            commissionVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            commissionVC.view.frame = frame
            scrollV.addSubview(commissionVC.view!)
        case 2:
            salesVC = self.childViewControllers[page] as! SalesViewController
            salesVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            salesVC.view.frame = frame
            scrollV.addSubview(salesVC.view!)
        default:
            break
        }
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
    }



    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageCurrent = Int(floor(scrollV.contentOffset.x / self.view.frame.size.width))
        switch pageCurrent {
        case 0:
            self.toggleSegmentButton(registrationButton)
            self.raceTo(CGPointMake(registrationButton.superview!.frame.midX - underLineV.frame.size.width*1/2, 47), withSnapBack: true, delegate: nil, callbackmethod: nil)

        case 1:
            self.toggleSegmentButton(commissionButton)
            self.raceTo(CGPointMake(commissionButton.superview!.frame.midX - underLineV.frame.size.width*1/2, 47), withSnapBack: true, delegate: nil, callbackmethod: nil)
        case 2:
            self.toggleSegmentButton(salesButton)
            self.raceTo(CGPointMake(salesButton.superview!.frame.midX - underLineV.frame.size.width*1/2, 47), withSnapBack: true, delegate: nil, callbackmethod: nil)
        default:
            break
        }

        self.calledViewWillAppear()
    }
    func raceTo(destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callbackmethod : (()->Void)?) {
        var stopPoint: CGPoint = destination
        if withSnapBack {
            let diffx = destination.x - underLineV.frame.origin.x
            let diffy = destination.y - underLineV.frame.origin.y
            if diffx < 0 {
                stopPoint.x -= 10.0
            }
            else if diffx > 0 {
                stopPoint.x += 10.0
            }

            if diffy < 0 {
                stopPoint.y -= 10.0
            }
            else if diffy > 0 {
                stopPoint.y += 10.0
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseIn)
        underLineV.frame = CGRectMake(stopPoint.x, stopPoint.y, underLineV.frame.size.width, underLineV.frame.size.height)
        UIView.commitAnimations()
        let firstDelay = 0.3
        let startTime = dispatch_time(DISPATCH_TIME_NOW, Int64(firstDelay * Double(NSEC_PER_SEC)))
        dispatch_after(startTime, dispatch_get_main_queue(), {() -> Void in
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
             UIView.setAnimationCurve(UIViewAnimationCurve.Linear)
            self.underLineV.frame = CGRectMake(destination.x, destination.y, self.underLineV.frame.size.width, self.underLineV.frame.size.height)
            UIView.commitAnimations()
        })
    }

    func raceScrollTo(destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callback method:(()->Void)?) {
        var stopPoint = destination
        var isleft: Bool = false
        if withSnapBack {
            let diffx = destination.x - scrollV.contentOffset.x
            if diffx < 0 {
                isleft = true
                stopPoint.x -= 10.0
            }
            else if diffx > 0 {
                isleft = false
                stopPoint.x += 10.0
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseIn)
        if isleft {
            scrollV.contentOffset = CGPointMake(destination.x - 5, destination.y)
        }
        else {
            scrollV.contentOffset = CGPointMake(destination.x + 5, destination.y)
        }
        UIView.commitAnimations()
        let firstDelay = 0.3
        let startTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(firstDelay * Double(NSEC_PER_SEC)))
        dispatch_after(startTime, dispatch_get_main_queue(), {() -> Void in
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIViewAnimationCurve.Linear)
            if isleft {
                self.scrollV.contentOffset = CGPointMake(destination.x + 5, destination.y)
            }
            else {
                self.scrollV.contentOffset = CGPointMake(destination.x - 5, destination.y)
            }
            UIView.commitAnimations()
            let secondDelay = 0.1
            let startTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64( secondDelay*Double( NSEC_PER_SEC)))
            dispatch_after(startTime, dispatch_get_main_queue(), {() -> Void in
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1)
                UIView.setAnimationCurve(.EaseInOut)
                self.scrollV.contentOffset = CGPointMake(destination.x, destination.y)
                UIView.commitAnimations()
                self.calledViewWillAppear()
            })
        })
    }

     func calledViewWillAppear() {
        let pageWidth = self.view.frame.size.width
        let page = Int(floor((scrollV.contentOffset.x - pageWidth / 2) / pageWidth)) + 1
        var registrationVC : RegistrationViewController
        var commissionVC : CommissionViewController
        var salesVC : SalesViewController
        if page == 0
        {
            registrationVC = self.childViewControllers[page] as! RegistrationViewController
            registrationVC.viewWillAppear(true)
        }
        else if page == 1 {
            commissionVC = self.childViewControllers[page] as! CommissionViewController
            commissionVC.viewWillAppear(true)
        }
        else
        {
            salesVC = self.childViewControllers[page] as! SalesViewController
            salesVC.viewWillAppear(true)
        }
        
    }
}
extension DashBoardViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.latestUsersArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("DashBoardCell", forIndexPath: indexPath) as? DashBoardCell{
            return cell
        }
        return UITableViewCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
    }

    func scrollViewWillBeginDecelerating(scrollView: UIScrollView) {
        print_debug("scrolled!!")
    }


}

//This extension is written where all data loading would be called from server
extension DashBoardViewController{


    func loadFrontLine(token:String,userID: String, pageNumber: Int, recordPerPage: Int, listOrder: ListOrder, fromDate: String, toDate: String, listedUsersTeamMaximumSize: Int, listUsersRank: Int, listUsersMinimumGoldPacks: Int, listUsersMaximumGoldPacks: Int, listUsersMinimumSilverPacks: Int, listUsersMaximumSilverPacks: Int, searchFieldName: String, searchFieldValue: String){

        DashBoardService.sharedInstance.getFrontLine(token, userID: userID, pageNumber: pageNumber, recordPerPage: recordPerPage, listOrder: listOrder, fromDate: fromDate, toDate: toDate, listedUsersTeamMaximumSize: listedUsersTeamMaximumSize, listUsersRank: listUsersRank, listUsersMinimumGoldPacks: listUsersMinimumGoldPacks, listUsersMaximumGoldPacks: listUsersMaximumGoldPacks, listUsersMinimumSilverPacks: listUsersMinimumSilverPacks, listUsersMaximumSilverPacks: listUsersMaximumSilverPacks, searchFieldName: searchFieldName, searchFieldValue: searchFieldValue) { (responseFrontLine) in
            if let frntline = responseFrontLine as? FrontLineModel{
                self.frontLine = frntline
                self.setUpDashBoardNavigationView(self.frontLine.totalUsers, frontLineCount: self.frontLine.maxUsers)

            }
        }
    }

}
