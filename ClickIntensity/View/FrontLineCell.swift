//
//  FrontLineCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 03/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class FrontLineCell: UITableViewCell {
    @IBOutlet weak var userImageView : UIImageView!
    @IBOutlet weak var DOJLabel : UILabel!
    @IBOutlet weak var userLevel : UILabel!
    @IBOutlet weak var userNameLabel : UILabel!

    @IBOutlet weak var emailLabel : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
