//
//  LoginViewController.swift
//  Sinr
//
//  Created by TecOrb on 25/05/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON
class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var forgotPasswordButton : UIButton!
    @IBOutlet weak var signupButton : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(loginButton, borderColor: UIColor.clearColor(), borderWidth: 0, cornerRadius: 4)
    }
    @IBAction func onClickLoginButton(sender: UIButton){
        //check the params
        var params = [String:String]()
        if let email = emailTextField.text{
            if !CommonClass.validateEmail(email){
                showErrorWithMessage("Email is not correct")
                return
            }else{
                params.updateValue(email, forKey: kEmail)
            }
        }
        if let password = passwordTextField.text{
            if !CommonClass.validatePassword(password){
                showErrorWithMessage("Password is not correct")
                return
            }else{
                params.updateValue(password, forKey: kPassword)
            }
        }

        //check the network
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }else{
            CommonClass.showLoader()
            LoginService.loginWithParams(params) { response in
                CommonClass.hideLoader()
                if response != nil{
                    let json = JSON(response!)
                    print("JSON: \(json)")
                    showSuccessWithMessage("\(json)")
                }
            }

        }
    }
    @IBAction func onClickForgotPasswordButton(sender: UIButton){
        //push to forgot password screen
    }

    @IBAction func onClickSignUpButton(sender: UIButton){
        //push to Signup Screen

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
