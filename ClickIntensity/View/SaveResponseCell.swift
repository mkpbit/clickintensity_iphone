//
//  SaveResponseCell.swift
//  ClickIntensity
//
//  Created by TecOrb on 10/06/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

class SaveResponseCell: UITableViewCell {
    @IBOutlet weak var recordButton : UIButton!
    @IBOutlet weak var saveResponseButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class ResponseRepresentingCell: UITableViewCell {
    @IBOutlet weak var responseTypeLabel : UILabel!
    @IBOutlet weak var dateTimeLabel : UILabel!
    @IBOutlet weak var commentLabel : UILabel!
    @IBOutlet weak var containerView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        let color = UIColor(red: 175.0/255.0, green: 190.0/255.0, blue: 210.0/255.0, alpha: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.containerView, borderColor: color, borderWidth: 1, cornerRadius: 4)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
